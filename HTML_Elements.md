---
title: HTML 元素與屬性
date: 2020-02-3 20:44:16
tags:
- HTML
- 進階
- chapter5
categories:
- HTML
- Element
mathjax: false
---

[Mozilla Element](https://developer.mozilla.org/zh-TW/docs/Web/HTML/Element)

[標籤屬性](https://developer.mozilla.org/zh-TW/docs/Web/HTML/Attributes)

# 元素標籤

HTML 標籤可依功能分為下列幾群：

### 根元素

`<html>`

### 詮釋元素(metadata)

通常放在 `<head>` 裡面，預設通常會有 `<meta>`, `<link>`,`<style>`,`<title>`等。

### 本體根元素

`<body>`

### 內容部分元素

h1, h2, h3, h4, h5, h6 標題

網頁區塊　header, nav, main, section, article, aside, footer, details, summary 

訊息　address, time

[w3Schools Semantic Elements](https://www.w3schools.com/html/html5_semantic_elements.asp)

```html
    <details>
        <summary>點我看詳細(IE沒有支援)</summary>
        <p>詳細內容第1點，點擊之後才能看。</p>
        <p>詳細內容第2點，除非打開，否則看不到</p>
    </details>
    <p>不是詳細內容，就都可以看到！</p>
```

效果如下：

<details style="color: green">
        <summary>點我看詳細(IE沒有支援)</summary>
        <p>詳細內容第1點，點擊之後才能看。</p>
        <p>詳細內容第2點，除非打開，否則看不到</p>
    </details>
<p style="color: green">不是詳細內容，就都可以看到！</p></div>
網頁區塊示意圖：

![HTML Semantic Elements](https://www.w3schools.com/html/img_sem_elements.gif)



### 本文內容元素(Text content) 

blockquote, dd, dir, div, dl, dt, figcaption, figure, hr, li, ol, p, pre, iu 等。

表列(list)  ol, ul, li

引用文內容 blockquote

詞彙定義　dt, dd

### 線內本文語意(inline text semantics)

a, abbr, b, bid, bdo, cite,  data, dfn,  rb, rp, rt, rtc, ruby, s, samp, time, tt, u, var, wbr。

span 最常用來作為一般標示。q 與 quotation 類似，只是一個是線內，一個是區塊。

字型部分：code, em, b, i, kdd, mark, samp, s, small, strong

位置： ruby, rt, rp, rb, sup, sub

### 影像多媒體

area, audio, img, map, track, video

### 內嵌內容

embed

### 腳本 

canvas, script

### 表格

caption, col, colgroup, table, tbody, thead, td, th, tr

### 表單

form, button, input, label, option, output, progress, select, textarea。大部分的 ui 元件都是屬於 input 的。

表單的建立主要是在 form 元素。

```html
    <form>
	  <input type="email" required>
	  <input type="submit">
	</form>	
```

一個表單內可以有許多 input 元素，input 元素依據 type 屬性值提供不同的介面，e-mail 是一個文字輸入框，而submit 則是一個提交鍵：

<form> <input type="email" required>
  <input type="submit"></form>

給一個範例如下：

```html
<form>
	姓&nbsp;&nbsp;&nbsp;名：<input type="text" name="UserName" size="40"><br>
    E-Mail：<input type="email" name="UserMail" size="40" value="username@mailserver"><br>
    年&nbsp;&nbsp;齡：
      <input type="radio" name="UserAge" value="Age1">未滿20歲
      <input type="radio" name="UserAge" value="Age2" checked>20~29
      <input type="radio" name="UserAge" value="Age3">30~39
      <input type="radio" name="UserAge" value="Age4">40~49
      <input type="radio" name="UserAge" value="Age5">50歲以上<br>
    您使用過哪些廠牌的手機？
      <input type="checkbox" name="UserPhone[]" value="hTC" checked>hTC
      <input type="checkbox" name="UserPhone[]" value="Apple">Apple
      <input type="checkbox" name="UserPhone[]" value="ASUS">ASUS
      <input type="checkbox" name="UserPhone[]" value="acer">acer<br>
	您使用手機時最常碰到哪些問題？<br>
      <textarea name="UserTrouble" cols="45" rows="4">連線速度不夠快</textarea><br>
	您使用過哪家業者的門號？(可複選)
      <select name="UserNumber[]" size="4" multiple>
        <option value="中華電信">中華電信
        <option value="台灣大哥大" selected>台灣大哥大
        <option value="遠傳">遠傳
        <option value="威寶">威寶
      </select><br>
      <input type="submit" value="提交">
      <input type="reset" value="重新輸入">
</form>
```

介面如下：

<form style="background: lightyellow; padding: 1em;">
姓&nbsp;名：<input type="text" name="UserName" size="40"> <br>
E-mail: <input type="email" name="UserMail" size="40" value="sername@mailserver"> <br>
年齡：<input type="radio" name="UserAge" value="Age1"> 未滿20歲
    <input type="radio" name="UserAge" value="Age2"> 20-29
    <input type="radio" name="UserAge" value="Age3"> 30-39
    <input type="radio" name="UserAge" value="Age4"> 40-49
    <input type="radio" name="UserAge" value="Age5"> 50歲以上<br>
    您使用過哪些廠牌的手機？
   <input type="checkbox" name="UserPhone[]" value="hTC" checked>hTC
   <input type="checkbox" name="UserPhone[]" value="Apple">Apple
   <input type="checkbox" name="UserPhone[]" value="ASUS">ASUS
   <input type="checkbox" name="UserPhone[]" value="acer">acer<br>
  您使用手機時最常碰到哪些問題？<br>
   <textarea name="UserTrouble" cols="45" rows="4">連線速度不夠快</textarea><br>
  您使用過哪家業者的門號？(可複選)
   <select name="UserNumber[]" size="4" multiple>
    <option value="中華電信">中華電信
    <option value="台灣大哥大" selected>台灣大哥大
    <option value="遠傳">遠傳
    <option value="威寶">威寶
   </select><br>
   <input type="submit" value="提交">
   <input type="reset" value="重新輸入">
  </form>

除了上述常見 input type 之外，還有幾個也常用到：

```html
<input type="password" name="passwd" size="10">      密碼欄位
<input type="hidden" name="author" value="jkchen">   隱藏欄位，用來傳遞不給使用者看到的訊息
<input type="file" name"upfile" size="50">

<input type="email">
<input type="url">
<input type="search">
<input type="number" min="0" max="20" step="2">
<input type="range" min="0" max="24" step="3">
<input type="color">  跳出一個選色對話框
<input type="tel" pattern="[0-9]{4}(\-[0-9]{6})" placeholder="例如0926-388888" title="例如0926-388888">
<input type="date" name="birthday">
<input type="time" name="meeting">
<input type="datetime" name="activity">
<input type="month" name="duemonth">
<input type="week" name="workday">
<input type="datetime-local" name="activity">
```

label 是用來協助標示輸入框的元素。

fieldset 及 legend 是用來將輸入欄位進行分群，並對每個分群給一個標註(legend)。

```html
<label for="username">姓名</label> <input type="text" id="username">

<fieldset><legend>個人資料</legend>
<input type="text" name="username">
<input type="number" name="age">
</fieldset>
```



### 其他

template，用來保存用戶端內容，可以再運行時使用 javascript  來實例化。示例：

```html
<h1>Tempalte Sample </h1>
<button onclick="insertt()">insert a row in the table...</button>
    <table id="producttable">
        <thead>
          <tr>
            <td>UPC_Code</td>
            <td>Product_Name</td>
          </tr>
        </thead>
        <tbody>
          <!-- 在這裡可以選擇性地包括既有資料 -->
        </tbody>
      </table>
      
      <template id="productrow">
        <tr>
          <td class="record"></td>
          <td></td>
        </tr>
      </template>
```

首先，我們有個稍後將透過 JavaScript 插入的表格。接著，我們把重點轉移到描述 HTML 內容模板結構的 template：它意味著一個表格的行。

現在表格已經建立、也定義了模板，所以我們將以 template 為基礎，用 JavaScript 把每個產生出來的行加到表格內。

```js
        function insertt(){
            // 透過檢查 HTML template 元素屬性的存在與否，以測試瀏覽器是否支援它
            if ('content' in document.createElement('template')) {

            // 使用現有 HTML tbody、行以及模板，來實例化表格
            var t = document.querySelector('#productrow'),
            td = t.content.querySelectorAll("td");
            td[0].textContent = "1235646565";
            td[1].textContent = "Stuff";

            // 複製新的行並將其插至表格
            var tb = document.querySelector("tbody");
            var clone = document.importNode(t.content, true);
            tb.appendChild(clone);

            // 複製新的行
            td[0].textContent = "0384928528";
            td[1].textContent = "Acme Kidney Beans";

            // 複製新的行並將其插至表格
            var clone2 = document.importNode(t.content, true);
            tb.appendChild(clone2);

            } else {
            // 因為 HTML template 不被支援，所以要用其他方法在表格增加新行
            }
        }
```

使用 document.importNode(..., true) ，因為 template 不屬於 document, 因此被視為外部節點，外部節點要複製需要使用 importNode。 

語法： 

```javascript
var clone = document.importNode( externalNode, deep);
```

  deep = true 時，表示深度複製， deep = false 時，表示一層複製。

# 屬性

基本屬性 id, name, class

許多屬性是跟著元素的特性設計的，因此我們應該由元素的來學習各種元素屬性。

很多元素都有 width, height, value, disabled 。

屬性分為全域屬性及個別屬性。全域屬性是所有元素都能有的屬性，例如 id, name, class, ...。

