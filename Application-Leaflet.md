---
title: Application Leaflet
date: 2020-04-29 10:00:00
tags:
- leaflet
- maps
- ajax
categories:
- Application
---

範例 1(leaflet.html)

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link
      rel="stylesheet"
      href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
      crossorigin=""
    />
    <style>
      #mymap {
        height: 480px;
      }
    </style>
    <script
      src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
      integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
      crossorigin=""
    ></script>
    <script>
      function init(){
        if ('geolocation' in navigator) {
        console.log('geolocation available');
        navigator.geolocation.getCurrentPosition(position => {
          lat = position.coords.latitude;
          lon = position.coords.longitude;
          console.log(lat, lon);
          document.getElementById('latitude').textContent = lat;
          document.getElementById('longitude').textContent = lon;

          const mymap = L.map('mymap').setView([lat, lon], 15);
          const attribution =
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';
          const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
          const tiles = L.tileLayer(tileUrl, { attribution });
          tiles.addTo(mymap);
          const marker = L.marker([lat, lon]).addTo(mymap);
        }); 
        } 
        else {
          console.log('geolocation not available');
        }
      }
    </script>
    <title>Document</title>
  </head>
  <body onload="init();">
    <h1>基礎應用 地圖的使用 Leaflet App</h1>
    <p>
      緯度 glatitude: <span id="latitude"></span>&deg;<br />
      經度 longitude: <span id="longitude"></span>&deg;
    </p>
    <div id="mymap"></div>
    <script>
    </script>
  </body>
</html>
```

[Leaflet API JS 網站](https://leafletjs.com/)

1. 要使用 leaflet 地圖服務，要先載入相關連結，一個是 css, 一個是 js 程式庫。
2. 網頁要準備一個 dvi,  id 設定成 mymap, 用來放置地圖。
3. 我們將程式寫在 init() 裡面，onload 之後執行。
4. 利用 navigator 提供的目前所在位置，以裝置的位置作為地圖的中央。我們也可以自行定義要顯示的位置。
5. navigator.geolocation.getCurrentPosition( callbackFunction ); 取得目前的位置後，再執行 callbackFunction。這裡使用 arrow function 方式撰寫。
6. 如果我們的瀏覽器沒有支援 location, 那就不顯示了！
7. 回傳的 position, 經緯度以 position.coords.latitude 及 position.coords.longitude 來取得。
8. L.map("mymap").setview([lat, lon], 15) 取得 地圖，設中心點座標及縮放尺度。 尺度 1 ~ 16。
9. Leaflet 是地圖系統，不包含地圖上的圖片，地圖影像的繪製需要另外提供。我們這個範例利用 openstreetmap 的開源資料。
10. L.tileLayer( url, attribute); 設定 tile。 tile 是磁磚的意思，也就是地圖上一塊一塊的影像。這個 api 會根據 url  及 attribute 取得所需的 tile 。
11.  .addTo(mymap) 將磁磚貼到地圖上。
12. L.marker([lat, lon]) 設定地標位置。 .addTo( mymap ) 將路標放上地圖。

有關 leaflet 更多的服務，請參閱 Leaflet API 網站。 

練習： 在 google 地圖上找到 新竹、台北、台中、台南、高雄 的圖書館座標，在你的地圖上標上這些圖書館。



在地圖上，如果我們想要標上我們自己的圖像，我們可以使用 L.icon([lat, lon], ... ) 再把 icon 設成 L.marker ，最後再去設定它的位置。下面的範例，我們利用之前取得 ISS 國際太空站座標的程式，將國際太空站的移動在地圖上表現出來。

範例 2 leaflet_iss.html

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=\, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <link
      rel="stylesheet"
      href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css"
      integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
      crossorigin=""
    />
    <script
      src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js"
      integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
      crossorigin=""
    ></script>
    <script>
      function init(){
          // Making a map and tiles
          const mymap = L.map('issMap').setView([0, 0], 1);
          const attribution =
            '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors';

          const tileUrl = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
          const tiles = L.tileLayer(tileUrl, { attribution });
          tiles.addTo(mymap);

          // Making a marker with a custom icon
          const issIcon = L.icon({
            iconUrl: 'iss200.png',    // 請自行設計太空站圖樣
            iconSize: [50, 32],
            iconAnchor: [25, 16]      // 中心錨點在圖樣的中央
          });
          const marker = L.marker([0, 0], { icon: issIcon }).addTo(mymap);

          const api_url = 'https://api.wheretheiss.at/v1/satellites/25544';
          let firstTime = true;

          async function getISS() {
            const response = await fetch(api_url);
            const data = await response.json();
            const { latitude, longitude } = data;

            marker.setLatLng([latitude, longitude]);
            if (firstTime) {
              mymap.setView([latitude, longitude], 2);
              firstTime = false;
            }
            document.getElementById('lat').textContent = latitude.toFixed(2);
            document.getElementById('lon').textContent = longitude.toFixed(2);
          }
          
          getISS();
          setInterval(getISS, 1000);
      }
    </script>
    <style>
      #issMap {height: 480px;}
    </style>
    <title>Fetch JSON from API and map lat lon</title>
  </head>
  <body onload="init();">
    <h1>國際太空站在哪裡?</h1>
    <p>
      latitude: <span id="lat"></span>°<br />
      longitude: <span id="lon"></span>°
    </p>
    <div id="issMap"></div>
  </body>
</html>
```

在這個範例中，我們首次看到 async - await 的語法。 ES6 之後的 javascript 支援非同步語法：若我們要執行的指令需要等待完成後再繼續往下執行，我們可以在指令前加上 await ，表示這個指令要執行完成後才能繼續，例如 fetch()。 一個函式，若包含有 await ，則必須加上 async 確保這個函式的執行要能 await 。

fetch 的用法請參看我們 HTTP Fetch 單元的說明。

