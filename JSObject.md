---
title: JS Object 物件的使用
date: 2020-01-22 20:44:16
tags:
- JavaScript
- object oriented
- oo
categories:
- Javascript
mathjax: false
---
# 物件的使用

JS 的物件包含物件名稱及屬性

JSobject.JSProperty

```js
var myCar = new Object();
myCar.make = 'Ford';
myCar.model = 'Mustang';
myCar.year = 1969;
myCar.color; 
```

我們可以任意增加屬性，如上述的 .make, .model 以及 .year 。.color 也有效，但是其值為 undefined。

物件的屬性也可以使用中括號 [ ]來取用，與 . 相同。

```js
myCar['make'] = 'Ford';
myCar['model'] = 'Mustang';
myCar['year'] = 1969;
```

整數可以用來作為屬性，另外稱之為索引。我們可以使用 myCar[0] = 'zero'; 使用 myCar.0 ='zero';

但是在屬性表列中，整數或字串都可以是屬性。

```
myCar[1] = 'Tesla';
for( p in myCar )
   console.log( p );
```

結果會有　make, model, year, color, 1 六個。只是我們不能使用 myCar.1 。

for .. in 的語法是用來取出這個物件的所有屬性，也就是列舉屬性。若要直接取得屬性的陣列，可以使用下列二種方式：

- [`Object.keys(o)`](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Reference/Global_Objects/Object/keys)
  This method returns an array with all the own (not in the prototype chain) enumerable properties' names ("keys") of an object `o`.
- `Object.getOwnPropertyNames(o)`
  This method returns an array containing all own properties' names (enumerable or not) of an object `o`.

## 建立一個物件

方法有幾個，最簡單的方法是　var obj = {}; 或是 var obj = new Object(); 除此之外，也可以直接給值

```js
var myHonda = {color: 'red', wheels: 4, engine: {cylinders: 4, size: 2.2}};
```

或是使用建構子

```js
function Car(make, model, year) {
  this.make = make;
  this.model = model;
  this.year = year;
}
```

Now you can create an object called `mycar` as follows:

```js
var mycar = new Car('Eagle', 'Talon TSi', 1993);
```

這個概念是類別-物件的概念，只是 JS 提供了不只一種的類別-物件的機制，Car 是一種型態，稍後我們會看到使用 class 來建立類別以及之後物件的產生。

建立一個物件之後，物件的屬性同樣是隨時可以增加的。例如 mycar.color = 'red'; 但是若是要增加 Car 的屬性，便要使用 prototype 來增加。

```js
Car.prototype.color = null;
car1.color = 'black';
```

## 建立一個有方法的物件

```js
var myObj = {
  myMethod: function(params) {
    // ...do something
  }

  // OR THIS WORKS TOO

  myOtherMethod(params) {
    // ...do something else
  }
};
```

使用屬性: 匿名方法定義　或是　直接給一個函式名稱(省略關鍵字function)。

```
myObj.newMethod = function(args){
    // some actions
    // return or not return
}
```

或是使用已經定義好的方法，如下。

```
function method1(){
    return this.color;
}
car1.col = method1;
```

都是為物件增加一個方法。

雛形物件(或是理解為類別)，定義的方法需要使用初始定義或是 prototype。

```js
function displayCar() {
  var result = 'A Beautiful ' + this.year + ' ' + this.make
    + ' ' + this.model;
  pretty_print(result);
}
```

```js
function Car(make, model, year, owner) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.owner = owner;
  this.displayCar = displayCar;
}
```

合併成下列程式碼也可以。

```js
function Car(make, model, year, owner) {
  this.make = make;
  this.model = model;
  this.year = year;
  this.owner = owner;
  this.displayCar = function(){
        var result = 'A Beautiful ' + this.year + ' ' + this.make
    					+ ' ' + this.model;
    	pretty_print(result);
  		}
}
```

加上 prototype 再定義：

```js
Car.protoptye.displayCar = function() {
          var result = 'A Beautiful ' + this.year + ' ' + this.make
            + ' ' + this.model;
          pretty_print(result);
}
```

## Using `this` for object references

假設一個函式用來檢查 form 的輸入內容是否合法，

```js
function validate(obj, lowval, hival) {
  if ((obj.value < lowval) || (obj.value > hival)) {
    alert('Invalid Value!');
  }
}
```

我們可以使用 this 來表示輸入的元素。

```html
<input type="text" name="age" size="3"
  onChange="validate(this, 18, 99)">
```

In general, `this` refers to the calling object in a method.

可是，當我們與 `form` property組合使用時， `this`  refer to 目前物件的父層表單。檢視下列程式碼。

```html
<form name="myForm">
<p><label>Form name:<input type="text" name="text1" value="Beluga"></label>
<p><input name="button1" type="button" value="Show Form Name"
     onclick="this.form.text1.value = this.form.name">
</p>
</form>
```

## Defining getters and setters

A [getter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/get) is a method that gets the value of a specific property. A [setter](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/set) is a method that sets the value of a specific property. 這二個函式算是 magic functions，定義之後不直接呼叫，而是透過等號的左右來讀寫物件內容 。定義物件內某個屬性值的 getter 及 setter 有不只一種方式： 

```js
var o = {
  a: 7,
  get b() { 
    return this.a + 1;
  },
  set c(x) {
    this.a = x / 2;
  }
};

console.log(o.a); // 7
console.log(o.b); // 8
o.c = 50;
console.log(o.a); // 25
```

get b(){} 定義 右值 o.b 的動作。 

set c() 定義 左值 o.c 的動作。

下例是比較大部分的情況，針對一個屬性設定屬性的 setter 及 getter。 

下面範例為 Date 這個雛形增加一個新屬性 year, 並且設定 year 的 getter 及 setter 。

```js
var d = Date.prototype;
Object.defineProperty(d, 'year', {
  get: function() { return this.getFullYear(); },
  set: function(y) { this.setFullYear(y); }
});
```

These statements use the getter and setter in a `Date` object:

```js
var now = new Date();
console.log(now.year); // 2020 右值
now.year = 2001; // 987617605170　左值
console.log(now);
// Wed Apr 18 11:13:25 GMT-0700 (Pacific Daylight Time) 2001
```

In principle, getters and setters can be either

- defined using [object initializers](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Guide/Working_with_Objects#Object_initializers), or
- added later to any object at any time using a getter or setter adding method.

When defining getters and setters using [object initializers](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Guide/Working_with_Objects#Object_initializers) all you need to do is to prefix a getter method with `get` and a setter method with `set`. Of course, the getter method must not expect a parameter, while the setter method expects exactly one parameter (the new value to set). For instance:

```js
var o = {
  a: 7,
  get b() { return this.a + 1; },
  set c(x) { this.a = x / 2; }
};
```



```js
var o = { a: 0 };

Object.defineProperties(o, {
    'b': { get: function() { return this.a + 1; } },
    'c': { set: function(x) { this.a = x / 2; } }
});

o.c = 10; // Runs the setter, which assigns 10 / 2 (5) to the 'a' property
console.log(o.b); // Runs the getter, which yields a + 1 or 6
```

Which of the two forms to choose depends on your programming style and task at hand. If you already go for the object initializer when defining a prototype you will probably most of the time choose the first form. This form is more compact and natural. However, if you need to add getters and setters later — because you did not write the prototype or particular object — then the second form is the only possible form. The second form probably best represents the dynamic nature of JavaScript — but it can make the code hard to read and understand.

## Deleting properties　刪除掉 property

You can remove a non-inherited property by using the `delete` operator. The following code shows how to remove a property.

```js
// Creates a new object, myobj, with two properties, a and b.
var myobj = new Object;
myobj.a = 5;
myobj.b = 12;

// Removes the a property, leaving myobj with only the b property.
delete myobj.a;
console.log ('a' in myobj); // yields "false"
```

You can also use `delete` to delete a global variable if the `var` keyword was not used to declare the variable:

```js
g = 17;
delete g;
```

## Comparing Objects　物件是否相同

In JavaScript objects are a reference type. 除非二個物件是同一個物件，否則就算二個物件內容完全相同，相比時答案也會是 false。



```js
// Two variables, two distinct objects with the same properties
var fruit = {name: 'apple'};
var fruitbear = {name: 'apple'};

fruit == fruitbear; // return false
fruit === fruitbear; // return false
// Two variables, a single object
var fruit = {name: 'apple'};
var fruitbear = fruit;  // assign fruit object reference to fruitbear

// here fruit and fruitbear are pointing to same object
fruit == fruitbear; // return true
fruit === fruitbear; // return true
fruit.name = 'grape';
console.log(fruitbear);    // yields { name: "grape" } instead of { name: "apple" }
```

For more information about comparison operators, see [Comparison operators](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Comparison_Operators).

## 類別與物件

所有的 JavaScript 函數都可以作為建構子。使用 `new` 操作符來建立實例物件。

以建構子進行繼承，JavaScript 使用 prototype 來進行。

```js
function Employee () {
  this.name = "";
  this.dept = "general";
}
```

```js
function Manager () {
  this.reports = [];
}
Manager.prototype = new Employee;　　//Manager 繼承自 Employee

function WorkerBee () {
  this.projects = [];
}
WorkerBee.prototype = new Employee;  // WorkerBee 繼承自 Employee
```

### 建構子的預設參數

JavaScript 程式設計師喜歡使用 || 來為屬性的初始給預設值。

```js
function Employee (name, dept) {
  this.name = name || "";
  this.dept = dept || "general";
}
```

```js
function WorkerBee (projs) {
  this.projects = projs || [];
}
WorkerBee.prototype = new Employee;
```

JavaScript 的邏輯 OR 操作符（`||）`將求解它的第一個參數。如果該參數的值可以轉換為真，則操作符返回該值。否則，操作符返回第二個參數的值。

因此，這行代碼首先檢查 `name` 是否具有一個對 `name` 屬性有用的值。如果有，則設置其為 `this.name` 的值。否則，設置 `this.name` 的值為空的字串。

## 內建物件

### [Map](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map) 

以 insertion order 迭代物件內的每個元素。

一個 Map 跟一個 Object 其實很像，除了：

1. key  沒有預設，必須明示， Object 的 key 有可能預設。
2. key 可以是任何型態，包含 function, object, primitive
3. key 的順序依照 insertion 的順序
4. .size 可以取得Map中元素的個數

建構子

am = new Map()

主要用法：

新增 am.set(key, value)

取得  am.get(key)

 for..of 迭代

```js
let myMap = new Map()
myMap.set(0, 'zero')
myMap.set(1, 'one')

for (let [key, value] of myMap) {
  console.log(key + ' = ' + value)
}

for (let key of myMap.keys()) {
  console.log(key)
}

for (let value of myMap.values()) {
  console.log(value)
}

for (let [key, value] of myMap.entries()) {
  console.log(key + ' = ' + value)
}
```

Map 的一個元素是一個元素為2 的陣列！

nX2 的陣列可以作為 Map 建構子的參數：

```js
let first = new Map([
  [1, 'one'],
  [2, 'two'],
  [3, 'three'],
])
let second = new Map([
  [1, 'uno'],
  [2, 'dos']
])

let merged = new Map([...first, ...second])
```

... 運算(攤開運算) 對 Map 也可用！

am.forEach() 

```js
myMap.forEach(function(value, key) {
  console.log(key + ' = ' + value)
})
```

am.has(key)  是否存在

am.delete(key) 刪除元素



## [Set](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set)

as = new Set()

as.add(value)

as.has(value)

as.delete(value)

as.size

for( let a of as )

as.foreach((e)=>{ ..})

適合撰寫集合的運算，如 包含，聯集，交集，差集。

### [JSON](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON)

JSON.parse()

JSON.stringify()

JSON is a syntax for serializing objects, arrays, numbers, strings, booleans, and [`null`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null). It is based upon JavaScript syntax but is distinct from it: some JavaScript is *not* JSON.