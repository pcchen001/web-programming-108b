---
title: Ajax using Fetch 
date: 2020-04-15 10:00:00
tags:
- Javascript
- fetch
- ajax
categories:
- HTTP
- Javascript
mathjax: false
---
本單元介紹使用 fetch 指令進行 Ajax 工作。

我們介紹的任務分為 1. 到目標網站下載資料 2. 與我們的網站進行 ajax  3. 配合 session 。

Fetch 是ES6 內建的函式。支援 Promise, 也就是說可以使用 .then() 來處理取得的回應。Promise 我們另章介紹。

範例一 下載文字檔資料

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="utility.js" type="text/javascript"></script>    
    <script type="text/javascript">
      function init(){
        button1.onclick= async (e) =>{
          fetch("poetry.txt")
          .then((r)=>{return r.text()})
          .then((r)=>span1.innerText=r)
        }
      }
    </script>
  </head>
  <body>
    <form id="form1">
      <input id="button1" type="button" value="顯示詩句">
      <br><br>
      <span id="span1"></span>
    </form>
  </body>
</html>
```

poetry.txt

```
故人具雞黍，邀我至田家。綠樹村邊合，青山郭外斜。 
```

範例二  同一網站 php

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8"> 
    <script>
      function init(){
          button1.onclick= async (e)=>{
          fetch("GetServerTime.php")
          .then((r)=>r.text())
          .then((r)=>{span1.innerText=r})
        }   
      }
    </script>
  </head>
  <body onload="init()">
    <form id="form1">
      <input id="button1" type="button" value="顯示時間">
      <br><br><span id="span1"></span>
    </form>    
  </body>
</html>
```

GetServerTime.php

```
    <?php
        echo gmdate("Y-m-d H:i:s");       
    ?>
```

範例三 應用之一 (GET) 天氣

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <script type="text/javascript">
       function init(){
         console.log(city.value);
         let appid="f801d07b74053bf7c18416e01506c643";
         button.onclick = async (e)=>{
           let url = "http://api.openweathermap.org/data/2.5/weather";
           url +="?q=";
           url +=city.value; url += "&mode=json&appid=";
           url += appid;
            fetch(url)
            .then((r)=>{return r.json()})
            .then((r)=>{
                weather.innerText=r.weather[0].main + "溫度: " + Math.round(r.main.temp-273.15) + "C";
            })
            .catch((e)=>{console.log(e)});
         }
       }
    </script>
  </head>
  <body onload="init()">
    <h1>Open Weather Map Example</h1>
    <h2>using Ajax & Json</h2>
    城市：<input id="city" type="text" value="Miaoli"> or Taipei <br>
    <input id="button" type="button" name="" value="天氣">
    <span id="weather"></span>
  </body>
</html>
```

在這個範例，我們看到如何將參數送出去。 如果是一個 form 的資料，就必須將 form 資料取出包裝到 url。

使用 fetch, 回傳的資料可以在 then( ) 中的函式以參數帶進來。上面範例中，我們用變數 r 參照 response。

伺服器回傳的資料可能是 arrayBuffer, text, json, blob 或是 formdata。我們依照返回的型別，呼叫不同的函式來進行解碼。對應這 5 個型別，函式分別使用 arrayBuffer(), text(), json(), blob() 或是 formData() 。上面的範例是假設回傳的是 json 。

blob 是二進位碼資料，通常是影像或是其他型態的檔案內容。如果是影像的話，因為 HTML 支援 img 的 src 中放的是 URL編碼的影像資料，因此我們可以將一個影像檔案內容資料進行URL編碼，編碼後就可以放到 img 了。

範例四  blob 影像資料的取得：

```html
<body>
    <div><button id="getdel">顯示影像</button></div>
    <img id="img1" src="" alt="">
    <script>
        getdel.onclick = async (e) =>{
            imgurl = "https://upload.wikimedia.org/wikipedia/commons/7/77/Delete_key1.jpg";
            fetch(imgurl)
            .then((r)=>r.blob())
            .then((r)=>URL.createObjectURL(r))
            .then((r)=>img1.src = r)
        }
    </script>
</body>
```

補充說明：  then((r)=>r.blob()) r 是回傳的資料，執行 r.blob() 之後會自動回傳給下一個，加不加 return 都沒關係。我們先將 r 取出 blob 再將之進行URL編碼 再將之給 img1.src 。



範例一 ~ 範例四 都是使用 GET 送出資料，如果要送出一個 form (form 可能含有附檔) 那就需要使用 POST, 而 POST 的送出資料可以是 FormData 或是其他編碼的內容。我們先看 FormData 的語法

範例五 POST 

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax form post</title>
</head>
<body>
    <form id="myform">
        <input name="username" type="text" value="">
        <input type="password" name="password" value="">
        <input type="file" name="upfile">
        <input type="button" id="send" value="send">
    </form>
    <div id="response"></div>
    <script>
        send.onclick = async (e)=>{
            let fd = new FormData(myform);
            fetch("formpost.php",{
                method: "POST",
                body: fd,
            })
            .then((r)=>r.text())
            .then((r)=>{
                response.innerText = r;
            })
        }
    </script>
</body>
</html>
```

formpost.php

```php
<?php
$u = $_POST['username'];
$p = $_POST['password'];
$f = $_POST['upfile'];
echo $u;
echo $p;
?>
```

正確的話我們會獲得 username 及 password 。

POST 除了送出 formdata 外，也可以把要上傳的資料打包成 JSON，此時，接受端的 PHP 就不能用 formdata 的方式讀取資路，必須時用  file_get_contents("php://input"); 來讀取。

下面的範例除了 demo json 資料的上傳外，我們也 demo 額外設定的情況。

範例六

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <button id="ajson"> 送出 JSON </button>
    <div id="resp"></div>
    <script>
        ajson.onclick = async (e) =>{
            postData('jsondata.php', {answer: 42})
            // .then(data => data.json()) // JSON from `response.json()` call
            .then(data => { 
                for( val in data ) resp.innerHTML +=  val+":"+data[val]+"<br>";
                console.log(data)})
            .catch(error => console.error(error))
        }
            function postData(url, data) {
            // Default options are marked with *
            return fetch(url, {
                body: JSON.stringify(data), // must match 'Content-Type' header
                cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
                credentials: 'same-origin', // include, same-origin, *omit
                headers: {
                'user-agent': 'Mozilla/4.0 MDN Example',
                'content-type': 'application/json'
                },
                method: 'POST', // *GET, POST, PUT, DELETE, etc.
                mode: 'cors', // no-cors, cors, *same-origin
                redirect: 'follow', // manual, *follow, error
                referrer: 'no-referrer', // *client, no-referrer
            })
            .then(response => response.json()) // 輸出成 json
            }
    </script>
</body>
</html>
```

範例中，fetch 指令增加了許多參數是為了說明用的，不一定要加上去。

jsondata.php

```php
<?php
$j = file_get_contents("php://input");
$data = json_decode($j);
$data->answer = 123;
$data->key = "secret";
$j = json_encode($data);
echo $j;

?>
```

PHP 的處理，由於我們已經不是使用 formdata 來傳，因此不能用 $_POST['att'] 的形式來取得資料，標準的作法是 file_get_contents("php://input") 這會拿到執行這個php程式時所帶來的引數。

由於我們送過時打包成 json, 所以這裡就用 json 解碼。解碼後的資料是一個 php　的物件，要用物件的寫法去處理。



備註： 有時在處理回應前，想要檢驗 response 是否 ok,  可以先檢查 response.ok 是否為 true。 若不是 true 則應該是沒有送東西回來。(不一定會是 catch)  處理方式可依以下範例：

```javascript
fetch('flowers.jpg').then(function(response) {
  if(response.ok) {
    return response.blob();
  }
  throw new Error('Network response was not ok.');
}).then(function(myBlob) { 
  var objectURL = URL.createObjectURL(myBlob); 
  myImage.src = objectURL; 
}).catch(function(error) {
  console.log('There has been a problem with your fetch operation: ', error.message);
});
```



## 應用範例　國際太空站

國際太空站在地球上方 ? 萬公里，我們想寫個程式去抓這個太空站的位置。

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ajax(Fetch) Get JSON (ISS)</title>
    <script>
        const url = "https://api.wheretheiss.at/v1/satellites/25544";
        async function update(){
            fetch(url)
            .then( (r) => r.json())
            .then( (jsondata)=>{
                times.innerText="UNIX_TIME_STAMP: " + jsondata.timestamp
                isslati= jsondata.latitude;
                isslong= jsondata.longitude;
                issa.innerText="緯度：" + isslati;
                isso.innerText="經度：" + isslong;
            })
        }
        function init(){
            setInterval(update, 1000);
        }
    </script>
</head>
<body onload="init()">
    <h2>ISS 太空站 </h2>
    <div id="earchMap"></div>
    <div>
        <h3 id="times"></h3>
        <ul>
        <li id="issa">Latitude: </li>
        <li id="isso">Longitude: </li>
    </ul></div>
</body>
```



# Ajax/Fecth 配合 Session 

假設我們要開發一個單頁應用的網頁，與伺服器端的資料傳送便都會要使用 Ajax 。本節我們說明如何使用Ajax時，也能使用 Session 來記住 login 資料。

我們設計一個網頁，用一個表單送出登入資料。username, password 。

check.php 收到登入資料進行查核，若正確則回應 check: true 不正確則回應 check: false 。我們包成一個 json 。

ajax 收到回應後，若通過，則隱藏登入區塊，顯示 歡迎區塊以及 登出選項。若沒通過，則顯示未登入。

伺服器端查核成功時，設定 $_SESSION['login'] = true 。

要開始我們的應用時，假設我們要進入 trycheck.php 。此時若 SESSION OK, 那就回傳 check: true 否則回傳 ckeck: false。

```html
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ajax Fetch Session</title>
</head>
<body>
<h1>Ajax Fetch 配合 Session</h1>
<h2>Main Page</h2>
<div id="login">
<form id= "loginForm" name="loginForm">
  帳號：(正確為kk) <input type="text" id="username" name="username" value=""><br>
  密碼：(正確為yy)<input type="password" id="password" name="password" value=""><br>
  <input type="submit" value="登入"></input>
</form>
</div>
<div><button id="tryRequest">進入應用</button></div>
<div id="welcome"></div>
<div id="logout"><h2><a href="logout.php">登出</a></h2></div>
<div id="application"></div>
<script>
       logout.style.display="none";
        loginForm.onsubmit = async (e)=>{
        e.preventDefault();
        let response = await fetch('check.php', {
          method: 'POST',
          body: new FormData(loginForm),
          credentials: 'same-origin',    // 這一行很重要
        });
        let result = await response.json();
        console.log(result);
        if( result.check == "true"){
          login.style.display = "none";
          welcome.innerHTML = "Welcome!";
          logout.style.display = "block";
        }
      }

      tryRequest.onclick = async (e)=>{
        e.preventDefault();
        let response = await fetch('trycheck.php', {
          method: 'POST',
          credentials: 'same-origin',   // 這一行決定送  cookie 出去
        });
        let result = await response.json();
        console.log(result);
        if( result.check == "true"){
          h3 = document.createElement("h3");
          h3.innerText = "Retry OK! ";
          welcome.appendChild(h3);
        }
        if( result.check == "false"){
          let h3 = document.createElement("h3");
          h3.innerText = "尚未登入!! ";
          application.appendChild(h3);
        }

      }
</script>
</body>
</html>
```

check.php

```php
<?php
session_start();
// 能夠同時處理 post 及 get 二種需求
$username = isset($_POST['username']) ? $_POST['username'] : $_GET['username'];
$passwd = isset($_POST['password']) ? $_POST['password'] : $_GET['password'];

if($username == "kk" && $passwd == "yy"){
  $_SESSION['log'] = true;
  $resp = '{ "check" : "true" ';
  $resp .= ', "username" : "'.$username.'" }';
  echo $resp;
  }
else
  echo '{ "check" : "false" }';
?>
```

trycheck.php

```php
<?php

session_start();
if( !isset($_SESSION['log'])) {
  echo '{ "check" : "false" }';
  exit();
}

if ( $_SESSION['log'] == true ){
    $resp = '{ "check" : "true" }';
    echo $resp;
    exit();
}

  echo '{ "check" : "false" }';
  exit();
?>
```

如果我們在 Edge 執行網頁，打開開發人員工具，在[網路]這一欄下，你可以檢視傳送 ajax 的 head。可以留意 cookie 的 PHPSESSION。

要提醒大家的是使用 fetch 或是 jQuery 的 ajax 指令時，都必須要將 credentials 打開，這樣我們的瀏覽器才能把 Session 的 cookie 送回去給伺服器端做認認。沒有打開伺服器端就沒辦法知道送來的 request 是否是同一個端點。jQuery 打開的設定是 credentials: true 。詳細請參閱相關說明。





------

#### 進階範例

Example jsondata2.html (本範例與上面的 jsondata 類似，目的是增加程式碼的參考性！同學可以留意差異之處)，另外本範例的另一個目的是測試 CORS。因此測試的正確作法是將  jsondata2.php 放在另一個伺服器，例如 http://coding.im.nuu.edu.tw 而 jsondata2.html 則是從 localhost 取得。如此才能看出異地資源存取的問題及解決方式。

- credentials
  - credentials: same-origin 表示當fetch送出request的目的與這個網頁相同來源時，送出認證識別，這個認證識別是一個　PHPSession的cookie。有了這個 cookie, 伺服器端才可以檢驗 session 。
  - credintials: include 表示就算不同源，也會送出 cookie 。　 omit 則是不送出。fetch 預設是不送的！
  - 我們進行 ajax 的 session 檢查是，必須要設 include 或是 same-origin。
- mode: cors
  - 假使我們 fetch 非同源資料時， mode 要設為 cors。 允許瀏覽器擷取非同源資料。
- cache: no-cache
  - 確保我們每次讀去的資料都是強制讀取最新版本的資料。有時我們找不到錯誤，原因就是因為取得的資料是上次 cache 的，而非我們已經更改過的！

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <script>
    function my(){
        fetch('http://localhost/web-programming-108b/Examples/Ch15/fetch.php')
        .then(function(response) {
            let data = response.json();
            console.log( data );
            return data;
        })
        .then(function(myJson) {
            console.log(myJson.a);
        });
    }

    function my1(){
        console.log("begin my1");
        postData('http://localhost/web-programming-108b/Examples/Ch15/fetch.php', {answer: 42})
        .then(data => console.log(data.json())) // JSON from `response.json()` call
//   .catch(error => console.error(error));
    }


function postData(url, data) {
  // Default options are marked with *
  return fetch(url, {
    body: JSON.stringify(data), // must match 'Content-Type' header
    cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
    // credentials: 'same-origin', // include, same-origin, *omit
    headers: {
      // 'user-agent': 'Mozilla/5.0',
      'content-type': 'application/json' // 'text/plain', 'application/x-www-form-urlencoded'
    },
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    mode: 'cors', // no-cors, cors, *same-origin
    redirect: 'follow', // manual, *follow, error
    referrer: 'no-referrer', // *client, no-referrer
  })
  .then(response => {
    // const data = response.json();
    // console.log(data.a);
    return response;
  })
}
    </script>
</head>
<body>
    <button onclick="my();">GET</button>
    <button onclick="my1();">POST</button>
</body>
</html>
```

 fetch.php

```php
<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
if($_SERVER['REQUEST_METHOD'] == "POST"){
    // 換成php://input 
    $inputJson = file_get_contents('php://input');
    $p = json_decode($inputJson);
    $s = json_encode($p);
    // $s = '{"a":"post", "b":"banana", "c":123, "d":999}';
    echo $s;
}
else {
    $ans = $_GET['answer'];
    $s = '{"a":'.$ans.', "b":"banana", "c":123, "d":999}';
    echo $s;
}

?>
```

php 補充說明： 

1. 前面三個 header 主要是解決 cors 的問題。由於 application/json 會被要求 preflight, 因此要加上第2行。

2. 當我們送上來的資料不是 formData 時，$_POST['field'] 會拿不到資料，因此要改成 

   ```php
       $inputJson = file_get_contents('php://input');
       $p = json_decode($inputJson);
   ```

   才能拿到資料。

3. 使用 url 或是 formData  送上來的資料，則可以使用 $_GET 或是 $\_POST 讀取。

4. php 的 header 也可以直接再 Apache 伺服器中設定，方法之一是修改 httpd.conf，範例如下：

   1.  header module 要打開，伺服器送出的回應才能將所設定的 header 送出去。

   ```
   LoadModule headers_module modules/mod_headers.so
   
   <Directory />
       AllowOverride none
       Require all denied
   # Options +Indexes +FollowSymLinks
   Header set Access-Control-Allow-Origin "*"
   # AllowOverride all
   # Require local
   
   </Directory>
   ```

   #### 

