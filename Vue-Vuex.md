---
title: Vue Vuex
date: 2020-04-12 20:22:16
tags:
- Vue
- Vuex
- Store
categories:
- Vue
- Advanced
---

Vuex 是一個提供前端網頁間共同使用變數的物件，利用 Vuex 的 store, 各個模組可以共享同一套變數。

我們可以想像一個前端的資料庫，為了物件化，我們會定義有那些資料，以及讀寫這些資料的函式，然後透過讀寫函式來達到使用這些資料的目的。

這些資料我們稱之為 state, 寫的動作稱之為 actions 讀的動作稱之為 getters, actions 可以呼叫 mutations 的函式來完成動作。這些東西的組合稱之為 store。換句話說，vuex 用 store 概念來實作，包含 state, mutations, actions 以及 getters 。

在 vue cli 的步驟：

1. 如果創建項目 create project 時不選預設，手動增加了 Vuex，所產生的項目就會有 store 子目錄。新增 index.js, state.js, getters.js, actions.js 以及 mutations.js。

2. index.js

   ```
   import Vue from 'vue'
   import Vuex from 'vuex'
   import state from '@/store/state'
   import actions from '@/store/actions'
   import getters from '@/store/getters'
   import mutations from '@/store/mutations'
   Vue.use(Vuex)
   
   export default new Vuex.Store({
     state,
     mutations,
     actions,
     getters
   })
   ```

   準備 state.js, actions.js, getters.js 以及 mutations.js 四個檔案。

3. state.js

   ```
   export default {
     count: 0,
     posts: [
       {
         title: 'Post 1',
         body: 'Body of Post 1'
       },
       {
          title: '貼文 2',
          body: '　我的貼文 2 內容'
       }
     ]
   }
   ```

   定義所需要的 state, 也就是資料。可以在這裡給初始資料。

4. getters.js

   ```
   export default {
       postsCount: (state)=>{
           return state.posts.length
       },
       getAllPosts: (state) =>{
           return state.posts
       },
       getCount: (state)=> return state.count
   }
   ```

   map　到 computed: 。

5. actions.js

   ```
   export default {
       addPost: ({commit}, payload) => {
           commit('appendPost', payload)
       },
       increment: ({commit})=> commit("inc"),
       decrement: ({commit})=> commit("dec")
   }
   ```

   作為匯出給模組使用的寫入函式。vuex 執行 mutations 的函式必須使用 commit 或是 dispatch。 dispatch 我們暫時不介紹，記得要用 commit 就可以了。參數裡的 {commit}　也可以有其他寫法，但這樣寫是最方便的。

6. mutations.js

   ```
   export default {
       appendPOst: (state, post) => {
         state['posts'].push(post)
       },
       inc: (state)=>state['count']++,
       dec: (state)=>state['count']--
     }
   ```

   mutations 是更動之意，提供vuex內部運算的工具函式。要記得函式參數必須含有 state, 這個在 commit 時不須傳入。

7. 會用到state, actions, getters 的模組，匯入

   ```
   import {mapState, mapActions, mapGetters} from 'vuex'
   ```

   留意匯入使用 mapState, mapActions 及 mapGetters 對應 state, actions 及 getters 。這些在使用時，必須使用 ...mapState(['someState', 'other']) 的形式對應到 vue 內的物件。... 是 spread 運算，若不知道這是什麼運算子，請 google 'javascript spread' 學習一下。

8. 使用時

   ```
     computed: {
       ...mapGetters([
         'postsCount', 'getAllPosts', 'getCount'
       ])
     },
     methods:{
       ...mapActions([
         'addPost'
       ]),
   ```

   如此一來， 三個 getters 的函式就被應對到模組中的 computed ，而 actions 的函式 addPost 也被應對到 methods 。使用範例：

   ```vue
   <template>
     <div class="home">
       <img alt="Vue logo" src="../assets/logo.png">
       <HelloWorld msg="Welcome to Your Vue.js App"/>
   
       <li v-for="post of getAllPosts" :key="post.id">
         <span><strong>{{post.title}}</strong>:{{post.body}}</span>
       </li>
       <input type="text" v-model="formData.title">
       <input type="text" v-model="formData.body">
       <input type="button" @click="addapost" value="新增貼文">
       {{getCount}}
     </div>
   </template>
   
   <script>
   // @ is an alias to /src
   import HelloWorld from '@/components/HelloWorld.vue'
   import {mapActions, mapGetters} from 'vuex'
   
   export default {
     name: 'Home',
     components: {
       HelloWorld
     },
     data(){
       return {
         formData: {
           title: "",
           body: ""
         }
       }
     },
     computed: {
       ...mapGetters(['getCount', 'getAllPosts'])
     },
     methods: {
       ...mapActions(['decrement','increment','addpost']),
       addapost(){
         const {title, body} = this.formData;
         const npost = {title:title, body:body};
         this.addpost(npost);
         console.log(title)
       },
       decr: ()=>this.decrement()
     }
   }
   </script>
   ```

  請留意 

1. methods 中， ...mapActions 是將 $store.actions 的函式 map 到同名的函式。
2. 定義 addapost, 定義的方式不使用 addapost: function(){}而是 addapost(){}。請特別留意，否則會說找不到 formData。

詳細程式碼請參考範例　 vueX/a1, 

延伸範例 adopt-pets 。

