---
title: CSS Box
date: 2020-01-18 20:44:16
tags:
- Basic
- Box
categories:
- CSS
mathjax: false

---
# Cascaded Style Sheet CSS Box Model

Box Model 是一種排版模型，針對每一個元素，都可以有內容，留白，框限，邊界四項組成。參考下面圖示。

![](image-20200304204058135-1585714622971.png)



我們可以設定一個元素的 height, width, margin, border, padding 等。相關屬性：

```
height
width
top
left
right
bottom
margin
padding: 10px;   // 10px 5px 上下 左右，  3px 5px 7px 8px 上 右 下 左，
border: solid 10px lightyellow; 
border-color
border-top-color: red;  // top, left, right, bottom 都有可以分別設定
border-wdith
bordertop-width: 10; // top, left, right, bottom 都有可以分別設定
```

## 基本定位方式

CSS 定位使用了幾個概念，

1. display 設定如何顯示，例如方塊顯示，線內顯示，不顯示等。
2. position 設定定位方式，例如靜態，相對，絕對，固定等。
3. float,clear 設定文繞圖。

display 的屬性值可由三個組成。

元素外部: block, inline, run-in 三選一。

元素內部： flow, flow-root, table, flex, grid, ruby 六選一。

元素盒：contents, none 二選一。 

position 的屬性值有 static, relative, absolute, fixed 。

- static 是預設的定位方式，我們無法使用 top, left, 等屬性更改元素位置。
- relative 是相對的定位方式，我們使用 top, left 來更動元素位置時，是相對於 static 時的位置。
- absolute 是絕對的定位方式，以網頁頁面的左上角為(0, 0) 。元素重疊時依據 z-index 來決定哪一個在上面。z-index 的數值愈大愈上面。
- fixed 是固定定位，以視窗的左上方為0, 0。畫面捲動時，元素固定在視覺上的固定位置。

文繞圖的設定可以讓方塊浮動，當畫面寬度改變時，方塊可能因此改變排版。解除文繞圖使用 clear。

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
	<title>示範定位方式</title>
	<style>
      img {float:left}
      #n2 {float:none}
    </style>
  </head>	
  <body>
    <img src="jp2.jpg" width="300">    
    <img id="n2" src="jp2.jpg" width="300">

	<h1>豪斯登堡</h1>
    <p>豪斯登堡位於日本九州，一處重現中古世紀歐洲街景的渡假勝地，
	   命名由來是荷蘭女王陛下所居住的宮殿豪斯登堡宮殿。</p>
	<p style="clear:left">園內風景怡人俯拾皆畫，還有『ONE PIECE 航海王』的世界，乘客
	   可以搭上千陽號來一趟冒險之旅。</p>	  
  </body>
</html>
```

一個 float:left 會讓這個方塊靠左，其餘變成文繞圖。若想某個方塊不再跟著文繞圖，那就設定 clear:left。

float: left 由 clear: left 來清。 float:right 由 clear:right 來清。 clear: both 是清掉 float:right 及 float:left。



一個由 box-model 框起來的方塊，我們可以加陰影效果。

```html
<h1 style="width:400px; background-color:lightpink; box-shadow:10px 10px 5px silver">將禁酒</h1>    
<h1 style="width:400px; background-color:burlywood; box-shadow:10px 10px 10px silver, 20px 20px 20px lightyellow">鳳凰臺</h1>
```

 <h1 style="width:400px; background-color:lightpink; box-shadow:10px 10px 5px silver">將進酒</h1>
<h1 style="width:400px; background-color:burlywood; box-shadow:10px 10px 10px silver, 20px 20px 20px lightyellow">鳳凰臺</h1>



垂直對齊