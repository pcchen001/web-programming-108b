---
title: Vue Cli
date: 2020-05-20 08:00:00
tags:
- Vue
- vue cli
- vue cli ui
- framework
categories:
- Advanced
---

安裝

npm install -g @vue/cli

更新

npm update -g @vue/cli

安裝之後，用 ui 進入專案管理介面：

vue ui

點選左下方...，點開 Vue 項目管理器

點選 +創建

選擇專案位置，點擊下方　＋在此創建新項目　(create new project)

給　project folder 項目文件夾，使用預設默認，下一步

點選 default 默認　（babel, eslint) 點選下方 創建項目 (create project)

等上約 2 ~ 5分鐘。出現 Welcome to new Project 歡迎來到新項目

點選左側欄上的 task 任務 (另外四個分別是儀錶板dashboard、插件 plugin、依賴 dependence、配置 configuration)

點選 serve 。出現一個儀表盤，可以點選運行(operate)，啟動伺服器

點選啟動app 或是　到瀏覽器上，輸入 localhost:8080 。即可看到預設的畫面。

現在，開啟 VS Code, 開啟對應的資料夾。

打開 src/ 我們的主要程式碼都會在這個資料夾裡面。

先看一下 public/index.html, src/main.js 以及 src/App.vue 。

[課堂說明 三個檔案內容、功能及關連性]

我們從 App.vue 開始編輯。

編輯 HelloWorld.vue

```
<template>
  <div class="hello">
     Hello World! and msg:{{msg}}
  </div>
</template>
```

template　內留下三行就可以了，script 及 style 保留。

留意到 App.vue 的 style 沒有 scoped，　HelloWorld.vue 的 style 有 scoped 。scoped 表示這個　css 只有區域性有效。沒有 scoped 表示 global scope.

接著來到 App.vue

```vue
<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
    <li :key="todo.id" v-for="todo in todos">
      <input type="checkbox"><span>{{todo.name}}</span>
    </li>
  </div>
</template>

<script>
import HelloWorld from './components/HelloWorld.vue'

export default {
  name: 'App',
  components: {
    HelloWorld
  },
  data(){
    return {
      todos: [
        {
          name: 'To play with vue',checked: false
        },
        {
           name: '讀一本小說', checked: false
        }
      ]
    }
  }
}
</script>
```

script 增加了 data(){} 部分。原本應該是

```vue
data: {
  todos: ...
}
```

但使用 data(){ return { ... }} 比較常見。

我們先給二筆資料做範例。

template 增加了　li 。適用 v-for 取得陣列的元素，通常需要加上 :key 。

現在可以看一下畫面。出現了二筆待班事項了！

接著我們要說明如何設計新的元件。請新增一個 todos.vue 再 components/ 資料夾下面。

```vue
<template>
    <div class="todos">
        <h2>待辦事項</h2>
        <li :key="todo.id" v-for="todo in todos">
            <span><input type="checkbox">{{todo.name}}</span>
        </li>
    </div>
</template>
<script>
export default {
    name: "Todos",
    components: {},
    props: {        // 也可以寫成 props: ["todos"] 不特別標註型別
        todos: Array
    }
}
```

原來的 App.vue 改成

```
    <Todos :todos="todos" />
```

script 部分，要將剛才的 Todos 模組匯入，tempalte 的 Todos  標籤才能用。

```
import Todos from './components/Todos.vue'
```

接著我們增加　新增待辦事項　的功能。

App.vue  template 部分增加一個 form

```vue
    <form action="">
      <input type="text" v-model="newtodo">
      <input type="button" @click="addtodo" value="新增待辦事項">
    </form>
```

script 部分，data(){ } 內，增加 newdata: ""。

另外增加 methods: { addtodo : function(){}} 用來處理 按下按鈕時新增待辦事項。

```vue
<script>
import HelloWorld from './components/HelloWorld.vue'
import Todos from './components/Todos.vue'

export default {
  name: 'App',
  components: {
    HelloWorld, Todos
  },
  data(){
    return {
      todos: [...],
      newtodo: ""
    }
  },
  methods:{
    addtodo: function(){
      const newone= {name:this.newtodo,checked: false};
      this.todos.push(newone);
    }
  }
}
</script>
```

當我們輸入一筆資料時，記錄成 {name: "", checked:false}，然後 push 到 todos 陣列。

目前大致完成一個待辦事項的顯示及新增。 Ch17_VueCli/b02



為了更深入學習更多的語法及功能，我們來看 todolist 範例。

gitlab 我們的課程範例　Ch17_VueCli/todolist 

新增待辦事項也寫成一個 module 。AddTodo.vue

```vue
<template>
  <div>
    <h3>Add Todo bar</h3>
    <form @submit="addTodo">
      <input type="text" v-model="title" name="title" placeholder="Add Todo...">
      <input type="submit" value="Submit" class="btn">
    </form>
  </div>
</template>

<script>
// import uuid from 'uuid';
export default {
  name: "AddTodo",
  data() {
    return {
      title: ''
    }
  },
  methods: {
    addTodo(e) {
      e.preventDefault();
      const newTodo = {
        title: this.title,
        completed: false
      }
      // Send up to parent
      this.$emit('add-todo', newTodo);

      this.title = '';
    }
  }
}
</script>
```

重點之一是 this.$emit('add-todo', newTodo) 。這是發出事件 add-toto ，這個事件的處理函式會取得 newTodo 資料。

這表示當我們在這個模組內輸入一筆資料時，產生了一個事件add-todo ，這個事件需要上一層的模組來處理。請看 App.vue

```vue
    <AddTodo v-on:add-todo="addTodo"/>
```

當 addTodo 模組引發事件 add-todo 時，由方法 addTodo 來處理。

分別說明：App.vue, Todos.vue, TodoItem.vue。

這個範例使用了一個 open resouece https://jsonplaceholder.typicode.com/todos 這個服務提供了讀寫 todo list 的範例服務，是用來協助學習的。我們應該更改為我們自己的資料庫服務。

另外，這個範例也提供了一個刪除待辦事項的功能，詳細在 TodoItem.vue 。

------

CORS 問題：我們撰寫自己的資料庫服務時，會遇到 CORS 的問題。解決之道在於 apache httpd 的設定。簡要步驟如下：

步驟：

1. 到 httpd.conf 編輯，加上 Header set Access-Control-Allow-Origin "*"

   ```html
   DocumentRoot "${INSTALL_DIR}/www"
   <Directory "${INSTALL_DIR}/www/">
       Options +Indexes +FollowSymLinks +Multiviews
       Header set Access-Control-Allow-Origin "*"
       AllowOverride allRequire local
    </Directory>
   ```

2. 找到 # LoadModule headers_module modules/mod_headers.so 將 # 移除

```
LoadModule headers_module modules/mod_headers.so
```

重先啟動 Apache 。

說明：　這個設定讓伺服器回覆時，header 加上 Access-Control-Allow-Origin: * 這是瀏覽器在進行跨源請求時需要的回覆，若沒有得到這樣的回覆，則表示這個資料來源可能會傷害使用者，因此幫使用者擋掉了！

------

