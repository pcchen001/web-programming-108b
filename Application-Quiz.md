---
title: Application Quiz
date: 2020-04-22 08:00:00
tags:
- ajax
- Application
categories:
- Application
---

我們找了一個應用來練習 ajax 以及互動處理。

我們從 open TRIVIA database 獲得靈感： https://opentdb.com/

在這個網站，你可以根據類別取得或是貢獻一些你覺得不難的題目及答案。

opentdb.com 提供了 API, 我們可以給類別及題型，從這個網站取得題目庫。...

仿照該網站的題庫資料結構，我們作了以下的範例及練習。

線上測驗的應用，假設我們有題庫，一題包含有問題，正確答案以及不正確答案。

假設我們的資料 opentdb.json

```json
{"response_code":0,
  "results":[
    {
        "category":"Science: Computers","type":"multiple","difficulty":"medium",
         "question":"新冠病毒爆發，那個國家災情最嚴重?",
         "correct_answer":"美國",
         "incorrect_answers":["中國大陸","義大利","西班牙"]
    },
    {
        "category":"Science: Computers","type":"multiple","difficulty":"medium",
        "question":"Whistler was the codename of this Microsoft Operating System.",
        "correct_answer":"Windows XP",
        "incorrect_answers":["Windows 2000","Windows 7","Windows 95"]
    },
    {
        "category":"Science: Computers","type":"multiple","difficulty":"medium",
        "question":"Moore&#039;s law originally stated that the number of transistors on a microprocessor chip would double every...",
        "correct_answer":"Year",
        "incorrect_answers":["Four Years","Two Years","Eight Years"]
    }
  ]
}
```

程式步驟：

1. 新準備一個 button (下一題) 以及 二個 div(題目，答案)
2. 撰寫 onload 的函式，我們用 jQuery 的 $(function(){}); 來進行。
3. 載入題庫後，取出第一題。
4. 第一題設定 current = 0, 我們取出題目，用一個 h2 來表示
5. 建立一個陣列，將正確答案跟不正確答案都放到陣列，搖一搖(shuffle)
6. 將陣列的每一個答案都表示成一個按鈕。
7. 把每一個答案按鈕 appendChild() 給 div#answer。
8. 我們設計二組 class (答對，答錯)，分別設定 css。暫時設計成背景色不同。
9. 回答時，檢查答案，錯誤，設定 class 為答錯，正確則設定成 答對。
10. 下一題，只需要 current += 1, 再把顯示題目的內容更新就可以了。
11. 再看範例之前，我們先複習即個指令
    1. document.querySelector( selector );
    2. e.lastElementChild 最後一個 child, 
    3. e.removeChild( child ) 移除某個 child ，可以從 lastElementChild  開始移除
    4. document.createElement()
    5. appendChild()
    6. push()
    7. e.setAttribute("class", "corrOrincorr");

範例 1 quiz_basic.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Quiz</title>
    <style>
        .correctanswer {
            background: rgb(0, 255, 76);
        }
        .incorrectanswer {
            background: orange;
        }
    </style>
    <script>
        function init(){
            fetch("opentdb.data")
            .then((r)=>r.json())
            .then((data)=>{
                results = data.results;
                nofq = results.length
                sample = results[0]
                update();
            })
        }

        var current = 0;
        function update(){
            while( questiondiv.lastElementChild )
                questiondiv.removeChild(questiondiv.lastElementChild); 
            let sample = results[current];
            let question = document.createElement("h2");
            question.innerHTML = (current+1)+". "+sample.question;
            questiondiv.appendChild(question); 
            anslist=[]; console.log(anslist.length);
            correct = sample.correct_answer;
            anslist.push(correct); 
            for( ic of sample.incorrect_answers){
                anslist.push(ic);
            }
            shuffle(anslist);
            while( answersdiv.lastElementChild )
                answersdiv.removeChild(answersdiv.lastElementChild);
            for( ans of anslist){
                let but = document.createElement("button");
                but.style.width = "100%";
                but.style.textAlign = "center";
                but.style.padding = "10px";
                but.textContent=ans;
                but.onclick=check;
                answersdiv.appendChild(but);
            }        
        }
        function shuffle(a) {
            for (let i = a.length - 1; i > 0; i--) {
                const j = Math.floor(Math.random() * (i + 1));
                [a[i], a[j]] = [a[j], a[i]];
            }
            return a;
        }
        function check(e){
            pick = e.target;
            if( pick.textContent == correct ){
                pick.setAttribute("class", "correctanswer");
            } else {
                pick.setAttribute("class", "incorrectanswer");
            }
        }
        function next(){
            current += 1;
            if( current >= nofq){
                current = 0
            }
            console.log(current)
            update();
        }
    </script>
</head>
<body onload="init()">
    <h1>Quiz Baic 線上測驗 基礎版</h1>
    <div id="questiondiv"></div>    
    <div id="answersdiv"></div>
    <button id="next" onclick="next();">下一題</button>
</body>
</html>
```

延伸：紀錄答題次數以及答對次數。顯示出來。



接著，我們進行一點美化 (這個等 bootstrap 單元結束後，我們再回來！)

[bootstrap components](https://getbootstrap.com/docs/3.4/components/)

我們到這裡來找一些比較好看的版子來用。我選了一個 jambotron 以及 btn 來用。

範例 2 quiz_bootstrap.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz try 1</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://cdn.bootcss.com/jquery/3.2.1/jquery.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.bootcss.com/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.bootcss.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.11/lodash.min.js"></script>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <style>
       .header {
           text-align: center;
       }
    </style>
        <script>
            var results;
            var index = 0;
            var correct_answer;
            function init(){
                console.log("begin init()");
                axios.get("opentdb.data")
            //    axios.get("https://opentdb.com/api.php?amount=20&category=18&difficulty=medium&type=multiple")
                .then((resp)=>{results=resp.data.results; update();})
                .catch((err)=>{console.log(err)});
            }
    
            function update(){
                // console.log(results[index].question);
                $("#question").html(results[index].question);
                var ans = [];
                correct_answer = results[index].correct_answer;
                ans.push(correct_answer);
                for( inc of results[index].incorrect_answers){
                    ans.push( inc );
                }
                // shuffle(ans);
                ans = _.shuffle(ans);
                var str = '';
                for(an of ans){
                    str += '<button type="button" class="btn btn-light btn-md btn-block" onclick="ans(event)">'+an+'</button><br>'
                }
                $("#anss").html(str);
            }
            function ans(event){
                let pick = event.target;
                if( pick.innerHTML === correct_answer ){
                    pick.style.background = "lightgreen";
                }else{
                    pick.style.background = "orange";
                }    
            }
            function next(){
                index +=1;
                update();
            }
            function clearx(){
                $("#anss button").css("background" ,"white");
            }
            function shuffle(a){
                for(let i = 0; i < a.length; i++){
                    j = Math.floor(Math.random(a.length));
                    [a[i],a[j]]=[a[j], a[i]];
                }
            }
        </script>
</head>
<body onload="init()">
    <h1 class="header">資管金頭腦</h1>
    <div id="app">
        <div class="jumbotron">
                <p id="question" class="lead question">Question</p>
                <hr class="my-4">
                <p id="anss">Answers</p>
                <a class="btn btn-primary btn-md" href="#" role="button" onclick="clearx()">Submit</a>
                <a class="btn btn-success btn-md" href="#" role="button" onclick="next()">Next</a>
              </div>
    </div>
</body>
</html>
```

- 為了練習不同的 ajax, 本範例改用 axios 。

- 另外，https://opentdb.com/api.php?amount=20&category=18&difficulty=medium&type=multiple 是一個提供一些廣泛百科題庫的 opensource ，同學可以試試看。

- 除了 axios 之外，這個範例中我加入了  lodash 程式庫，這是一個被廣泛使用的數學函式庫，陣列的 shuffle 我寫了一個，也可以直接使用 lodash 提供的 _.shuffle( arr );

  ```html
  <script src="https://cdn.jsdelivr.net/npm/lodash@4.17.11/lodash.min.js"></script>
  ```

