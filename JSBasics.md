---
title: JSBasics 基本語法
date: 2020-03-25 18:03:45
tags:
- javascript
- basics
categories:
- Javascript
---

# Javascript Basics (基本語法)

Javascript 在網頁上可以用內嵌的方式執行。

```html
<!doctype html> 
<html>
  <head>
    <meta charset="utf-8">
	<title>第一個JavaScript程式</title>
  <script>
     alert("Hello World!");
  </script>
  </head>	
  <body>    
  </body>
</html>
```

在 script 中所夾的就是 javascript 的程式碼。

為了方便開發，瀏覽器也提供一個 console (瀏覽器主控臺) 。我們可以點擊瀏覽器右上的選項，找到 更多工具/開發人員工具。

chrome 的開發人員工具提供不只 console 的支援，在開發前端時很好用！

Javascript 語法介紹，我們先借用 Chrome 的 開發人員工具。

## 程式碼慣用

英文大小寫有區分

空白字元一個以上會略過

結尾加分號

註解符號 // 或是 /*   */

## 型別

JS 型別分為二種，基本型別及物件型別。

基本型別包括：數值、布林、字串；Null, Undefined, Symbol 以及 BigInt。

物件型別包括：陣列、函式、物件、日期、規則表示(RegExp)。

### 數值

我們可以隨意使用諸如123、-456、78.9、-12345.6789 等十進位數值，其型別均為number，注意不要超過 -2 <sup>1024</sup> ~ 2<sup>1024</sup> (-10<sup>307</sup> ~ 10<sup>307</sup> ) 的範圍即可。JavaScript 提供了下列幾個特殊的數值(Symbol)：

NaN

Infinity

-Infinity

JavaScript 的數值是「雙精確度 64 位元格式 IEEE 754 值」(“double-precision 64-bit format IEEE 754 values”)。這能造成一些有趣的後果。JavaScript 沒有所謂的整數，所以你在做算術的時候得小心一點，尤其是假如你習慣了 C 或 Java 的數學。

### BigInt

BigInt 基本上是 Number 型別，但允許任意大的準確度。使用上可以在數值後面加上n 或是使用建構子產生數值。

```
> const x = 2n ** 53n;  // 在console 看結果
> const z = BigInt( 135870 );
```

### 布林

true 或 false

### 字串

使用單引號或雙引號，字串相連使用 + 。有時會用到反單引號 ` 。

### Symbol

Symbol 是唯一且不可變動的基本數值，Symbol 可用來作為 Object 的 key 值。

### 型別轉換

let x = parseInt("123", 10);   // 字串轉數值

不指定基底時，會依據表示進行不同的基底轉換

let y1 = parseInt("023");   // 八進位

let y2 = parseInt("23");   // 十進位

let y3 = parseInt("0x23");   // 十六進位

let f = parseFloat("23.4"); // 有小數時，請使用 float。

### 日期

```
let today = new Date()
let birthday = new Date('December 17, 1995 03:24:00')
let birthday = new Date('1995-12-17T03:24:00')
let birthday = new Date(1995, 11, 17)
let birthday = new Date(1995, 11, 17, 3, 24, 0)
```

Date.parse(), Date.UTC()

[Date](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date)

## 變數

Javascript 的變數命名與 C++ 相同。Javascript 變數可以不必另外宣告，因此可以直接使用：

```
myName = "聯大";
```

但有時為了 scope, 也會作宣告，此時可以使用三種方式：

```
var numx;
let x;
const y;
```

var 與 let 意思相近，let 的 scope 是方塊，也就是 {  } 內的區域。 var 的 scope 是函式。

當我們不在 { }  內 或是 function 內 使用 let 或是 var ，則此變數會是 global。

若我們沒有使用 let 或是 var 或是 const 時，所宣告的變數均視為 global 變數。例如：

```javascript
function fun1(){
    msg = 'global msg!';
    console.log( msg );
}
fun1();
alert( msg );   // msg 可以使用！
```

const 是常數的宣告，const 的 scope 是方塊，這一點跟 let 是一樣的。

const 變數的值不能更動，但是若這個常數是一個物件的參照，物件內容是可以更動的！也就是說，這個參照不能更改成指到另一個物件，但物件內部的成員是可以更動的。

```
const v = {};
v.a = 100;
v.b = 200;
v = {a: 1, b:2};   // 錯誤
```

## 運算子

與 C++ 大部分相同。

=== , !=== 與 PHP 相同。

`>>>` 向右無號移位。

typeof( x ) 取得型別。   number, boolean, string, object, array, function, ...

## 控制流程

if

if else 

switch

for

while

do

for( of )    // 陣列 或 物件 (JSON)

for( in )   // 陣列 或 物件(JSON)

## 函式

### 內建函式

eval(exp)

isFinite( x )

isNan( x )

parseInt( "numberString")

parseFloat("numberString")

Number("string")

String( Object )

encodeURI

decodeURI

encodeURIComponent

decodeURIComponent

另外許多函式定義在個別的內建類別內：[內建函式](https://www.tutorialspoint.com/javascript/javascript_builtin_functions.htm) 。

內建類別：

- [Number](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number)
- [String](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String)

- Boolean

- Array

- Date

- [Math](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math)

- [RegExp](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp) 正則表示式 


### 自訂函式

```javascript
function funcName( parlist ){
    statements;
    return or no return;
}

function add(x, y) {
    var total = x + y;
    return total;
}
```

[more about functions](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/A_re-introduction_to_JavaScript#.E5.87.BD.E5.BC.8F
) 概念延伸說明。

匿名函式可以是一個物件，若由一個變數來參照，變數名可以作為函式名稱呼叫。

```javascript
var myFun = function( a, b){ return a + b;};
myFun(100, 20);
```

myFun 是一個函式，但也是一個物件。

### 回呼函式(Callback Function)

JS 常使用 callback 函式參數，當一個函式結束時，呼叫 callback 函式。一般函式的返回值會做為 callback 的第一個參數輸入。例如：

```javascript
document.addEventListener(“EventListened”, callbackfunction);
function callbackfunction(){
     createCanvas(700, 400);
     mic = p5.AudioIn();
}

```

## 陣列

Javascript 的陣列沒有規定一定要相同型別。

```
let x = [];
let y = [1,3,4,5,6];
let z = [1,3,'apple0', y];
let a = [x, y, z];
y[10] = 101;
y['seven'] = 7;
for( let v in y ) console.log(v);

```

進階的陣列介紹，另外。

## 物件

JSON (Java Simple Object Notation) 可以說是 Javascript 版的 associative array 。不過在Javascript 我們稱為物件。

```
let ox = {
   'name': "John",
   'year': 100
}
```

ox 是一個物件，資料成員有 name, year 。

ox.name = " ";  ox.year = "  ";

ox["name"] 等同於 ox.name

如果 key 是整數，那就必須使用陣列索引來參照，

ox[1] = 'apple';

ox.1  //  嚴重錯誤

```javascript
oox = { 1: 100, name:"BBC", "age": 60, 5: 'apple'};
{1: 100, 5: "apple", name: "BBC", age: 60}
```

oox[1] OK 但是 oox.1 不可以。

oox[5] OK 但是 oox.5 不可以。

oox.name, oox['name'] 都可以使用。

oox.age, oox['age'] 都可以使用。

在一個物件中，陣列可以作為一個 value。

一個陣列裡，物件可以是一個元素。

```
ar = [1,3,5];
bo = {a: 100, b: 'banana', c: ar};
cr = [1,3, 'apple', bo];
```

### 函式也是一個物件

一個函式當作物件傳遞時，不需加括號() 。呼叫時則需要加上括號。

### 類別與自訂類別

[自訂類別說明](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/A_re-introduction_to_JavaScript#.E8.87.AA.E8.A8.82.E7.89.A9.E4.BB.B6) 簡要說明

[物件導向類別定義](https://developer.mozilla.org/zh-TW/docs/Web/JavaScript/Introduction_to_Object-Oriented_JavaScript) 定義說明

依照函式及簡單物件的說明，我們可以設計一個函式，讓這個函式返回一個物件，如此一來，這個函式就會有建構子的味道：

```javascript
function mkStudent(n, c, id){
    return {
        name: n,
        class: c,
        id:id
    }
}

let nobj = mkStudent( 'john', 'im07', 733000);
```

雖然可以，但不好管理及後續延伸。通常寫成

```javascript
function mkStudent(n, c, id){
    this.name = n;
    this.class = c;
    this.id = id;
}

let nobj = new mkStudent('john', 'im07', 733000);
```

這種物件導向稱之為 prototype based 。

要定義成員函式，有二種方法：

```javascript
function mkStudent(n, c, id){
    this.name = n;
    this.class = c;
    this.id = id;
    this.fst = function(){
        return this.name+' '+this.id+')';
    }
}

let nobj = new mkStudent('john', 'im07', 733000);
nobj.fst();
```

```javascript
function mkStudent(n, c, id){
    this.name = n;
    this.class = c;
    this.id = id;
}
let nobj = new mkStudent('john', 'im07', 733000);
myStudent.prototype.fst = function(){
    return this.name+' '+this.id+')';
}
nobj.fst();
```

我們可以隨時對作為類別雛形的函式，增加新的成員函式或是資料成員。

ES6 之後，提供類似 C++ 建立類別的方式。

### 反單引號的使用

JS 中我們有時會需要將資料放在一個字串中，通常的寫法是：

```
const value = 137;
const echo = "The value is " + value +", and we want ...";

const echo = `The value is ${value}, and we want ...`;
```

原則上反單引號也是一個引號，但比單引號多一個能夠解析變數的能力。

