---
title: Application Todo-list
date: 2020-04-22 10:00:00
tags:
- ajax
- Application
- API
categories:
- Application
---

在範例中，我使用了三個 init 函式。通常網頁載入需要時間，因此我們的 js 程式要等網頁載完之後才開始工作，因此程式碼需要寫在 onload 裡面。我準備了三個分別是 init, init2 以及 init3 。分別針對 fetch 以及 axios 。fetch 示範一個 local 一個 遠端。

### App ToDoList

todo.html

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>待辦事項管理</title>
    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
      // 程式碼在 todo.js 
      <script src="todo.js"></script>
      <link rel="stylesheet" href="todo.css">
  </head>
  <body onload="init2()">
     <h1>Todo list</h1>
     <div class="d-flex justify-content-center">
       <div class="todo">
          <ul id="todos">
            <li>
              <input type="checkbox" name="" value="" onchange="toggle(event)">
                Lerning JavaScript</li>
            <li><input type="checkbox" name="" value="" onchange="toggle(event)">
                Lerning CSS</li>
          </ul>
          <form class="" action="javascript:addtodo()" method="post">
            <input id="newitem" type="text" name="" value="">
            <input type="submit" name="" value="新增事項">
          </form>
              </div>
              <div class="content">
                <h2>content</h2>
          </div>
     </div>
  </body>
</html>
```

todo.js

```javascript
// 使用 fetch 本機資料
function init(){
  fetch("todos.data")
  .then((resp)=>{return resp.json()})
  .then((resp)=>{resp.map(x=>{
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.name;
    })});
}
// 使用 fetch 遠端
function init2(){
  fetch("https://jsonplaceholder.typicode.com/todos")
  .then((resp)=>{return resp.json()})
  .then((resp)=>{resp.map(x=>{
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    if( x.completed === false)
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.title;
    else
    el.innerHTML="<input type='checkbox' onchange='toggle(event)' checked>"+x.title;
    
    })});
}
// 使用 axios
function init3(){
  axios.get("https://jsonplaceholder.typicode.com/todos")
  .then((resp)=>{resp.data.map(x=>{  
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    if( x.completed === false)
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.title;
    else
    el.innerHTML="<input type='checkbox' onchange='toggle(event)' checked>"+x.title;
    
    })});
}
function addtodo(){
  <!-- alert("hello"); -->
  var newitem = $("<li></li>")
      .html("<input type='checkbox' name='' value='' onchange='toggle(event)'>"
             +$("#newitem").val()
            );
  $("ul").append(newitem);
  $("#newitem").val("");
}
function toggle(event){
   if($(event.target).prop("checked") === true)
      $(event.target).parent().addClass("completed");
   else
      $(event.target).parent().removeClass("completed");
}
```

todo.css

```css
li {
  list-style: none;
}
.completed{
  text-decoration: line-through;;
}
.todo {
  width: 400px;
}
.content{
  width: 600px;
  background-color: lightyellow;
}
```

todos.data

```json
[
    {
    "name" : "Learning JS",
    "checked" : true
    },
    {
    "name" : "Learning Bootstrap",
    "checked" : false
    },
    {
    "name" : "Learning Vue",
    "checked" : false
    }
]
```

