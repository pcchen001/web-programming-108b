---
title: CSS Bootstrap
date: 2020-05-6 08:07:53
tags: 
- css
- boostraap
categories:
- CSS
---

Bootstrap 最被普及使用的 RWD CSS 排版工具。

官網： https://getbootstrap.com/

建議的初始模板：

```html
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    <h1>Hello, world!</h1>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  </body>
</html>
```

Bootstrap 以 mobile 為優先，其次大螢幕。

viewport 的設定是為了　mobile 裝置的比例。當我們設定 width=device-width, 手機瀏覽器直立時和橫擺時 width 會不同。不然就會都是以橫擺的寬度作為 width 。

Bootstrap box-sizing 使用 border-box 而非 content-box.  此舉可能對 google Map 造成影響。(content-box 在 border-box 內，差了 padding的大小。)

Bootstrap 使用 reboot 來應對不同瀏覽器及不同裝置間顯示的不一致性。

Bootstrap 使用一點 jquery，也提供一點簡潔的 jquery 類型的函式呼叫。

Bootstrap 包含 scss 變數，提供方便的主題設計來進行主題或元件切換。

## Container

container 用來作為排版的容器。大部分的情況不要在 container 巢狀引入另一個 container 。

container 分三種：

- `.container`, which sets a `max-width` 每一個分隔點

- `.container-fluid`, which is `width: 100%` at all breakpoints

- `.container-{breakpoint}`, which is `width: 100%` until the specified breakpoint

  e.g. .container-sm 或是 .container-md 或是 .container-lg 或是 .container-xl 。

  breanpoint:   sm, md, lg, xl 四塊。 sm  用在手機橫式。 md 用在平板。 lg, xl 用在螢幕。
  
  大部分情況，我們只使用 .container 。

## Grid

```css
<div class="container">
  <div class="row">
    <div class="col-sm">
      One of three columns
    </div>
    <div class="col-sm">
      Second one of three columns
    </div>
    <div class="col-sm">
      Third ne of three columns
    </div>
  </div>
</div>
```

基本款。比 sm 小的螢幕時，會排成一直列。否則橫排。也就是會在窄螢幕時折疊。

如果是 col-md 則在螢幕寬度小於 md 大小時，就會摺下來。　col-lg 以及 col-xl　以此類推。

我們可以想像，大部分的情況，我們預期手機直立時會改變，就要使用 col-sm 。

基本工作原理：

- .container 置中，補上留白。
- .row  橫排，不夠寬的時候，把後面的 .col 折疊　下來。
  - Each column has horizontal `padding` (called a gutter) for controlling the space between them. This `padding` is then counteracted on the rows with negative margins. This way, all the content in your columns is visually aligned down the left side.
- .col 直列，寬度會自動平均分配。
- .col-sm 直列，寬度自動平均分配，但螢幕寬度低於 sm 時，摺疊下來。
- .col-md 直列，寬度自動平均分配，但螢幕寬度低於 md 時，摺疊下來。
- .col-gl 及 col-xl 比照。
- 若需要有不同的比例時，使用 12 格制。
- .col-4 直列，寬度　4/12 = 33%。 .col-6　直列，寬度 6/12= 50%。不會自動折疊下來。
- 若需要有不同的比例，又要在窄螢幕時摺疊下來，那麼就使用類似　.col-sm-4 的寫法。
- .col-sm-4 直列，寬度 4/12 = 33%。且螢幕寬度小於 sm 時會折疊下來。
- .col-md-6 直列，寬度 6/12 = 50%。且螢幕寬度小於 md 時會折疊下來。
- .col-gl-n 及 .col-xl-n 比照。
- 直列的水平 padding 會自動補上。若不要這個自動的 padding, 可以在橫列的 class 中加上 .no-gutters 。
- sm, md, lg, xl 代表的寬度界線預設為 540px, 720px, 960px 以及 1140px。
- 直列的寬度既不要平均寬度，也不要固定給比例時，換句話說就是希望能依據內容自動訂出比較洽當的寬度，此時可以使用 col-sm-auto 。 md, lg, xl 比照。



## w-100 及 h-100

.w-100 表示寬度 100%, .w-20 表示寬度 20%

.h-50 表示高度 50%, h-20 表示 高度 20%

下面的範例加入了一個寬度 100% 的 div 。有將之後的 div 折疊下來的效果。

```
<div class="container">
  <div class="row">
    <div class="col">col</div>
    <div class="col">col</div>
    <div class="w-100"></div>
    <div class="col">col</div>
    <div class="col">col</div>
  </div>
</div>
```



col-{sepcification}-auto 用來表示寬度由內容來決定。　b02.html

```html
<div class="container">
  <div class="row justify-content-md-center">
    <div class="col col-lg-2">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
  <div class="row">
    <div class="col">
      1 of 3
    </div>
    <div class="col-md-auto">
      Variable width content
    </div>
    <div class="col col-lg-2">
      3 of 3
    </div>
  </div>
</div>
```

lg 以上，長度固定，lg 以下，平均寬度。

響應設計最常見的就是設計小於 sm 要摺疊，大於 sm 時改為水平排列。此時，使用下列範例：

```html
<div class="container">
  <div class="row">
    <div class="col-sm-8">col-sm-8</div>
    <div class="col-sm-4">col-sm-4</div>
  </div>
  <div class="row">
    <div class="col-sm">col-sm</div>
    <div class="col-sm">col-sm</div>
    <div class="col-sm">col-sm</div>
  </div>
</div>
```

sm 以上 水平排列，寬比 8/4 。 sm 以上水平排列，寬比 平均。

我們經常讓一個 div 有多個 class, 依照符合的寬度來分配：

```html
<div class="container">
  <!-- mobile 時 折疊，第一個全寬，第二個半寬 -->
  <div class="row">
    <div class="col-md-8">.col-md-8</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- mobile 時一個 50% 寬，所以會有一個摺疊下來，md 以上則改為 33.3% 個平均寬 -->
  <div class="row">
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
    <div class="col-6 col-md-4">.col-6 .col-md-4</div>
  </div>

  <!-- 固定 50% 寬, on mobile and desktop -->
  <div class="row">
    <div class="col-6">.col-6</div>
    <div class="col-6">.col-6</div>
  </div>
</div>
```

重點： col 表示 平均, col-6 表示mobile 時 12 格的6格。 col-sm-4 表示當不再是 mobile 時，改為 4格。依此類推。

### Gutters (間隙) 

與 padding 及 margin 有關的設定。　間隙大小 1~5, n1~n5 。

.px-sm-5 表示 水平paddings 在 sm 以上時，設定為 5 。.px-sm-n5 表示 padding 設定為 -5。

.py-sm-5 表示 垂直 paddings 在 sm 以上時，設定為 5 。

.mx-sm-2 表示 margins 在 sm 以上，設定為 2。 

.my-sm-2 表示 垂直 margins 的設定。

範例 b02.html

```html
  <h2>Gutters Demo </h2>
  <div class="container px-lg-5">
    <div class="row mx-lg-n5">
      <div class="col py-5 px-lg-5 border bg-light">Custom column padding</div>
      <div class="col py-3 px-lg-5 border bg-light">Custom column padding</div>
    </div>
  </div>
```

程式註：　border 會幫我們加上邊框。 bg-light 給淡灰色的背景色。

### Row columns

除了我們使用寬度設定來設計排版之外，也可以使用版塊數目來進行排版。基本形式為設定一個 Row 包含幾個 col， 超過就折到下一排。

```html
<div class="container">
  <div class="row row-cols-2">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>
```

row-cols-2 一排二個。

可以對不同尺寸定不同數目：　b02.html (進入開發者模式觀看！)

```html
<div class="container">
  <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
    <div class="col">Column</div>
  </div>
</div>
```

mobile 一排一個， sm 以上  一排二個， md 以上 一排 4個。很實用對不對！

更多有關 grid 的功能，請查閱：https://getbootstrap.com/docs/4.4/layout/grid/

# Content 內容渲染

Bootstrap 對於內容的呈現提供了不少工具：包含 Typography, Code, Images, Tables 及 Figures 等。這些工具都是透過 class 的設定來進行的。

https://getbootstrap.com/docs/4.4/content/reboot/　bootstrap 把許多原來預設的 html 渲染都給重新換上新裝， h1 ~ h6, list, table, 等等都有基本上的更改，而更多的小工具就要另外查看（如下）

https://getbootstrap.com/docs/4.4/content/typography/　字形支援

https://getbootstrap.com/docs/4.4/content/images/　影像放置支援

https://getbootstrap.com/docs/4.4/content/tables/　表格顯示支援

# Components

Bootstrap 提供了好用的 components：最好就是來這裡找好用的片段，複製過去用。

- [Alerts](https://getbootstrap.com/docs/4.4/components/alerts/) 提醒框 ( 類似的還有：Modal, popover, toast)
- [Badge](https://getbootstrap.com/docs/4.4/components/badge/) 徽章 (小圓紐上有文字，通常只有一個數字)
- [Breadcrumb](https://getbootstrap.com/docs/4.4/components/breadcrumb/)
- [Buttons](https://getbootstrap.com/docs/4.4/components/buttons/)
- [Button group](https://getbootstrap.com/docs/4.4/components/button-group/)
- [Card](https://getbootstrap.com/docs/4.4/components/card/)
- [Carousel](https://getbootstrap.com/docs/4.4/components/carousel/)
- [ Collapse](https://getbootstrap.com/docs/4.4/components/collapse/) 收放　b03.html
- [Dropdowns](https://getbootstrap.com/docs/4.4/components/dropdowns/) 拉放 b03.html
- [Forms](https://getbootstrap.com/docs/4.4/components/forms/) 表單設計
- [Input group](https://getbootstrap.com/docs/4.4/components/input-group/)
- [Jumbotron](https://getbootstrap.com/docs/4.4/components/jumbotron/) 這個也很好用，大型選單。
- [List group](https://getbootstrap.com/docs/4.4/components/list-group/)
- [Media object](https://getbootstrap.com/docs/4.4/components/media-object/)
- [Modal](https://getbootstrap.com/docs/4.4/components/modal/) 互動視窗 b04.html
- [Navs](https://getbootstrap.com/docs/4.4/components/navs/) b03.html
- [Navbar](https://getbootstrap.com/docs/4.4/components/navbar/) b03.html
- [Pagination](https://getbootstrap.com/docs/4.4/components/pagination/) 分頁索引
- [Popovers](https://getbootstrap.com/docs/4.4/components/popovers/) b04.html
- [Progress](https://getbootstrap.com/docs/4.4/components/progress/)
- [Scrollspy](https://getbootstrap.com/docs/4.4/components/scrollspy/)  捲動定位　b05.html
- [Spinners](https://getbootstrap.com/docs/4.4/components/spinners/)　等待動畫
- [Toasts](https://getbootstrap.com/docs/4.4/components/toasts/)　另一種 alert
- [Tooltips](https://getbootstrap.com/docs/4.4/components/tooltips/)　b06.html

# Utilities

最後就是一些好用的造型，排版工具

- [Borders](https://getbootstrap.com/docs/4.4/utilities/borders/) 邊框造型
- [Clearfix](https://getbootstrap.com/docs/4.4/utilities/clearfix/) 浮動排版工具
- [Close icon](https://getbootstrap.com/docs/4.4/utilities/close-icon/) 打叉叉圖示
- [Colors](https://getbootstrap.com/docs/4.4/utilities/colors/)　text-primary, ... bg-primary, 
  - (primary藍, secondary灰, success綠, danger紅, warning黃, info青, light淡灰, dark, white )
- [Display](https://getbootstrap.com/docs/4.4/utilities/display/)　.d-{ value }　所有 display 的模式都可以透過 class 設定來達成。
- [Embed](https://getbootstrap.com/docs/4.4/utilities/embed/)
- [Flex](https://getbootstrap.com/docs/4.4/utilities/flex/) Flex 排版。這裡包含所有 flex 排版的功能！
- [Float](https://getbootstrap.com/docs/4.4/utilities/float/) 文繞圖排版
- [Image replacement](https://getbootstrap.com/docs/4.4/utilities/image-replacement/)
- [Overflow](https://getbootstrap.com/docs/4.4/utilities/overflow/)
- [Position](https://getbootstrap.com/docs/4.4/utilities/position/)
- [Screen readers](https://getbootstrap.com/docs/4.4/utilities/screen-readers/)
- [Shadows](https://getbootstrap.com/docs/4.4/utilities/shadows/)
- [Sizing](https://getbootstrap.com/docs/4.4/utilities/sizing/)　h-auto, h-1~h-100,  w-auto, w-1 w-100
- [Spacing](https://getbootstrap.com/docs/4.4/utilities/spacing/) px, py, pt, pb, pl, pr, mx, my, mt, mb, ml, mr-1~mr-5, mr-auto
- [Stretched link](https://getbootstrap.com/docs/4.4/utilities/stretched-link/)
- [Text](https://getbootstrap.com/docs/4.4/utilities/text/) text-left, text-right, text-center
- [Vertical align](https://getbootstrap.com/docs/4.4/utilities/vertical-align/)
- [Visibility](https://getbootstrap.com/docs/4.4/utilities/visibility/)