---
title: Vue basics
date: 2020-05-13 08:00:00
tags:
- Vue
- framework
categories:
- Vue
---

基礎的 Vue 元件使用，使用 CDN

```html
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.21/dist/vue.js"></script>
```

Vue 是一個前端框架。

JS 撰寫前端程式時，資料不容易有封包性及結構性，在開發大型程式時不好管理，因此若能充分地將元件物件化，可以改善程式的結構性，便於大型應用的管理。

先看一個範例：

範例 1 

```html
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title></title>
    <!-- development version, includes helpful console warnings -->
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
    <script>
      function init(){
          var app = new Vue({
          el: '#app',
          data: {
            message: ' 歡迎來到 Vue 世界',
            name: 'Pochi Chen',
            titlemessage: '滑鼠移來時，另外顯示的內容',
            tcolor: 'background: lightgreen'
          }
        });
      }
    </script>
  </head>
  <body onload=init();>
    <div class="" id="app">
      <h1>{{name}}</h1>{{message}}
      <p v-bind:title="titlemessage">請將滑鼠移到這裡，demo v-bind:title </p>
      <p v-bind:style="tcolor">本段內容的色彩依據 vue 物件的資料成員 tcolor</p>
    </div>
  </body>
</html>
```

一個 Vue 的物件是由參數來定義的： el: '#app' 這個 Vue 物件綁上元素 #app。資料成員定義在 data:{} 裡面。資料成員不必一定要是字串，可以是數值、布林、陣列、甚至是 JSON物件。

在被綁定的元素內，**雙大括號**用來指涉對應的資料名稱。例如第24行的 name 及 message。

```
雙大括號 又稱為 鬍鬚指令 mustache, 例如 {{name}}
```

資料成員也可以出現在元素的 attribute 內，但是為了要區別是原來的 attribute 或是可以使用 vue 的attribute, 我們必須在可以使用 vue 的屬性前加上 v-bind: 例如 v-bind:title 就是讓原來的 title 屬性綁上了 vue ，如此一來，屬性值就會是對應到 vue 物件的資料成員。

在上面的範例中，v-bind:title 跟 v-bind:style 就是將原來的屬性綁上 vue 資料的。

這個綁定提供了我們對於頁面操作的豐富可能，我們透過對 vue 資料的處理，就可以更動頁面上的表現及內容。

**注意**： v-bind: 可以省略成只有 : 。例如 v-bind:title 可以只寫成 :title 。

被綁定的元素內的所有子元素，都可以使用綁定的 vue 物件。元素的屬性除了原來的屬性之外，有二個讓元素也可以具備程式控制流程功能的屬性： v-if 以及 v-for 。

### v-if

這個元素根據條件，出現或不出現。

```html
<div id="app-3">
  <span v-if="seen">Now you see me</span>
</div>

```

```javascript
var app3 = new Vue({
  el: '#app-3',
  data: {
    seen: true
  }
})
```

### v-for

```html
<div id="app-4">
  <ol>
    <li v-for="todo in todos">
      {{ todo.text }}
    </li>
  </ol>
</div>
```

```js
var app4 = new Vue({
  el: '#app-4',
  data: {
    todos: [
      { text: 'Learn JavaScript' },
      { text: 'Learn Vue' },
      { text: 'Build something awesome' }
    ]
  }
})
```

在畫面上你會看到有 三個 li 元素。

執行時，如果我們在 console 輸入 app4.todos.push({'text': 'Wanderful Experiance'}) 那麼頁面列表會多出一條。

Vue 物件除了資料成員之外，最重要的是可以定義成員函式。

另外，元素的事件處理屬性如  onclick, onmouseover 等，若要改成呼叫 vue 的成員函式，要改寫成 v-on:click 或是 v-on:mouseover 。

**注意**：v-on:event 可以簡寫成 @event 。例如 v-on:click="m1" 可以簡寫成 @click="m1"。

```html
<div id="app-5">
  <p>{{ message }}</p>
  <button v-on:click="reverseMessage">Reverse Message</button>
</div>
```

```js
var app5 = new Vue({
  el: '#app-5',
  data: {
    message: 'Hello Vue.js!'
  },
  methods: {
    reverseMessage: function () {
      this.message = this.message.split('').reverse().join('')
    }
  }
})
```

 Vue 物件的成員函式定義在 methods: 下。要使用資料成員必須使用 this. 例如 this.message 。

目前為止，我們介紹了新的元素屬性，綁上 vue 的 v-bind:attr, 事件處理的 v-on:event, 控制流程的 v-if, v-for 。除此之外，還有一個專門用在 input 元素的屬性 v-model 。這個屬性可以把這個元素取得的 value 對應到 vue 的資料成員上。

```html
<div id="app-6">
  <p>{{ message }}</p>
  <input v-model="message">
</div>
```

```js
var app6 = new Vue({
  el: '#app-6',
  data: {
    message: 'Hello Vue!'
  }
})
```

 現在請各位將剛才的幾個範例寫在一個 html 檔案中，檢視你的頁面。

## 使用元件來組成

Vue 使用 Vue.component('componentName', { ... }); 來定義元件。我們用範例來說明：

```js
// Define a new component called todo-item
Vue.component('todo-item', {
  template: '<li>This is a todo</li>'
})

var app = new Vue(...)
```

Now you can compose it in another component’s template:

```html
<ol>
  <!-- Create an instance of the todo-item component -->
  <todo-item></todo-item>
</ol>
```

元件也可以說是一個元素模板，相當於定義一個新的元素型態。我們可以將一個內容結構定義成一個元件，可以被重複使用。

既然當作元素使用，就可以定義屬於這個元素的屬性，這個屬性在使用時，也是要綁定的，也就是需要 v-bind: 。

```html
<div id="app-7">
  <ol>
    <!--
      在app7中，我們定義了 groceryList。是一個陣列
      我們使用 v-for 進行迴圈，每一個對應到 todo 
      針對 v-for 會有一個 :key 紀錄索引
    -->
    <todo-item
      v-for="item in groceryList"
      v-bind:todo="item"
      v-bind:key="item.id"
    ></todo-item>
  </ol>
</div>

```

```js
Vue.component('todo-item', {
  props: ['todo'],
  template: '<li>{{ todo.text }}</li>'
})

var app7 = new Vue({
  el: '#app-7',
  data: {
    groceryList: [
      { id: 0, text: 'Vegetables' },
      { id: 1, text: 'Cheese' },
      { id: 2, text: 'Whatever else humans are supposed to eat' }
    ]
  }
})
```

參考 (v01_many.html)

通常我們會為我們的網頁設計幾個元件，然後應用這些元件組成網頁應用。

Vue 的成員函式，除了 methods: 之外，還有 computed: 以及 watch: ，我們用下面的範例做說明：(v02.html)

```html
    <div class="" id="w1">
      <p>firstName: {{firstName}} </p>
      <p>lastName: {{lastName}}</p>
      <p>fullName: {{fullName}}</p>
      <input v-model="change" type="text" name="" value="">
    </div>
    <script type="text/javascript">
      var vw = new Vue({
        el: '#w1',
        data: {
          firstName: "Pochi",
          lastName: 'Chen',
          change: ''
        },
        computed: {
          fullName: {
            get: function(){
              return this.firstName+" "+this.lastName;
            },
            set: function(val){
              var names = val.split(" ");
              this.firstName = names[0];
              this.lastName = names[names.length-1];
            }
          }
        },
        watch: {
          change: function(){
            this.fullName = this.change;
          },
          firstName: function(val){
            this.fullName = val + ' ' +this.lastName;
          },
          lastName: function(val){
            this.fullName = this.firstName+' '+val;
          }
        }
      })
    </script>
```

computed 的函式，相當於 set 及 get 的 magic function, 是針對合成/計算變數設計的函式。上例中，我們定義了 fullName 這個 computed 的資料成員，其內容由 get 及 set 來定義。使用時與資料成員相同。

watch 的函式定義在資料成員上，一但資料成員有了變動，有需要連動的定義在這裡。[這裡有一個好玩的範例](https://vuejs.org/v2/guide/computed.html)

Vue 也允許使用一種叫做 filter 的函式，這種函式主要用來處理文字格式，例如大小寫，數字前面加上$ 等。filter 的函式可以被管線化 pipe 接續處理。例如下面的範例，capitalize 是一個 filter函式， message 通過capitalize 進行格式處理。我們可以使用 雙大括號 或是 v-bind:attr 使用。

**注意**：雙大括號 在 Vue 英文稱之為 mustache 鬍鬚指令。

```js
<!-- in mustaches -->
{{ message | capitalize }}

<!-- in v-bind -->
<div v-bind:id="rawId | formatId"></div>
```

我們可以在 Vue 物件內定義:

```js
filters: {
  capitalize: function (value) {
    if (!value) return ''
    value = value.toString()
    return value.charAt(0).toUpperCase() + value.slice(1)
  }
}
```

也可以全域定義：

```js
Vue.filter('capitalize', function (value) {
  if (!value) return ''
  value = value.toString()
  return value.charAt(0).toUpperCase() + value.slice(1)
})

new Vue({
  // ...
})
```

複習一下：成員函式分為 一般的 methods, 計算性的 computed, 監察變化的 watch 以及格式處理的 filters。

### v-if v-else

v-if 可以搭配 v-else

```html
    <div class="static" v-bind:class="[activeClass,  errorClass]" id="c1">
      <p>This div has class static and dynamic class error</p>
    </div>
    <script type="text/javascript">
      var vb = new Vue({
        el: '#c1',
        data: {
          isActive: false,
          isError: true,
          classobj: {active: true, error: false},
          activeClass: 'active',
          errorClass: 'error'
        }
      });
    </script>
    <div class="" id="sty">
      <template v-if="gok" id="">
        <span :style="styobj">Hello World色彩與大小</span>
        <h2 v-if="ok">Yes ok</h2>
        <h2 v-else>No it is else</h2>
      </template>
    </div>
    <script type="text/javascript">
      var vs = new Vue({
        el: '#sty',
        data: {
          stycolor: 'red',
          stysize: 24,
          styobj: {
            color: 'green',
            fontSize: '30px'
          },
          ok: true,
          gok: true
        }
      })
    </script>
```

### class & style 屬性值

[class 及 style 的屬性常常會有一個以上的值，需要語法的支援來處理。](https://vuejs.org/v2/guide/class-and-style.html)

歸納幾個語法

```
<div v-bind:class="{ active: isActive }"></div>

<div
  class="static"
  v-bind:class="{ active: isActive, 'text-danger': hasError }"
></div>
如果
data: {
  isActive: true,
  hasError: false
}
那麼 class = "static active"
```

如下的寫法也行：

```html
<div v-bind:class="classObject"></div>

data: {
  classObject: {
    active: true,
    'text-danger': false
  }
}
```

陣列也行

```html
<div v-bind:class="[activeClass, errorClass]"></div>
如果
data: {
  activeClass: 'active',
  errorClass: 'text-danger'
}
```

相當於

```html
<div class="active text-danger"></div>
```

允許條件式寫法：

```html
<div v-bind:class="[isActive ? activeClass : '', errorClass]"></div>
```

若 isActive 為  true 則 activeClass 否則 空 。另外加上 errorClass 。

也可以寫成如下：

```html
<div v-bind:class="[{ active: isActive }, errorClass]"></div>
```