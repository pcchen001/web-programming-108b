---
title: Graphql 使用 node.js 
date: 2020-06-12 20:22:16
tags:
- Graphql
- node.js
- database
categories:
- Database
- Advanced
---



https://graphql.org/graphql-js/

使用 node.js 作為 GraphQL Server, 首先：

```bash
npm init
npm install graphql --save

```

寫個程式 server.js 

```javascript
var { graphql, buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};

// Run the GraphQL query '{ hello }' and print out the response
graphql(schema, '{ hello }', root).then((response) => {
  console.log(response);
});
```

If you run this with:

```bash
node server.js
```

You should see the GraphQL response printed out:

```javascript
{ data: { hello: 'Hello world!' } }
```

Congratulations - you just executed a GraphQL query!

以上是最單純的 graphql 查詢程式。然而我們的伺服器是要能接受網站的 request, 因此需要架上一個網站伺服器， node.js 最簡單的 Server 是使用 Express。

首先要先安裝：( graphql 已經安裝過了，可以不需要重複安裝)

The simplest way to run a GraphQL API server is to use [Express](https://expressjs.com/), a popular web application framework for Node.js. You will need to install two additional dependencies:

```bash
npm install express express-graphql graphql --save
```

修改後的程式碼如下，執行後提供了 http://localhost:4000/graphql 的端點服務：

```javascript
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    hello: String
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  hello: () => {
    return 'Hello world!';
  },
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at http://localhost:4000/graphql');
```

You can run this GraphQL server with: 當然還是得執行

```bash
node server.js
```

畫面應該會出現GraphiQL, 左邊是模擬查詢輸入，右邊是結果。我們可以先試試看：

```
{ hello }
```

查看結果： Hello world!

由於 configured `graphqlHTTP` with `graphiql: true`, 我們可以看到左右二欄的手動輸入畫面，設 graphiql: false 則不會有手動輸入介面。

It's also simple to send GraphQL from the browser. Open up http://localhost:4000/graphql, open a developer console, and paste in: 打開 console 輸入下列程式碼：

```javascript
fetch('/graphql', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
  body: JSON.stringify({query: "{ hello }"})
})
  .then(r => r.json())
  .then(data => console.log('data returned:', data));
```

You should see the data returned, logged in the console:

```undefined
data returned: Object { hello: "Hello world!" }
```

## 範例二

我們可以改寫 server.js 成為比較複雜一點的資料庫，允許送入引數進行查詢：

我們設計一個能提供幾面骰子以及幾個骰子查詢的伺服器:

```javascript
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    rollDice(numDice: Int!, numSides: Int): [Int]
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  rollDice: ({numDice, numSides}) => {
    var output = [];
    for (var i = 0; i < numDice; i++) {
      output.push(1 + Math.floor(Math.random() * (numSides || 6)));
    }
    return output;
  }
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at localhost:4000/graphql');
```

在這個伺服器中， root 定義的物件包含一個 method:  rollDice。 rollDice 包含二個參數 numDice, numSides ，計算出一個 numDice 長度的陣列作為返回。查詢語法為呼叫此 method, 代入引數：

查詢時，GraphQL 查詢語法為：

```javascript
{
  rollDice(numDice: 3, numSides: 6)
}
```

 `node server.js` and browse to http://localhost:4000/graphql you can try out this API. 畫面右欄輸入上述查詢，可得：

```json
{
  "data": {
    "rollDice": [
      6,
      3,
      5
    ]
  }
}
```

我們也可以開 console, 輸入下列程式碼：

```javascript
var dice = 3;
var sides = 6;
var query = `query RollDice($dice: Int!, $sides: Int) {
  rollDice(numDice: $dice, numSides: $sides)
}`;

fetch('/graphql', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
  },
  body: JSON.stringify({
    query,
    variables: { dice, sides },
  })
})
  .then(r => r.json())
  .then(data => console.log('data returned:', data));
```

我們將查詢使用 $ 語法代入，graphql 一樣會收到代入變數的查詢。 console 返回如下：

```
data returned: 
{data: {…}}
data:
rollDice: Array(3)
0: 4
1: 2
2: 2
length: 3
__proto__: Array(0)
__proto__: Object
__proto__: Object
```

## 範例三

我們再看一個伺服器範例： (Schema 包含了基本的三個型態 String, Float 以及 In 陣列。這是基本型別的範例。

```javascript
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type Query {
    quoteOfTheDay: String
    random: Float!
    rollThreeDice: [Int]
  }
`);

// The root provides a resolver function for each API endpoint
var root = {
  quoteOfTheDay: () => {
    return Math.random() < 0.5 ? 'Take it easy' : 'Salvation lies within';
  },
  random: () => {
    return Math.random();
  },
  rollThreeDice: () => {
    return [1, 2, 3].map(_ => 1 + Math.floor(Math.random() * 6));
  },
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at localhost:4000/graphql');
```

依樣執行 `node server.js` and 瀏覽器 http://localhost:4000/graphql you can try out these APIs.

查詢：

```
{ quoteOfTheDat, random, roolThreeDice }
```

## 範例四

物件型別的範例：

當我們希望查詢回來的不是數值或是字串(也就是基本型別)，而是希望資料以一個物件的形式表示，GraphQL 允許使用自訂型別。

在上上一個範例，我們使用一個只有一個method 的物件：

```javascript
type Query {
  rollDice(numDice: Int!, numSides: Int): [Int]
}
```

當我們查詢 rollDice 時，返回 [Int]。現在我們重新組織這個過程，將 rollDice() 改為 getDie() 返回一個物件 RandomDie 。

RandomDie 就是我們要設計的類別，這個類別的物件要能夠設定 numSides, 能夠返回 rollOnce 以及 roll({numRolls})。

```javascript
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  type RandomDie {
    numSides: Int!
    rollOnce: Int!
    roll(numRolls: Int!): [Int]
  }

  type Query {
    getDie(numSides: Int): RandomDie
  }
`);

// This class implements the RandomDie GraphQL type
class RandomDie {
  constructor(numSides) {
    this.numSides = numSides;
  }

  rollOnce() {
    return 1 + Math.floor(Math.random() * this.numSides);
  }

  roll({numRolls}) {
    var output = [];
    for (var i = 0; i < numRolls; i++) {
      output.push(this.rollOnce());
    }
    return output;
  }
}

// The root provides the top-level API endpoints
var root = {
  getDie: ({numSides}) => {
    return new RandomDie(numSides || 6);
  }
}

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000);
console.log('Running a GraphQL API server at localhost:4000/graphql');
```

程式中的 schema 包含自訂的 RandomDie 以及固定的 Query 。自訂的 RandomDie 使用 class 定義。 root 作為資料庫內容。

查詢：

```
{getDie(numSides: 6){
  rollOnce
  roll(numRolls: 3)
}}
```

這個範例的重點在於將返回的資料定義成一個物件，我們可以查詢物件的內容或是呼叫成員函式。

## Mutation 更動內容

```
type Mutation{
  setMessage(message: String): String
}
type Query{
   getMessage: String
}
```

Query 定義查詢， Mutation 定義更動或新增。

練習了四個範例之後，我們大概可以歸納設計 GraphQL 應用的步驟：定義 Schema, 完成 Schema 所需的函式或是資料。Schema 包含 type Query, type Mutation 以及 type 自訂型別。除此之外，還可以定義 input MessageInput。input 定義一個輸入的自訂型別。

資料更動或新增，必須定義在 type Mutation 中。

假設我們要寫一個可以新增，查詢訊息的介面，訊息包含 ID, 內容以及作者：

```
type Message{
    id:ID!
    content: String
    author: String
}
type Query {
    getMessage({id}) : Message
}
input MessageInput{
  content: String
  author: String
}
type Mutation{
    createMessage( input: MessageInput) : Message
    updateMessage( id: ID!, input: MessageInput): Message
}
```

其中 Message 為自訂型別：

```
class Message{
  constructor(id, {content, author}){
      this.id = id;
      this.content = content;
      this.author = author;
  }
}
```

作為資料庫，我們需要一個變數來存放所有資料：

```
var fakeDatabase = {};
```

資料的新增，修改，查詢，必須透過 root 才能完成：

```
var root = {
  getMessage: ({id})=>{
      if( !fakeDatabase[id]) throw new Error('無訊息: '+id);
      return new Message(id, fakeDatabase[id])
  },
  createMessage: ({input})={
      var id = require('crypto').randomBytes(10).toString('hex');
      fakeDatabase[id] = input;
      return new Message(id, input);
  },
  updateMessage: ({id, input}) => {
      if( !fakeDatabase[id]) throw new Error('無訊息: '+id);
      fakeDatabase[id] = input;
      return new Message(id, input);
  },
};
```

完整的 server.js 如下：

```javascript
var express = require('express');
var graphqlHTTP = require('express-graphql');
var { buildSchema } = require('graphql');

// Construct a schema, using GraphQL schema language
var schema = buildSchema(`
  input MessageInput {
    content: String
    author: String
  }

  type Message {
    id: ID!
    content: String
    author: String
  }

  type Query {
    getMessage(id: ID!): Message
  }

  type Mutation {
    createMessage(input: MessageInput): Message
    updateMessage(id: ID!, input: MessageInput): Message
  }
`);

// If Message had any complex fields, we'd put them on this object.
class Message {
  constructor(id, {content, author}) {
    this.id = id;
    this.content = content;
    this.author = author;
  }
}

// Maps username to content
var fakeDatabase = {};

var root = {
  getMessage: ({id}) => {
    if (!fakeDatabase[id]) {
      throw new Error('no message exists with id ' + id);
    }
    return new Message(id, fakeDatabase[id]);
  },
  createMessage: ({input}) => {
    // Create a random id for our "database".
    var id = require('crypto').randomBytes(10).toString('hex');

    fakeDatabase[id] = input;
    return new Message(id, input);
  },
  updateMessage: ({id, input}) => {
    if (!fakeDatabase[id]) {
      throw new Error('no message exists with id ' + id);
    }
    // This replaces all old data, but some apps might want partial update.
    fakeDatabase[id] = input;
    return new Message(id, input);
  },
};

var app = express();
app.use('/graphql', graphqlHTTP({
  schema: schema,
  rootValue: root,
  graphiql: true,
}));
app.listen(4000, () => {
  console.log('Running a GraphQL API server at localhost:4000/graphql');
});
```

mutation：

```
mutation {
  createMessage( input: {
      content: "Learning GraphQL is good!",
      author: "Johny"
  }){
    id
  }
}
```

查詢：

```
{
  getMessage(id: "8d4511c4c23e74106b04"){
    author
    content
  }
}
```

其中 id 是create 時返回的。

這些範例都是使用 memory 作為資料庫，若要永久保留，可以將物件存成檔案，開啟時再將檔案載入。

有許多 Graph DB 也可以很方便的提供物件的存取。這個部分另外再介紹。

### DataLoader

graphql 執行時，資料庫是一個資料結構，若需要讓我們的資料擷取/寫入分開，可以使用一個 dataloader 的 API 介面。 安裝 dataloader

npm install --save dataloader

伺服器通常要位沒一個 request 起一個 DataLoader 的 instance, 這個 instance 表示一個 unique cache 。

一個DataLoader 建構子的參數是一個函式，此函式的參數是一個陣列 keys, 我們要撰寫這個 DataLoader 取資料的程式碼。

```
const DataLoader = require('dataloader')
const userLoader = new DataLoader( keys => {
   // batch load function here!;
   // return 對應 keys 的查詢結
   // 這個函式返回一個  Promise , return 的資料是 resolve 出來的
})

const user = await userLoad.load(1)  // 取得第1筆資料
const invi = await userLoad.load(user.invitedByID)  // 取得第1筆資料的 invitedByID
console.log(` 對應第 1 筆資料所取得的 invitedById 是 {$invi}`)

```

userLoad.load( key )

userLoad.loadMany( keys )

```js
const [ a, b ] = await myLoader.loadMany([ 'a', 'b' ])
```

useLoad.clear( key )

userLoad.clearAll()

userLoad.prime(key, value) 新增，但若 key 存在則不新增

要建立一個 DataLoader 就是要實作一個能夠從某個資料結構或是資料庫取出資料的函式。假設我們的 database 是暫時性的，那就會類似下面程式碼

```js
// 假設 db 有一個 fetchAllKeys 的函式，給一個陣列 keys, 返回對應的資料
async function batchFunction(keys) {
  const results = await db.fetchAllKeys(keys)
  return keys.map(key => results[key] || new Error(`No result for ${key}`))
}

const loader = new DataLoader(batchFunction)
```

如果 database 有類似 sqlite 的資料庫系統支持，那這一段程式碼就變成到資料庫取資料回來！

DataLoader 可以自訂排程函式，讓幾個 load() 動作依照排程進行。

DataLoader 另一個重要的工作是 cache. 由於資料的存取比較耗時，因此需要cache 相關的指令來加速存取！ ( 筆記待續 ... )

## 資料庫

graphql 的資料可以用一個資料庫來存取，sqlite3 是一個好選擇。

graphql 可透過 dataloader + squlite3 進行將資料永久存放！

我們先看獨立的 dataloader + sqlite3 的運作：

```js
const DataLoader = require('dataloader')
const sqlite3 = require('sqlite3')

const db = new sqlite3.Database('./sqliteDB/db.sql')

// Dispatch a WHERE-IN query, ensuring response has rows in correct order.
const userLoader = new DataLoader(ids => new Promise((resolve, reject) => {
  db.all('SELECT * FROM users WHERE id IN $ids', {$ids: ids}, (error, rows) => {
    if (error) {
      reject(error);
    } else {
      resolve(ids.map(
        id => rows.find(row => rows.id === id) || new Error(`Row not found: ${id}`)
      ));
    }
  });
}));

// Usage

const promise1 = userLoader.load('1234')
const promise2 = userLoader.load('5678')
const [ user1, user2 ] = await Promise.all([promise1, promise2])
console.log(user1, user2)
```

