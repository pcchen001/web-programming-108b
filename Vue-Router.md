---
title: Vue Router
date: 2020-04-13 09:13:12
tags:
- Vue
- Router
categories:
- Vue
- Advanced
---

使用　Vue Router 需要安裝 vue-router。

在 cli 中，我們在創建項目時，可以手動加上 router,  cli 會幫我們載入相關程式庫，留意 main.js 有加上必要的指令就可以了。

```js
// main.js
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index.js'
import BootstrapVue from 'bootstrap-vue'
  
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Vue.config.productionTip = false


new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app') 
```

檢查是否有 router.js

```js
import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import About from './views/About.vue'
import Cats from './views/Cats.vue'
import Dogs from './views/Dogs.vue'
import Pet from './views/Pet.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/Cats',
      name: 'cats',
      component: Cats
    },
    {
      path: '/Dogs',
      name: 'dogs',
      component: Dogs
    },
    {
      path: '/Pet/:species/:id',
      name: 'pet',
      component: Pet
    } 
  ]
})
```

另外在　App.vue 中使用 router-link 及 router-view

```vue
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link> |
      <!-- <router-link to="/about">About</router-link> -->
      <router-link to="/cats">Cats</router-link> |
      <router-link to="/dogs">Dogs</router-link>
    </div>
    <router-view/>
  </div>
</template>
```

router-link 就是 anchor, 點選時將連結的 view 關連到 router-view 。router-view 就是放置使用 router-link 連結的內容。

留意的是 /pet/:species/:id 是將網址的資料 bind 到 component 的屬性。