---
title: 課程綱要
date: 2020-01-02 20:44:16
tags:
- Course
categories:
- README
mathjax: false
---
# 網頁程式設計 108b

教師：陳博智

## 課程綱要

1.  課程簡介
2.  HTML 簡介( Ch. 1 ~ Ch. 7)
    - 頁面結構
    - 文字片段
    - 表列 list
    - 表格 table
    - 嵌入 影像、影音、音樂、及其他
    - 表單(gui介面)
3.  CSS 簡介( Ch. 8 ~ Ch. 9)
    - selector
    - attributes
    - box 
4.  JavaScript 語法
    - Ch.10 
    - X.1JavaScript topics
      1.  Array
      2.  Arrow function
      3.  Promise & callback
      4.  Async & await
      5.  regular expression
      6.  Object Oriented Programming
5.  Document Object Model 
    - Ch.11 Windows, document, element, ...
    - X.2 more on DOM
6.  Event Handling
    - Ch.12 Events & Event Handling
    - X.3 more on Event Handling
7.  jQuery
    1.  Ch.13 jQuery Basics
    2.  Ch.14 jQuery UI components
8.  Ajax
    - Ch.15 Ajax - 以 XHR 及 jQuery 進行 (簡要介紹)
    - 複習 Promise, Async, Await 
    - 使用　Fetch (重點)
      - 讀取靜態資料： text, image, json
      - get 送出資料，讀取各種形態資料
      - post 送出　formdata, jsondata 等，讀取各種形態資料
      - ajax + session 認證機制
      - CORS 概念、jsonp 概念
    - 使用　Axios
      - 以 Fetch 為基礎的 ajax 程式庫
9. Applications

   - Quiz

   - Todo

   - Wordnik

   - Maps- leaflet

   - Maps-(GoogleMap)

10. Responsive Web Design(RWD)
    - Ch. 16 RWD (jQuery UI)
    - X.5 Bootstrap & others
11. Vue
    1.  Vue Basics
    2.  Vue Application -- todo-list
    3.  Vue cli ui (command line interface, user interface)
12. node.js & npm

    - X.6 node.js & node package manager
13. App Examples

    - 寵物放養網
14. GraphQL-node.js

## 作業

1. 設計網頁 (HTML, CSS 基礎練習)
   
   - 請到網路上找二個不同類型，你還蠻喜歡的頁面設計，換上你的影像及內容。
   
2. 待辦事項 ( JS 基礎練習)

3. 待辦事項 Ajax 版，有預設的　database.json 資料

   1. 選項：另外撰寫 php ，新增的待辦事項　ajax 送回 php ，php 更改檔案內容。
   2. 期限：5/20

4. Ajax 登入頁面設計　(放上即時當地天氣、溫度)。有可以註冊的按鈕，註冊時出現註冊頁面，填寫帳號、密碼、色彩偏好。登入時檢查帳號密碼，正確後，出現訂單畫面以及有登出按鍵，訂單填寫後送出，資料存放資料庫，畫面出現訂單內容，按下登出後出現登入頁面。

   1. 期限：5/27

5. Bootstrap 以及 Ajax 應用：線上測驗系統

   1. 設計 10 題本學期課程相關的選擇題，將資料編寫成 quiz.json 檔案
   2. 讀取 quiz.json ，提供測驗介面。測驗結束顯示測驗成果。
   3. 期限：6/3

6. 待給定

   ------

   作業備註：地圖標示、線上測驗網頁 2 ，增加題庫管理(新增刪除修改)、待辦事項管理 1、待辦事項管理 2 、Vue 框架之應用。

   typora 備註：圖片請複製一份到 /public/2020/4/1/DOM 下的資料夾，然後在編輯的地方改成相對本身路徑。generate 出來的網頁才能有內容

進度註記：

4/1 DOM 初看，事件處理初看

4/8 事件處理、jQuery 基礎、jQuery UI、Ajax

4/15 HTTP_Ajax, HTTP_Fetch, HTTP_Axios

4/22 Applicatins 

4/29 期中考，檢查作業 1, 2, 3 。 口試！

5/6 Promise, Async, Await, Fetch 

5/13 Fetch, Applications

5/20 Bootstraps

5/27 Vue 基本概念

6/3 Vue-cli 

6/10 應用練習，期末專案討論

6/17 期末 demo 1, 問題與討論

6/20 期末 demo 2