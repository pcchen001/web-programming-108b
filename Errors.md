---
title: JS 異常處理
date: 2020-03-15 20:44:16
tags:
- Language
- Error
categories:
- JavaScript
mathjax: false
---
# Exception Handling

JS 的 Error 與 Exception 等義。

JS 處理錯誤的方式與 C++　類似，包含 throw 以及　try..catch..finally。

JS 可以直接 throw 一個 number 或是一個 string，然而比較正式的用法拋出一個 Error 。

```
throw 12;
throw 'Error here';
throw new Error('new Error');
```

處理Error 或 Exception 則與C++類似。

```
try{
    // some action here
    throw new Error('some Error');
}catch( e ){
    console.log( e.message );
}finally{
    // finally action.
}
```

除了 Error 這個　generic error type 之外，JS 提供六個 core error types:

```
EvalError
```

Creates an instance representing an error that occurs regarding the global function `eval()`.

```
InternalError
```

Creates an instance representing an error that occurs when an internal error in the JavaScript engine is thrown. E.g. "too much recursion".

```
RangeError
```

Creates an instance representing an error that occurs when a numeric variable or parameter is outside of its valid range.

```
ReferenceError
```

Creates an instance representing an error that occurs when de-referencing an invalid reference.

```
SyntaxError
```

Creates an instance representing a syntax error that occurs while parsing code in `eval()`.

```
TypeError
```

Creates an instance representing an error that occurs when a variable or parameter is not of a valid type.

```
URIError
```

Creates an instance representing an error that occurs when `encodeURI()` or `decodeURI()` are passed invalid parameters.

除了 JS 核心的六個 Error Type 之外，DOM 支援了 DOMException 及 DOMError 二群的錯誤型態，方便拋出不同型別的Error。

## DOMException

下列 31 個，可以有另外新增的。

```
IndexSizeError
```

The index is not in the allowed range. For example, this can be thrown by the [`Range`](https://developer.mozilla.org/en-US/docs/Web/API/Range) object. (Legacy code value: `1` and legacy constant name: `INDEX_SIZE_ERR`)

```
HierarchyRequestError
```

The node tree hierarchy is not correct. (Legacy code value: `3` and legacy constant name: `HIERARCHY_REQUEST_ERR`)

```
WrongDocumentError
```

The object is in the wrong [`Document`](https://developer.mozilla.org/en-US/docs/Web/API/Document). (Legacy code value: `4` and legacy constant name: `WRONG_DOCUMENT_ERR`)

```
InvalidCharacterError
```

The string contains invalid characters. (Legacy code value: `5` and legacy constant name: `INVALID_CHARACTER_ERR`)

```
NoModificationAllowedError
```

The object cannot be modified. (Legacy code value: `7` and legacy constant name: `NO_MODIFICATION_ALLOWED_ERR`)

```
NotFoundError
```

The object cannot be found here. (Legacy code value: `8` and legacy constant name: `NOT_FOUND_ERR`)

```
NotSupportedError
```

The operation is not supported. (Legacy code value: `9` and legacy constant name: `NOT_SUPPORTED_ERR`)

```
InvalidStateError
```

The object is in an invalid state. (Legacy code value: `11` and legacy constant name: `INVALID_STATE_ERR`)

```
SyntaxError
```

The string did not match the expected pattern. (Legacy code value: `12` and legacy constant name: `SYNTAX_ERR`)

```
InvalidModificationError
```

The object cannot be modified in this way. (Legacy code value: `13` and legacy constant name: `INVALID_MODIFICATION_ERR`)

```
NamespaceError
```

The operation is not allowed by Namespaces in XML. (Legacy code value: `14` and legacy constant name: `NAMESPACE_ERR`)

```
InvalidAccessError
```

The object does not support the operation or argument. (Legacy code value: `15` and legacy constant name: `INVALID_ACCESS_ERR`)

```
TypeMismatchError
```

The type of the object does not match the expected type. (Legacy code value: `17` and legacy constant name: `TYPE_MISMATCH_ERR`) This value is deprecated; the JavaScript [`TypeError`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError) exception is now raised instead of a `DOMException` with this value.

```
SecurityError
```

The operation is insecure. (Legacy code value: `18` and legacy constant name: `SECURITY_ERR`)

```
NetworkError
```

A network error occurred. (Legacy code value: `19` and legacy constant name: `NETWORK_ERR`)

```
AbortError
```

The operation was aborted. (Legacy code value: `20` and legacy constant name: `ABORT_ERR`)

```
URLMismatchError
```

The given URL does not match another URL. (Legacy code value: `21` and legacy constant name: `URL_MISMATCH_ERR`)

```
QuotaExceededError
```

The quota has been exceeded. (Legacy code value: `22` and legacy constant name: `QUOTA_EXCEEDED_ERR`)

```
TimeoutError
```

The operation timed out. (Legacy code value: `23` and legacy constant name: `TIMEOUT_ERR`)

```
InvalidNodeTypeError
```

The node is incorrect or has an incorrect ancestor for this operation. (Legacy code value: `24` and legacy constant name: `INVALID_NODE_TYPE_ERR`)

```
DataCloneError
```

The object can not be cloned. (Legacy code value: `25` and legacy constant name: `DATA_CLONE_ERR`)

```
EncodingError
```

The encoding or decoding operation failed (No legacy code value and constant name).

```
NotReadableError
```

The input/output read operation failed (No legacy code value and constant name).

```
UnknownError
```

The operation failed for an unknown transient reason (e.g. out of memory) (No legacy code value and constant name).

```
ConstraintError
```

A mutation operation in a transaction failed because a constraint was not satisfied (No legacy code value and constant name).

```
DataError
```

Provided data is inadequate (No legacy code value and constant name).

```
TransactionInactiveError
```

A request was placed against a transaction which is currently not active, or which is finished (No legacy code value and constant name).

```
ReadOnlyError
```

The mutating operation was attempted in a "readonly" transaction (No legacy code value and constant name).

```
VersionError
```

An attempt was made to open a database using a lower version than the existing version (No legacy code value and constant name).

```
OperationError
```

The operation failed for an operation-specific reason (No legacy code value and constant name).

```
NotAllowedError
```

The request is not allowed by the user agent or the platform in the current context, possibly because the user denied permission (No legacy code value and constant name).

## DOMError types

| Type                         | Description                                                  |
| ---------------------------- | ------------------------------------------------------------ |
| `IndexSizeError`             | The index is not in the allowed range (e.g. thrown in a [`range`](https://developer.mozilla.org/en-US/docs/Web/API/Range) object). |
| `HierarchyRequestError`      | The node tree hierarchy is not correct.                      |
| `WrongDocumentError`         | The object is in the wrong [`document`](https://developer.mozilla.org/en-US/docs/Web/API/Document). |
| `InvalidCharacterError`      | The string contains invalid characters.                      |
| `NoModificationAllowedError` | The object can not be modified.                              |
| `NotFoundError`              | The object can not be found here.                            |
| `NotSupportedError`          | The operation is not supported                               |
| `InvalidStateError`          | The object is in an invalid state.                           |
| `SyntaxError`                | The string did not match the expected pattern.               |
| `InvalidModificationError`   | The object can not be modified in this way.                  |
| `NamespaceError`             | The operation is not allowed by Namespaces in XML            |
| `InvalidAccessError`         | The object does not support the operation or argument.       |
| `TypeMismatchError`          | The type of the object does not match the expected type.     |
| `SecurityError`              | The operation is insecure.                                   |
| `NetworkError`               | A network error occurred.                                    |
| `AbortError`                 | The operation was aborted.                                   |
| `URLMismatchError`           | The given URL does not match another URL.                    |
| `QuotaExceededError`         | The quota has been exceeded.                                 |
| `TimeoutError`               | The operation timed out.                                     |
| `InvalidNodeTypeError`       | The node is incorrect or has an incorrect ancestor for this operation. |
| `DataCloneError`             | The object can not be cloned.                                |