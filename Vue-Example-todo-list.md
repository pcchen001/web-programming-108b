---
title: Vue Example todo-list
date: 2020-05-13 10:00:00
tags:
- Vue
- Application
- framework
categories:
- Vue
---

本範例延續我們在 Ajax 應用中的待辦事項管理應用，現在改成用 Vue 來實現：

```
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        function init(){
            
        }
    </script>
</head>
<body onload="init();">
    <div ip="app"></div>
</body>
</html>
```

接著在 init() 中建立 Vue 的物件。 

created: 函式用來載入資料，我們延續之前的範例使用 fetch 載入 json 資料。

```js
        function init(){
            var app = new Vue({
                el: '#app',
                data: {
                    todos: [],
                    newTitle: ""
                },
                created: function(){
                    fetch("todos.data")
                    .then(resp=>{return resp.json()})
                    .then(resp=>this.todos = resp)
                }
            })
        }
```

以及在 body 中建立輸出：

```html
    <div id="app">
        <ol>
            <li v-for="todo in todos">
                <input type="checkbox">
                <span>{{todo.title}}</span>
            </li>
        </ol>
    </div>
```

dotos.data 如下：

```json
[
    {
    "title" : "Learning JS",
    "completed" : true
    },
    {
    "title" : "Learning Bootstrap",
    "completed" : false
    },
    {
    "title" : "Learning Vue",
    "completed" : false
    }
]
```

現在我們已經有初步的畫面了，接著要提供 completed 的功能。我們希望 completed 時，這個項目會有刪掉的效果：

```html
                <input type="checkbox">
                <del v-if="todo.completed">{{todo.title}}</del>
                <span v-else>{{todo.title}}</span>
```

v-if v-else 可以提供我們判斷，依據 todo.complete 來決定是否加上刪除線。接著，要讓在 checkbox 打勾可以更改 todo.completed。我們要撰寫回應打勾的 methods 假設式 toggle。

```html
                <input type="checkbox" @change="toggle(todo)">
```

```js
                methods: {
                    toggle: function(item){
                        item.completed = !item.completed
                    }
                }
```

現在可以打勾勾了。但是第一個打了勾就沒有刪除線，不打勾卻有刪除線？ 怎麼辦？答案是

```html
<input type="checkbox" @change="toggle(todo)" :checked="todo.completed">
```

綁 checked 屬性，由 todo.completed 的 true 或 false 來決定是否打勾。

差不多了，接著就該給一個 input 將新的待辦事項新增上去：

```html
<input type="text" v-model="newTitle"><button @click="addTodo">新增待辦事項</button>
```

準備一個  input 及一個按鈕。input 內容綁到 newTitle。按鈕給一個事件處理函式，假設 addTodo。

接著要去完成 addTodo() 這個方法：

```js
                methods: {
                    toggle: function(item){
                        item.completed = !item.completed
                    },
                    addTodo: function(){
                        this.todos.push({title:this.newTitle, completed: false})
                        this.newTitle=""
                    }
                }
```

我們將一個新的 todo 加到 todos 這個陣列，並且重置我們的 input ，也就是 newTitle 。

應用完成！

延伸： 如果你可以寫 php 端程式，可以增加一個離開的按鍵，將新的 todos 存放到 datas.data 。

練習：增加一個欄位來輸入期限。

練習：檢查期限是否已經過了。

後記： 用過 vue 之後是否覺得使用 vue 在設計應用時更具有邏輯性跟結構性。這會讓應用邏輯更加複雜時，我們的程式撰寫可以支援的住！

作業：撰寫  Quiz 的 Vue 版本！