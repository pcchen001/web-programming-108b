---
title: Ajax using jQuery .Ajax
date: 2020-04-15 08:00:00
tags:
- HTTP
- Ajax
- jQuery
categories:
- HTTP
---

從前端的網頁要取得資料，必須使用非同步資料擷取。

ES6 之前的 Javascript 支援 HTTPXMLRequest，我們需要撰寫稍微複雜的程式碼來進行非同步擷取。jQuery 將一些複雜的細節包在一系列的指令中，讓非同步資料擷取變得相對簡單許多。

擷取需要送出 Request, HTTP 送出 Request 常用 GET 或是 POST 。

送出 Request 有時需要傳送資料給伺服器端，有時不需要。

伺服器端送回來的資料，型態大致可以分為 text, xml, json, form 以及 blob。

jQuery 的 Ajax 指令，單純的 get 需求可以使用$.load() 或 $.get(), 單純的 post 需求可以使用 $.post。若需要個別設定不同參數時，可以使用 $.ajax。

範例 1  jquery_get_text.html

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
      $(function(){		
	    $("#show").on("click", function(){        
	      $.get("poetry.txt", function(data){
	        $("#content").text(data);
	      });
        });		  
      });	  
    </script>
  </head>
  <body>
    <button id="show">顯示詩句</button>
    <div id="content"></div>	
  </body>
</html>
```

在 script 中， $(function(){}); 相當於 onload 。也就是網頁載入後執行之。

$("#show").on("click". function(){}); 是設定 onclick 的事件處理函式。

$.get("url", function(data){}); 是對 url 進行非同步擷取，取回的會當作 function 的 引數，也就是 data。

$("#content").text(data); 是將 data 設定成 #content 的 text 內容。



範例 2 jquery_get_xml.html

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">	
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>	  
      $(function(){
        $.get("http://httpbin.org/xml", function(xml){		
		   $(xml).find("slide").each(function(){
		     var li = $("<li></li>").text($(this).find("title").text());
			 $("#list").append(li);
		   });
      });
});	  
    </script>
  </head>
  <body>     
    <ol id="list"></ol>   
  </body>
</html>
```

$(function(){...});  如範例 1 說明。這是 jQuery 對於onload function 的匿名函式定義。

$.get("url", function(xml){...}); 這是執行 get 的 ajax 動作，並且將回傳的資料 xml 當作引數呼叫後面的匿名函式，.get() 的第二個引數是一個 callback 函式。 

 $(xml).find("slide").each(function(){...}); 這是將回傳的 xml 當作一個元素，find 其中 slide 元素，找到一個陣列的 slide 元素，然後對每一個 slide 元素執行 function(){} 。 

```
$('<li></li>').text( 'sometext' )
```

可以建立一個 li 的元素，並且將sometext 設為這個元素的 text內容。

.each( function (){}); 是將 array 的每一個元素用 function來執行。 在 function 中，這每一個元素用 $(this)來指涉。 這裡用 find("title").text() 來取得其中 title 子元素的 text 內容。

append()  加到 #list 這個 ol 元素裡面成為最後一個子節點元素。

範例中的 http://httpbin.org/xml 是網路範例，內容如下。若網路已經毀了，那我們可以在本地複製建立一個 .xml檔案來替代。

```xml
<!--   A SAMPLE set of slides   -->
<slideshow title="Sample Slide Show" date="Date of publication" author="Yours Truly">
    <!--  TITLE SLIDE  -->
    <slide type="all">
        <title>Wake up to WonderWidgets!</title>
    </slide>
    <!--  OVERVIEW  -->
    <slide type="all">
        <title>Overview</title>
        <item>        Why        <em>WonderWidgets</em>        are great        </item>
        <item>        Who        <em>buys</em>        WonderWidgets        </item>
    </slide>
</slideshow>
```



範例 3 jquery_get_json.html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>jQuery Ajax Get JSON</title>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
        $(function(){
            $.getJSON("sometodo.json", function(jsondata){
                for( v of jsondata){
                    console.log(v.name);
                    var l = $("<li></li>").text(v.name);
                    $('#list').append(l);
                }
            });
        });
    </script>
</head>
<body>
    <h2>todo list </h2>
    <ol id="list"></ol>
</body>
</html>
```

jQuery 為了擷取 JSON 資料，客製化了一個 getJSON 的函式，可以將取得的 json 資料立即解析。

範例  3 與 範例 1, 2 類似，主要差別在於 getJSON 。其中我們使用的 json 資料 sometodl.json 內容如下：

```json
[
    {
        "name": "研讀 HTML",
        "checked": false
    },
    {
    "name" : "Learning JS",
    "checked" : true
    },
    {
    "name" : "Learning Bootstrap",
    "checked" : false
    },
    {
    "name" : "Learning Vue",
    "checked" : false
    }
]
```



範例 4 jquery_get_php.html / GetServerTime.php

這個範例主要說明我們的 url 不僅對一個檔案，也可以是一個服務傳回來的資料，例如一個 php 程式的回傳。

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
      $(function(){		
	    $("#show").on("click", function(){        
	      $.get("GetServerTime.php", function(data){
	        $("#content").text(data);
	      });
        });		  
      });          
    </script>
  </head>
  <body>
    <button id="show">顯示時間</button>
    <div id="content"></div>        
  </body>
</html>
```

範例中的 GetServerTime.php 如下：

```php
    <?php
        echo gmdate("Y-m-d H:i:s");       
    ?>
```

範例 4 也是使用 get, 但我們的 url 是一個本地的 php 服務。



範例 5 連續抓取太空站位置資料 (jquery_get_json_iss.html)

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>jQuery Ajax Get JSON (ISS)</title>
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
        const url = "https://api.wheretheiss.at/v1/satellites/25544";
        $(function(){
            setInterval(update, 1000);
        });
        function update(){
            $.getJSON(url, function(jsondata){
                isslati= jsondata.latitude;
                isslong= jsondata.longitude;
                $("#issa").text("Latitude: "+isslati);
                $("#isso").text("Longitude:"+isslong);
            });
        }
    </script>
</head>
<body>
    <h2>ISS 太空站 </h2>
    <div id="earchMap"></div>
    <div><ul>
        <li id="issa">Latitude: </li>
        <li id="isso">Longitude: </li>
    </ul></div>
</body>
```

這是一個有趣的實務範例，國際太空站 可經由 open-notify Nasa的API, 提供有關衛星的資料 

> The [International Space Station](http://en.wikipedia.org/wiki/International_Space_Station) is moving at close to 28,000 km/h so its location changes really fast! Where is it right now?
>
> http://api.open-notify.com/
>
> http://open-notify.org/Open-Notify-API/ISS-Location-Now/ 可以取得
>
>   {   "message": "success",    "timestamp": UNIX_TIME_STAMP,    "iss_position": {    "latitude": CURRENT_LATITUDE,     "longitude": CURRENT_LONGITUDE   }  }     
>
> 稍後的地圖應用案例中，我們將會把這個座標放到地圖上去，實際看一下太空站的移動軌跡。

若要從一個 form 中將資料送出去，可以利用 FormData。







------

### jsonp

在以 ajax 取得 json 資料的經驗中，有一次我遇到了要到中央氣象局抓空氣品質資料時， json 不work! 一查之下知道他回傳的是 jsonp 而非 json 。然後我們發現 jQuery 的 ajax 可以設定 dataType 為 jsonp ，因此就直接使用了下面的抓取程式片段：

示例：

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <script>
      $(function(){
	    var dataUrl = "http://opendata.epa.gov.tw/ws/Data/AQFN/?$orderby=PublishTime%20desc&$skip=0&$top=1000&format=json";

	    $("#forecast").on("click", function(){
	      $.ajax({
            url : dataUrl,			//指定資料的網址
            dataType : "jsonp",		//指定資料的格式
	        success : onSuccess		//指定當Ajax要求成功時所要執行的函式
          });
		});
	  });

      function onSuccess(data){
	    //清空表格內的資料
	    $("#airQ").empty();

		//建立表格的第一列
	    var firstRow = $("<tr><th>地區</th><th>預報內容</th></tr>");
	    $("#airQ").append(firstRow);

		//將取得的資料一一建立為表格的每一列
		$.each(data, function(i){
		  var row = $("<tr></tr>");
		  var td1 = $("<td></td>").text(this.Area).appendTo(row);
		  var td2 = $("<td></td>").text(this.Content).appendTo(row);
		  $("#airQ").append(row);
		});
      }
    </script>
  </head>
  <body>
    <button id="forecast">更新空氣品質預報</button><br><br>
    <table id="airQ" rules="rows"></table>
  </body>
</html>
```

執行程式，果然我們可以順利取得目前各地的空氣品質資訊！ 

雖然我們可以直接用網址取得 json 資料，但是在程式中就是不行。檢查錯誤發現了 cors 錯誤，也就是我們的瀏覽器不接受不同來源的資料合併一起。這種設計的背後有資安的考量，但是要如何破解呢？

jsonp 就是破解方式，上述若我們設定 dataType 是 jsonp 那麼，jQuery 將執行 success 所指定的函式，而這個函式的引數會是傳回來的 json 資料。這就是 $.ajax 使用 jsonp 的方式。

上述的範例中，我們假設伺服器端以 jsonp 的方式回應，我們只要將 datatype 設為 jsonp 就好，其餘方式跟 json 的取法完全相同。

#### jsonp 原理說明

但是，這倒底這個設計的原理是甚麼呢？我們查詢了許多參考資料，整理說明如下：(有興趣寫前端的同學要了解一下)  

Javascript因為安全性的問題，是無法做跨站(cross-domain)請求的。如果要顯示一些非同步且跨站的資訊，通常都會使用iframe。但是這樣就無法將跨站的資料與我們的程式做密切結合。有一個設計是利用 script 標籤的工作原理。script 標籤程式碼是將要載入運作的程式寫成 .js 檔案，然後用 src=" " 載入並且執行運算。例如：

```html
<script src="http://example.org/jsonp.js">
```

若我們設計一個函式在 src 裡面，那意味著這個函式會被自動執行。例如：

```html
<script> function myFun( data ){ console.log( data );}
</script>
<script src="myFun({name:'json', age:20})">
```

myFun() 會被立即執行。現在如果我們透過 request 遠端一個服務取得資料，而這個取得的資料用一個我們設計好的函式包起來，例如 myFun()，這樣這個函式的執行就可以直接處理取得的資料了。很厲害的想法！這個 myFun() 可以視為一個 callback 函式。 為了要動態執行取得資料，我們可以動態產生 script 標籤，這樣一來標籤產生的當下就會取遠端擷取資料，資料擷取後執行 callback 函式，完成工作。

一個合理的呼叫方式如下：

```
<script src="http://example.com/test.php?callback=myhand"></script>
```

其實這就是透過JSONP(JSON with Padding)傳輸的，將JSON資料填入Padding(Padding就是要呼叫的函式)

當我們送給伺服器端的 request 包含 $_GET['callback'] 時，jsonp 的伺服器回復會像是：

```
myhand({json content})
```

myhand 是我們寫的處理回覆的函式，也就是 callback 函式。透過這個方式，我們可以避免 cross-domain 要求的限制。

換句話說，若我們要寫一個以 jsonp 回復的伺服器端PHP程式，我們的回應會像是：

```php
$json = json_encode(array('name' => 'JohnsonLu','age' => '24', 'url' => 'blog.johnsonlu.org'));
echo $_GET['callback'] . '(' . $json . ')';
```

要動態執行， script 標籤必須要動態產生，一個簡單範例如下：

```javascript
$('#btn').click(function(){
  var s = document.createElement("script");
  s.src = "http://example.com/test.php?callback=myhand";
  document.body.appendChild(s);
});
function myhand( myObj ){
  var x, txt = "";
  for (x in myObj) {
    txt += myObj[x].name + "<br>";
  }
  document.getElementById("demo").innerHTML = txt;    
}
```

點擊 #btn 時，就會執行 myhand();

[w3schools/jsonp](w3schools/jsonp) 的說明簡要清楚。





