---
title: Web API - Event Handler
date: 2020-04-8 23:03:09
tags:
- Event
- Event Handler
categories:
- DOM
- Javascript
---

# Event Handlers

Events:

1. window 類
   1. load, unload
   2. focus, blur
   3. error
   4. scroll
   5. resize
2. 鍵盤類
   1. keydown, keyup, kdypress
3. 滑鼠類
   1. mousedown, mouseup, mouseover, mouseout, mousewheel, click, dblclick
4. 表單類
   1. submit, reset, select, change, focus, blur
5. HTML5 類
   1. video/audio
      1. loadstart, progress, suspend, abort, error, emptied, stalled, loadedmetadata, loadeddata, canplsy, canplaythrough
6. DOM 類
   1. focusin, focusout, mouseenter, mouseleave, textinput, wheel
7. Touch類
   1. touchstart, touchmove, touchend, touchcancel

Event Handlers

一般我們在元素的屬性，用 onevent='script' 來設定一個事件的處理程序。例如

```html
 <head>   
	<script>
      function c2f(){
        let vc = document.getElementById("c");
        let re = document.getElementById("r");
        re.innerHTML = vc.value * 9 / 5 +32;
      }
    </script>
  </head>
  <body>
    <input type="button" id="b1" value="顯示訊息" 
      onclick="alert('Hello World!');">
    <input type="text" id="c">
    <button onclick="c2f();"> 攝氏轉華氏溫度</button>  
    <label for="" id="r"></label>
  </body>
```

onclick 作為一個屬性，我們也可以用動態的方式設定事件處理函式，例如： (event3.html)

```html
    <input type="button" id="b1" value="顯示訊息">
	<script language="javascript">
	  var b1 = document.getElementById("b1");	//取得 <button> 元素
	  b1.onclick = showMsg;					//設定事件處理程式
	  function showMsg()
	  {    
	    window.alert('Hello World!');
	  }
	</script>
```

在程式中，我們也可以用 e.addEventLIstener( "load", handlerFunction, false) 來設定一個函式作為處理器。

例如：(event4.html)

```html
    <input type="button" id="b1" value="顯示訊息">
	<script language="javascript">
	  var b1 = document.getElementById("b1");	  
	  b1.addEventListener("click", showMsg, false);  //設定事件處理程式 
      function showMsg()
      {    
        window.alert('Hello World!');
      }
	</script>
```

javascript 作為直譯語言，一件事情能在一行指令完成會盡量在一行指令完成。因此常常會使用匿名函式而不是另外定義一個函式來呼叫。例如：(event5.html)

```html
    <input type="button" id="b1" value="顯示訊息">
	<script language="javascript">
      var b1 = document.getElementById("b1");
      b1.addEventListener("click", function() {alert('Hello World!');}, false);
      b1.addEventListener("click", function() {alert('歡迎光臨!');}, false);
    </script>
```

在 ES6 之後，為了讓程式碼更加精簡，匿名函式可以寫成 arrow function, 例如：(event6.html)

```html
<input type="button" id="b1" value="顯示訊息">
<script language="javascript">
  var b1 = document.getElementById("b1");
  b1.addEventListener("click", ()=>alert('Hello World!'), false);
  b1.addEventListener("click", ()=>alert('歡迎光臨!'), false);
</script>
```
**arrow function** 是將 funtion(a, b){ statements } 改寫成 (a,b)=>{statements} 如果 statements 只有一行，那麼大括號也可以省略，程式看起來就會很精簡。

 

a 元素的 href 也可以放 javascript 的指令`,不過必須加上 script: 來標示這不是純粹的 url。例如下列程式，我們使用 a 來呼叫執行 print() 函式。

```html
    <h1>我的網頁</h1>
    <a href="script:print();">列印網頁</a>
```



時間，相關的事件主要有二個指令， setTimeOut 及 setInterval、clearInterval。

```javascript
setTimeOut( func, 1000);  // 設定延遲 1000 ms 後，執行 func 

var rep = setInterval( func, 2000);  // 每2秒執行一次 func
clearInterval( rep, 1000);  // 1秒之後停止 rep 的執行
```

javascript 支援遞迴呼叫，自己可以呼叫自己，因此有時程式設計師會使用 setTimeOut 配合遞迴呼叫來達到無窮迴圈的動畫目的。

課本 sample12-3.html 就是使用這個技巧：

```html
  <head>
    <meta charset="utf-8">
    <title>範例</title>	
	<script language="javascript">
      var info="歡迎光臨翠墨資訊﹗　　　　　　　　　　　　　　　";
      var interval=200;
      var empty="";
      var sin=0;
      function Scroll()
      {
        document.myForm.myText.value = info.substring(sin, info.length) 
		  + empty + info.substring(0, info.length);
        sin+=2;
        if (sin > info.length) sin = 0;
        window.setTimeout("Scroll();", interval);	// 遞迴呼叫
       } 
    </script>
  </head> 
  <body onload="javascript:Scroll();">
    <form name="myForm">
      <input type="text" name="myText" size="30">
    </form>
  </body>
```

如果只是要延遲一段時間再執行一個動作，沒有遞迴呼叫就沒有重複執行了。

另外一個重複執行動作的指令是 setInterval() 伴隨可以停止執行的 clearInterval()。時鐘或是我們希望定期更新頁面的情形常常需要使用 setInterval()。例如我們希望能即時取得 GPS 的位置，如果我們一直改變位置，那網頁就要不斷更新，此時當然要使用類似 setInterval()的指令來進行。

下面我們用一個計時器網頁來示範，同時也要各位設計一個有開始、繼續、重置功能的網頁。

```html
  <head>
    <meta charset="utf-8">
    <title>範例</title>	
    <script language="javascript">
      var miliseconds = 0, seconds = 0;
      function init(){
        document.myForm.myField.value = "0";
      }
      function showStayTime(){
        if (miliseconds >= 9){
          miliseconds = 0;
          seconds += 1;
        }
        else miliseconds += 1;
        document.myForm.myField.value = seconds + "." + miliseconds;
      }
      function start(){
        startx = setInterval(showStayTime, 100);
      }
      function stop(){
        clearInterval(startx);
      }
    </script>
  </head>
  <body onload="init();">
    <form name="myForm"> 
      網頁計時器<input type="text" name="myField" size="5">秒
    </form>
    <button onclick="stop();">Stop</button>
    <button onclick="start()">Start</button>
  </body>
```

**實務練習**： 這個計時器只有  start 及 stop 。請修改程式碼，加上一個重置，讓計時從 0 開始。



在設計網頁時，有時我們希望點擊一個超連結時，能保留目前的網頁，也就是希望瀏覽器能夠開新的頁面，此時我們需要使用 open() 這個指令，下面的範例可以看到如何開啟新的視窗網頁。(sample12-4.html

```html
<!doctype html> 
<html>
  <head>
    <meta charset="utf-8">
    <title>範例</title>	
    <script language="javascript">
      function GO(){
        mys = document.getElementsByName("mySelect")[0];
        url = mys.options[mys.selectedIndex].value;
        newWin = open(url,"","width=200,height=100");
      }
    </script>
  </head> 
  <body>
    <form name="myForm"> 
      <select name="mySelect" size="1"> 
        <option value="http://tw.yahoo.com">雅虎奇摩 
        <option value="http://www.yam.com">蕃薯藤
        <option value="http://www.google.com.tw">Google
      </select>
      <input type="button" value="GO!" onclick="GO();">
    </form>
  </body>
</html>
```

這裡我們多介紹一點 open()指令。 open(url) 通常會開新的籤頁，open("", "", "width=400, height=100") 會開一個指定大小的視窗，我們還可以指定大小以及內容。





