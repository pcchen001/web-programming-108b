---
title: HTML 構成網頁的語言
date: 2020-01-22 20:44:16
tags:
- HTML
- Basics
categories:
- HTML
mathjax: false
---

以下內容參考 <a href="https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML">MOZILLA</a>

# HTML Overview

HTML 是一個 markup language. 排版語言，在網頁的構成中，HTML主要提供內容，為了種種理由，網頁的頁面內容最好是有結構設計，在結構設計下，一個網頁就能提供從單純的新聞報導到單頁應用的內容。

HTML(Hypertext Markup Language) 其中的 Markup 就是排版的意思。主要的精神是每一段內容我們都要給它標上一個標籤(tag)，標上標籤的方法是在內容的前後用標籤的前後符號包起來，例如：

```
<p>This is a paragraph. 這是一段文字內容</p>
```

其中 < p > 我們稱之為 openning tag, < /p > 我們稱之為 closing tag。

二個標籤內稱之為 content 。而openning tag + content + closing tag 稱之為 element 元素。

HTML 元素具有巢狀結構，也就是一個元素可以放在一個元素內，例如：

```
<p>開發網頁應用包含了<strong>前端程式</strong>以及其他部分，...</p>
```

## 區塊元素與在線元素

HTML 的元素可以分為二大類，一個是 block-level 區塊元素，另一個是 inline 在線元素。

- 區塊元素構成一個矩形的區塊，在排版上會是頁面上的一個可見的區塊。一個區塊內可以有區塊元素，也可以有在線元素，但是在線元素內不可以有區塊元素。
- 在線元素通常是一個內容的一部分，它可以是在一段內容內的一部份。

## 空元素

有些元素沒有 closing tag. 也就是沒有 content。通常這些元素是用來在頁面上插入如圖像、影片等。例如：

```
<img src="https://raw.githubusercontent.com/mdn/beginner-html-site/gh-pages/images/firefox-icon.png">
```

## 屬性

一個元素可以有屬性，看起來像下面：

```
<div class="middle-5-3">我的程式碼我自己寫。</div>
```

class 就是這個元素的一個屬性，middle-5-3 是這個屬性的屬性值，屬性值必須使用雙引號或是單引號括起來。有些屬性具有一定的功能性，例如：

```
<a href="http://www.im.nuu.edu.tw" title="國立聯合大學資訊管理學系" target="_blank"> 系上連結 </a>
```

- **`href`**: This attribute's value specifies the web address that you want the link to point to; where the browser navigates to when the link is clicked. For example, `href="https://www.mozilla.org/"`.
- **`title`**: The `title` attribute specifies extra information about the link, such as what page is being linked to. For example, `title="The Mozilla homepage"`. This will appear as a tooltip when the element is hovered over.
- **`target`**: The `target` attribute specifies the browsing context that will be used to display the link. For example, `target="_blank"` will display the link in a new tab. If you want to display the link in the current tab, just omit this attribute.

### 布林屬性

有些屬性的屬性值只能是 true 或 false 或是 enable 或 disable。這些屬性我們只使用屬性而不給屬性值。例如在下面的文字輸入元素，我們可以用 disable 屬性來限制這個欄位是否可以輸入：

```
<!-- using the disabled attribute prevents the end user from entering text into the input box -->
<input type="text" disabled>

<!-- The user can enter text into the follow input, as it doesn't contain the disabled attribute -->
<input type="text">  
```

## 基本HTML文件架構

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>My test page</title>
  </head>
  <body>
    <p>This is my page</p>
  </body>
</html>
```

1. `<!DOCTYPE html>`

   簡要標示這是一個 html 文件。

2. `<html></html>`: `<html>`元素包含整個文件，稱之為 root 元素。

3. `<head></head>`: The [`<head>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/head) 元素. 這個元素專門用來放不會出現在頁面內容內的資料或程式碼。

4. `<meta charset="utf-8">`: This element specifies the character set for your document to UTF-8, which includes most characters from the vast majority of human written languages. 

5. `<title></title>`: The [`<title>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/title) element. 出現在視瀏覽器的網頁頁標tab。

6. `<body></body>`: The [`<body>`](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/body) element. This contains *all* the content that you want to show to web users when they visit your page, whether that's text, images, videos, games, playable audio tracks, or whatever else.

| 小小練習一下：      |
| ------------------- |
| 編輯一個 index.html |

## 白空格 whitespace in HTML

HTML 忽略 whitespace 。

## 特殊字元

有幾個特殊字元，在編輯時需要使用字元參考：

| Literal character | Character reference equivalent |
| :---------------- | :----------------------------- |
| <                 | `&lt;`                         |
| >                 | `&gt;`                         |
| "                 | `&quot;`                       |
| '                 | `&apos;`                       |
| &                 | `&amp;`                        |

# 文字基礎 HTML text fundamentals

## 標題與內文

標題標籤 h1, h2, h3, h4, h5, h6 分別表示六個階層的標題。p 是段落的意思，表示內文標籤。

## 語意

語意無所不在，但是如何在頁面中表達？ 在 HTML 中，我們透過不同的標籤來表達一些意思，例如 h1 表達了標題，而 span 只是一段文字，沒有表達語意。

好的網頁會透過結構來表達內容語意，有了這些語意標示，除了在排版上能更排出一些在視覺上有結構的頁面(，排版的視覺效果通常經由 CSS 來達成)，在網頁搜尋時往往也可以提供良好的線索，讓SEO(Search Engine OPtimization) 表現得更好。

## 串列 LIsts

串列分為 ordered 及 unordered 二種：

### unordered lists

```html
<ul>
  <li>milk</li>
  <li>eggs</li>
  <li>bread</li>
  <li>hummus</li>
</ul>
```

### ordered lists

```html
<ol>
  <li>Drive to the end of the road</li>
  <li>Turn right</li>
  <li>Go straight across the first two roundabouts</li>
  <li>Turn left at the third roundabout</li>
  <li>The school is on your right, 300 meters up the road</li>
</ol>
```

## 強調片段

斜體字通常用來強調重要性，標籤為 `<em>` 。粗體字通常用來表示專有名詞或是強烈的加強注意，標籤為 `<strong>`。另外在編輯上常用的字體變化有`<i> <b> <u>` 分別用來表示斜體字、粗體字以及加底線。

# 細說超連結

[Mozilla 的超連結說明](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Creating_hyperlinks)

在 `<a>` 元素中， href="連結"， title="當滑鼠滑到此處時，會出現的文字內容" target="連結內容出現在何處"。target 若沒有設定，預設為目前的頁面，若是 "_blank" ，則是另起新頁面。

在 `<img>`元素中，src="連結圖像位置及檔名"，alt="當這個連結失效時，替代文字"。

不論是 href 或是 src ，我們都可以使用 http://... 或是取用伺服器的檔案 ，取用伺服器的檔案可以有絕對路徑以及相對路徑二種指定方式：

與目前網頁相同位置：

```html
<p>Want to contact a specific staff member?
Find details on our <a href="contacts.html">contacts page</a>.</p>
```

目前網頁的子目錄的檔案：

```html
<p>Visit my <a href="projects/index.html">project homepage</a>.</p>
```

目前網頁的上層下的pdfs 目錄下的檔案：

```html
<p>A link to my <a href="../pdfs/project-brief.pdf">project brief</a>.</p>
```

絕對路徑以網站的根目錄為起點：

```html
<p>A link to my <a href="/pdfs/project-brief.pdf">project brief</a>.</p>
```

也可以指定到檔案內的片段位置，這個片段位置由元素的 id 屬性值來識別。

```html
<p>Want to write us a letter? Use our <a href="contacts.html#Mailing_address">mailing address</a>.</p>
```

### 下載屬性的應用

利用 download 屬性，可以更明確的表示所點擊的是一個網頁連結還是一個檔案下載。例如：

```html
<a href="https://download.mozilla.org/?product=firefox-latest-ssl&os=win64&lang=en-US"
   download="firefox-latest-64bit-installer.exe">
  Download Latest Firefox for Windows (64-bit) (English, US)
</a>
```

## 網頁結構

### 基本章節 Basic Sections

常用的幾種章節：

<dl>
    <dt>header(頁首)</dt>
    <dd>通常是位於網頁的上方一個橫幅部分，經常包含主題名稱及Logo，整個網站共用一個頁首</dd>
    <dt>navigation bar(導覽區)</dt>
    <dd>通常是一塊在頁首下方的可以連結到網頁各個部份的區域</dd>
    <dt>main content(主要內容)</dt>
    <dd>網頁中央的一個大區域</dd>
    <dt>sidebar(側欄)</dt>
    <dd>放一些週邊訊息、連結、說明、廣告等，一般這個內容會跟著主要內容</dd>
    <dt>footer(頁尾)</dt>
    <dd>放在網頁底部的一塊，通常字體不大，標示版權，聯絡方式等訊息，有時也提供 SEO 找到本網頁的線索關鍵字</dd>
</dl>

![a simple website structure example featuring a main heading, navigation menu, main content, side bar, and footer.](https://mdn.mozillademos.org/files/12417/sample-website.png)

ch01/sampleHTML.html
