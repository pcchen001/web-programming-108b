---
title: CSS 
date: 2020-01-18 20:44:16
tags:
- Basic
- selector
categories:
- CSS
mathjax: false
---
# Cascaded Style Sheet CSS

## CSS 規則

example:

```css
body {
    color: #f02237;
    background: yellow;
}
選擇器 {
    屬性： 屬性值;
    ...
}
```

選擇器用來選定那些元素要套用大括號內的規則。

大類的選擇器分為 tagname, class, id 選擇器。有些元素還可以延伸 pseudo 類別或 pseudo元素。

css 規則可以用 style 標籤包起來，放在 head 裡面。例如

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Example1</title>
    <style>
        header{
            width: 100%;
            height: 30px;
        }
        #logo{
            float: left;
            width: 100px;
        }
        #cont{
            float:right;
            width:80px;
        }
         li {
             list-style-type: none;
             background-color:cadetblue;
             width: 60px;
             margin: 5px;
             float: left;
         }
    </style>
</head>
<body>
    <header>
        <div id="logo">RLogo</div>
        <div id="cont">contactMe</div>
    </header>
        <nav>
            <ul>
                <li>Home</li>
                <li>Buy</li>
                <li>Catalog</li>
                <li>Info</li>
            </ul>
        </nav>
    <footer>
        &copy; 2020 NUU.im
    </footer>
</body>
</html>
```

也可以用 link 或是 @import 匯入。

```html
    <link rel="stylesheet" href="body.css" type="text/css">	
```

```html
    <style>
      @import url("body.css");
    </style>
```

## Selectors

1. tagname Selector, 使用標籤名稱選擇，例如 body, h1, h2, p, div, ...

2. descentant Sector, 使用連續二個選擇器，用空格隔開，表示在第一個選擇內的第二個選擇。

   ```
   h1 {color: blue}
   i { color: red}
   h1 i { color: green}  /* h1 內的 i 元素
   ```

3. wildcard selector, 所有的元素

   ```
   * { margin: 10}
   ```

4. class selector, 元素屬性 class 所標示的。 打一個小點帶頭。(可以有二個以上的元素使用相同的 class名稱)
5. ID selector, 元素屬性 id 所標示的。(規定不能有二個元素使用相同 id名稱)
6. Attribute selector, 有此屬性的元素。不論其屬性值為何。使用中括號括起來。
7. Pseudo-Class Selectors [w3School連結](https://www.w3schools.com/css/css_pseudo_classes.asp)

| pseudo-classes    |      | selectors                                                    |
| ----------------- | ---- | ------------------------------------------------------------ |
| link              |      | :link, :visited                                              |
| user action       |      | :hover, :focus, :active                                      |
| language          |      | :lang()                                                      |
| target            |      | :target                                                      |
| UI element states |      | :enabled, :disabled, :checked, :indeterminate                |
| structural        |      | :root, :nth-child(), :nth-last-child(), :nth-of-type()、:nth-last-of-type()、:ﬁrst-child、:last-child、:ﬁrst-of-type、:last-of-type、:only-child、:only-of-type、:empty |
| negation          |      | :not()                                                       |

假類別 pseudo-classes 是指某個元素具有某種性質，例如 

| Selector                                                     | Example               | Example description                                          |
| :----------------------------------------------------------- | :-------------------- | :----------------------------------------------------------- |
| [:active](https://www.w3schools.com/cssref/sel_active.asp)   | a:active              | Selects the active link                                      |
| [:checked](https://www.w3schools.com/cssref/sel_checked.asp) | input:checked         | Selects every checked <input> element                        |
| [:disabled](https://www.w3schools.com/cssref/sel_disabled.asp) | input:disabled        | Selects every disabled <input> element                       |
| [:empty](https://www.w3schools.com/cssref/sel_empty.asp)     | p:empty               | Selects every <p> element that has no children               |
| [:enabled](https://www.w3schools.com/cssref/sel_enabled.asp) | input:enabled         | Selects every enabled <input> element                        |
| [:first-child](https://www.w3schools.com/cssref/sel_firstchild.asp) | p:first-child         | Selects every <p> elements that is the first child of its parent |
| [:first-of-type](https://www.w3schools.com/cssref/sel_first-of-type.asp) | p:first-of-type       | Selects every <p> element that is the first <p> element of its parent |
| [:focus](https://www.w3schools.com/cssref/sel_focus.asp)     | input:focus           | Selects the <input> element that has focus                   |
| [:hover](https://www.w3schools.com/cssref/sel_hover.asp)     | a:hover               | Selects links on mouse over                                  |
| [:in-range](https://www.w3schools.com/cssref/sel_in-range.asp) | input:in-range        | Selects <input> elements with a value within a specified range |
| [:invalid](https://www.w3schools.com/cssref/sel_invalid.asp) | input:invalid         | Selects all <input> elements with an invalid value           |
| [:lang(*language*)](https://www.w3schools.com/cssref/sel_lang.asp) | p:lang(it)            | Selects every <p> element with a lang attribute value starting with "it" |
| [:last-child](https://www.w3schools.com/cssref/sel_last-child.asp) | p:last-child          | Selects every <p> elements that is the last child of its parent |
| [:last-of-type](https://www.w3schools.com/cssref/sel_last-of-type.asp) | p:last-of-type        | Selects every <p> element that is the last <p> element of its parent |
| [:link](https://www.w3schools.com/cssref/sel_link.asp)       | a:link                | Selects all unvisited links                                  |
| [:not(selector)](https://www.w3schools.com/cssref/sel_not.asp) | :not(p)               | Selects every element that is not a <p> element              |
| [:nth-child(n)](https://www.w3schools.com/cssref/sel_nth-child.asp) | p:nth-child(2)        | Selects every <p> element that is the second child of its parent |
| [:nth-last-child(n)](https://www.w3schools.com/cssref/sel_nth-last-child.asp) | p:nth-last-child(2)   | Selects every <p> element that is the second child of its parent, counting from the last child |
| [:nth-last-of-type(n)](https://www.w3schools.com/cssref/sel_nth-last-of-type.asp) | p:nth-last-of-type(2) | Selects every <p> element that is the second <p> element of its parent, counting from the last child |
| [:nth-of-type(n)](https://www.w3schools.com/cssref/sel_nth-of-type.asp) | p:nth-of-type(2)      | Selects every <p> element that is the second <p> element of its parent |
| [:only-of-type](https://www.w3schools.com/cssref/sel_only-of-type.asp) | p:only-of-type        | Selects every <p> element that is the only <p> element of its parent |
| [:only-child](https://www.w3schools.com/cssref/sel_only-child.asp) | p:only-child          | Selects every <p> element that is the only child of its parent |
| [:optional](https://www.w3schools.com/cssref/sel_optional.asp) | input:optional        | Selects <input> elements with no "required" attribute        |
| [:out-of-range](https://www.w3schools.com/cssref/sel_out-of-range.asp) | input:out-of-range    | Selects <input> elements with a value outside a specified range |
| [:read-only](https://www.w3schools.com/cssref/sel_read-only.asp) | input:read-only       | Selects <input> elements with a "readonly" attribute specified |
| [:read-write](https://www.w3schools.com/cssref/sel_read-write.asp) | input:read-write      | Selects <input> elements with no "readonly" attribute        |
| [:required](https://www.w3schools.com/cssref/sel_required.asp) | input:required        | Selects <input> elements with a "required" attribute specified |
| [:root](https://www.w3schools.com/cssref/sel_root.asp)       | root                  | Selects the document's root element                          |
| [:target](https://www.w3schools.com/cssref/sel_target.asp)   | #news:target          | Selects the current active #news element (clicked on a URL containing that anchor name) |
| [:valid](https://www.w3schools.com/cssref/sel_valid.asp)     | input:valid           | Selects all <input> elements with a valid value              |
| [:visited](https://www.w3schools.com/cssref/sel_visited.asp) | a:visited             | Selects all visited links                                    |

8. Pseudo Elements  [w3schools Pseudo Elements連結](https://www.w3schools.com/css/css_pseudo_elements.asp) 一個元素的一部分，達不到整個完整元素的元素。

| Selector                                                     | Example         | Example description                                          |
| :----------------------------------------------------------- | :-------------- | :----------------------------------------------------------- |
| [::after](https://www.w3schools.com/cssref/sel_after.asp)    | p::after        | Insert something after the content of each <p> element       |
| [::before](https://www.w3schools.com/cssref/sel_before.asp)  | p::before       | Insert something before the content of each <p> element      |
| [::first-letter](https://www.w3schools.com/cssref/sel_firstletter.asp) | p::first-letter | Selects the first letter of each <p> element                 |
| [::first-line](https://www.w3schools.com/cssref/sel_firstline.asp) | p::first-line   | Selects the first line of each <p> element                   |
| [::selection](https://www.w3schools.com/cssref/sel_selection.asp) | p::selection    | Selects the portion of an element that is selected by a user |

![*](E:\wamp64\www\webP\source\_posts\art7672.tmp)[CSS Selectors](https://www.w3schools.com/cssref/css_selectors.asp) 補充，

![*](E:\wamp64\www\webP\source\_posts\art7673.tmp)[CSS Selector Tester](https://www.w3schools.com/cssref/trysel.asp)

## 常用的CSS屬性

```
width
height
font
font-family: "Times New Roman", Times, serif;  // 依序擇用
font-size
font-style
font-variant
font-weight
font-height
text-indent
text-decorate
text-spacing
text-align
text-transform
text-shadow
word-spacing
/* about list */
list-style-type
list-style-image: url(filename.git)
list-style-position: outside /* or inside */
/* color */
color:
opacity:0.3
/* background */
background
background-color:
background-image:
background-repeat: repeat-x;
background-attachment: 
background-position:
background:linear-gradient(to botton, blue, yellow) 漸層屬性值
background:radial-gradient()  漸層屬性值
```

在指定與長度有關的屬性值時，CSS 提供了許多單位。有些單位是絕對單位，例如 cm, mm, in, px, pt, pc 等。也有些是相對單位，例如 %, em, ex, ch, rem, w, vh, vmin, mmax等。

[css長度單位說明](http://www.w3schools.com/cssref/css_units.asp)

## Font Face 字形

使用字型時，我們會使用 font-family: 指定已經定義好的字型，也就是系統有提供的字型。

有時，為了讓我們的網頁使用的字型是所有的使用者都可以拿的到的，我們會定義網路字型(外部字型)，然後再用 font-family: 取用這個自訂字型。

```
@font-face {
    font-family: myFontName;
    src: url(sansation_light.woff);
}
```

定義之後，就可以用 font-family:  來取用。

```
div p {
    font-family: myFontName;
    font-weight: 500;
}
```

其實我們不常會自己設計字型，因此比較常見的情況是使用網路上免費提供的字型，Google 就提供了不少免費字型，中文字型主要是思源黑體，使用的方式是利用 @import url(https://fonts.googleapis.com/earlyaccess/notosanstc.css); 取得一系列的 font-face 的定義，這些字型包含

Noto Sans TC

...

使用範例：

```
html, body {
    font-family: 'Noto Sans TC';
    font-weight: 800;
}
```

字型引用參考連結：

​	**https://www.google.com/get/noto**

​	[Beautiful fonts with @font-face](http://hacks.mozilla.org/2009/06/beautiful-fonts-with-font-face/)

[	Open Font Library](http://openfontlibrary.org/)

​	https://www.fontsquirrel.com/

​	[Microsoft Developer Network (MSDN) @font-face reference](http://msdn.microsoft.com/en-us/library/ms530757(VS.85).aspx)

## CSS動畫

```
@keyframes myanimove{
    from { background-color: red }
    to { background-color: blue }
}

#mydiv {
    width:600px;
    height: 300px;
    animation: myanimove 5s infinite;  // 5秒內從 from 到 to, 無限循環
}
```

## Media Query

依據螢幕視窗大小以及橫直方向設定不同的 CSS 是現代網頁設計必備的彈性，有二種方式進行：一是用 link 以 media query 來指定不同的 .css 檔案；另一種則是在一個 .css 檔案中，用 @media 的 media query 來進行不同的設定。

使用 link: 

```
<link rel="stylesheet" media="screen and (min-width: 900px)" href="widescreen.css">
<link rel="stylesheet" media="screen and (max-width: 600px)" href="smallscreen.css">
```

使用 @media:

```
@media only screen and (max-width: 600px){
    body {
        background-color: lightblue;
    }
} 
```

其中 only screen and (max-width:600px) 就是所謂的 media query 。 media query 的語法可參考https://www.w3schools.com/cssref/css3_pr_mediaquery.asp。常用的屬性有　max-width: min-width: orientation: 等。orientation: landscape / orientation: portrait 。

```
@media not|only mediatype and (mediafeature and|or|not mediafeature) {
　　　　CSS-Code;
}
```

mediatype: all, print, screen, speech

mediafeature: max-width, min-width, orientation