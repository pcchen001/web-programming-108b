---
title: jQuery
date: 2020-04-09 16:15:00
tags:
- jQuery
- Ajax
categories:
- DOM
- Javascript
---

jQuery 函式庫支援了 DOM 的 API 以及 Mobile 的版面設計。關於 mobile 的版面支援，Bootstrap 後來居上，我們就不介紹 jQuery 了。

jQuery  支援的 API 一般好用，所以在許多 open source 都常看到他的影子，我們必須稍微了解。

我們整理投影片內容及 w3schools 內容。

jQuery的基本語法如下：

 $(*選擇器*).method(*參數*);    例如：

```javascript
 $("#msg").text("Hello, jQuery!");
```

jQuery支援多數的CSS3選擇器，常用的如下：

1. 選擇所有元素：例如 $("*") 或 $('*')
2. 使用HTML元素選擇元素：例如 $("h1")
3. 使用id屬性選擇元素：例如 $("#btn")
4. 使用class屬性選擇元素：例如 $(".heading")
5. 使用某個HTML元素的子元素選擇元素：例如 $("div p")
6. 使用屬性選擇元素：例如 $("[name='first_name']")
   - $("[attr]")  擁有 attr 這個屬性的所有元素
   - $("[attr=value]") 擁有 attr 這個屬性且屬性值為 value 的所有元素
   - $("[attr!=value]") 有attr 這個屬性但屬性值不為 value
   - ^= val 屬性值以 val 開頭
   - $= val 屬性值以 val 結束
   - ~= val 屬性值有包含 val 的
   - |= val 屬性值為 val 或是( val- 開頭的)
7. 使用以逗號隔開的選擇器選擇元素：例如 $("div.myClass, ul.people")
8. 使用虛擬選擇器選擇元素：例如 :visible、:hidden、:checked、 :disabled、:enabled、 :input、:selected、:password、:reset、 :radio、:text、:submit、:checkbox、:button等

### JQuery 的 $() 範例

```javascript
// demo.js
function init(){
  $("#demo").text("httxsx");

  var arr=[]
  for(var i = 0; i < 100; i++){
    arr.push(i*3);
  }
  var s="";
  for(var v in arr){
    s += (arr[v] +" ");
  }
  $("#demo").text(s);
  $("#demo").css("font-size", "20px");
  $(".demo").click(function(){
    var px = document.createElement("h1");
    var de = $("#demo");
    de.append(px);    // de 裡面的最後一個元素之後 px。請點擊數字看看
    px.innerHTML = "H1 here";
  })
}
```

jQuery 提供了好用的元素選擇器以及精簡的屬性設定函式。

配合下列 demo.html

```html
<!DOCTYPE html>
<html>
<head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="demo.js">
</script>
</head>
<body onload="init()">
<div class="demo" id="demo"></div>
</body>
</html>
```

## jQuery Events

事件處理語法：

```
// 元素.事件( 處理函式 );
$("p").click(function(){
  $(this).hide();
});

// 元素.on(事件, 處理函式);
$("p").on("click", function(){ $(this).hide()});

取消事件處理函式：
$("p").off("click", "*");
$("p").off();
```

元素由選擇器選定，事件包含下列：

| Mouse Events | Keyboard Events | Form Events | Document/Window Events |
| :----------- | :-------------- | :---------- | :--------------------- |
| click        | keypress        | submit      | load                   |
| dblclick     | keydown         | change      | resize                 |
| mouseenter   | keyup           | focus       | scroll                 |
| mouseleave   |                 | blur        | unload                 |

## jQuery 讀寫 (Get & Set)

DOM 物件的運算語法：

Three simple, but useful, jQuery methods for DOM manipulation are:

- `text()` - Sets or returns the text content of selected elements
- `html()` - Sets or returns the content of selected elements (including HTML markup)
- `val()` - Sets or returns the value of form fields
- attr() - Sets or returns the attribute of selected elements
- css() - sets or returns the css of selected elements 

這三個都是可以讀取跟寫入的。例如：

```javascript
var h1text = $("h1").text();
$("h1").text("jQuery");

var uname = $("#uname").val();
$("#uname").val( "Johnson" );

var html1 = $("#cont").html();
var s = "<div class="outside">
	    <div class="inside">Hello</div>
	</div>";
$("#cont").html(s);

// set 的範例
$("#btn1").click(function(){
  $("#test1").text("Hello world!");
});
$("#btn2").click(function(){
  $("#test2").html("<b>Hello world!</b>");
});
$("#btn3").click(function(){
  $("#test3").val("Dolly Duck");
});

// 屬性的讀寫
$("button").click(function(){
  alert($("#w3s").attr("href"));
});

$("button").click(function(){
  $("#w3s").attr("href", "http://coding.im.nuu.edu.tw/users/U0533001");
});
```

.text(), .html(), .val(), .attr() 四個也都可以使用 callback function 來進行內容設定：

```javascript
$("button").click(function(){
  $("#w3s").attr("href", function(i, origValue){
    return origValue + "/jquery/";
  });
});
```

將原來的內容當作 引數 傳入 callback function。

## jQuery Add Node(s)

掛上節點，也就是新增元素，的指令有下列幾個：

- `append()` - Inserts content at the end of the selected elements 子節點的最後
- `prepend()` - Inserts content at the beginning of the selected elements 子節點的最前面
- `after()` - Inserts content after the selected elements 本節點之後
- `before()` - Inserts content before the selected elements 本節點之前

append, prepend 是加到子節點。after, before 是加到平行兄弟節點。

```js
let newp = "<p>new paragraph</p>";
let newq = $("<p></p>").text("new text paragraph");
let newc = document.createElement("p"); newc.innerHTML = "new innerHTML paragraph";
$("#mydiv").append(newp);        // 加上 1 個
$('#mydiv').after(newq, newc);   // 依序加 2 個以上
```



## jQuery Remove Node(s)

移除節點的指令有二個：

- `remove()` - Removes the selected element (and its child elements)
  - 移除節點 $("#tod").remove();
- `empty()` - Removes the child elements from the selected element
  - 清除所有子節點 $("#clx").empty();

## jQuery CSS Classes

為某些元素新增 class 或是移除 class 是更改 css 設定的好策略之一。可以使用下列方法：

- `addClass()` - Adds one or more classes to the selected elements
- `removeClass()` - Removes one or more classes from the selected elements
- `toggleClass()` - Toggles 切換 between adding/removing classes from the selected elements 
- `css()` - Sets or returns the style attribute

```javascript
// 假設的 css 內容
.important {
  font-weight: bold;
  font-size: xx-large;
}
.blue {
  color: blue;
}

// 新增 class 
$("button").click(function(){
  $("h1, h2, p").addClass("blue");
  $("div").addClass("important");
});

// 移除 class
$("button").click(function(){
  $("h1, h2, p").removeClass("blue");
});
```

## jQuery css()

這個函式可以直接設定 css 內容以及取得 css 內容。

取得： css("propertyname");    e.g. $("p").css("background-color");

設定：css("propertyname", "value");  e.g. $("p").css("background-color", "blue");

## width() & height()

這個函式可以直接設定或取得元素的寬與高。

取得：$(selector).width(), $(selector).height()  e.g. var w = $("h1").width();

設定：$(selector).width("200px"); 

## $(function(){});

使用 jQuery 初始化執行時，可以使用 $(document).ready( function(){});  或是使用 $(function(){...});

這個函式會在網頁載入完成後才開始執行。我們可以省去寫 onload="init()" 的麻煩。

## jQuery $.each()

$.each() 可以將物件或是陣列進行迴圈處理。

```
語法
$.each(object, callback)
範例
$.each([1,3,5,7], function(index, value){ console.log(index+"--"+value)});
$.each({"one":100, "two":300}, function(index, value){console.log(index+": "+value)});
```

## jQuery 幾個渲染動畫效果

.show() .hide() 顯示隱藏  .toggle()切換

.slideDown(600), .slideUP(600)  0.6秒下滑顯示，上滑隱藏

.fadeIn(600), .fadeOut(600) 0.6秒淡入(顯示)，淡出(隱藏

.fadeTo(250, 0.25, callback) 0.25秒 淡出到 0.25 的程度，完成後 callback

.animation({css 設定}, 1500);   1.5 秒完成動畫

```
$("#enlarge").on("click", function() {
        $("img").animate({
          width: "300px",
          opacity: 1,
          borderWidth: "10px"
        }, 1500);
      });

```

jQ9.html ~ jQ12.html

jQ13.html 動畫及 .each() 的說明範例

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src="https://code.jquery.com/jquery-3.1.0.min.js"></script>
    <style>
      div, p{
        width: 80px;
        height: 40px;
        top: 0;
        margin: 0;
        position: absolute;
        padding-top: 5px;
      }
      p{
        background: azure;
        text-align: center;
      }
      div{
        background: purple;
      }
    </style>
  </head>
  <body>
    <p>猜錯了</p>
    <div></div>
    <p>猜錯了</p>
    <div></div>
    <p>猜對了！ＹＡ！</p>
    <div></div>
 
    <script>
      var getPos = function( n ) {
        return (Math.floor( n ) * 90) + "px";
      };
	  
      $( "p" ).each(function( n ) {
        var r = Math.floor( Math.random() * 3 );
        var tmp = $( this ).text();
        $( this ).text( $( "p:eq(" + r + ")" ).text() );
        $( "p:eq(" + r + ")" ).text( tmp );
        $( this ).css( "left", getPos( n ) );
      });
	  // p:eq(r) 第 r 個 p 
      // 這個 p 跟第 r 個 p 互換 text
        
      $( "div" ).each(function( n ) {
        $( this ).css( "left", getPos( n ) );
      })
      .css( "cursor", "pointer" )
      .on("click", function() {
        $( this ).fadeTo( 250, 0.25, function() {
          $( this ).css( "cursor", "" )
          .prev()
          .css({
            "font-weight": "bolder",
            "font-style": "italic"
          });
        });
      });
  </script>
  </body>
</html>
```



## jQuery Ajax

jQuery 提供完整的 Ajax 應用介面，簡要使用 load, get, post 即可，特殊需要時，請參考 [jQuery Ajax](https://www.w3schools.com/jquery/jquery_ref_ajax.asp) 的詳細指令。

### $.load()

```
語法
$(selector).load(URL,data,callback);
範例
$("#div1").load("demo_test.txt");  // 載入的內容放到 #div1 裡面
```

.data 是要送出 request 跟著送出的。若沒有或是 String, 以 GET 送出request，data 若為 object, 則以 POST 送出request。

## $.get()

```
語法
$.get(url, callback)
範例
$.get("http://coding.im.nuu.edu.tw/gettext.php?uname=pochi&uage=12", function(data, status){ console.log(data)});
```

使用 get 要將 form 的資料轉成字串，接在 url 裡面。



## $.post()

```javascript
語法
$.post(URL,data,callback);
範例
$("button").click(function(){
  $.post("myfunc.php",
  {
    name: "Donald Duck",
    city: "Duckburg"
  },
  function(data, status){
    alert("Data: " + data + "\nStatus: " + status);
  });
});
```

使用 post 要將 form 的資料轉成物件資料。

