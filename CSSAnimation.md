---
title: CSS Animation(動畫效果)
date: 2020-03-25 20:57:10
tags:
- CSS
- Animation
categories:
- CSS
---

參考來源：

[Mozilla CSS Animation](https://developer.mozilla.org/zh-TW/docs/Web/CSS/CSS_Animations/Using_CSS_animations)

CSS style configuration 用來定義排版效果，如果我們可以簡單的設定二種 configuration 之間的過場效果，就可以創造出更生動的網頁畫面。 CSS animation 就是完成這件事情的工具。

你只需要定義兩個部份：

1. 動畫的最初及結尾 
2. 動畫轉變的方式。

你可以使用 `animation` property 或其 sub-properties 來創建 CSS 動畫的細節( 諸如轉化時間等 )。

但這並不能決定動畫的外觀，外觀的部份我們將在下面的 [使用關鍵影格定義動畫流程](https://developer.mozilla.org/zh-TW/docs/Web/CSS/CSS_Animations/Using_CSS_animations#使用關鍵影格定義動畫流程) 介紹。

 這裡是 `animation` property 的 sub-properties：

- `animation-delay`

  延遲時間，等多久開始動畫。

- `animation-direction`

  定義是否動畫播放完畢後，會反向播放。

- `animation-duration`

  定義動畫完成一次週期的時間。

- `animation-iteration-count`

  定義動畫重複的次數。你可以用 `infinite` 來讓動畫永遠重複播放。

- `animation-name`

  定義**關鍵影格** `@keyframes` 的名字。

- `animation-play-state`

  控制動畫的播放狀態。有 pause 和 running 兩種值，後者為預設值。

- `animation-timing-function`

  定義動畫轉變時時間的加速曲線 (例如 linear)。

- [`animation-fill-mode`](https://developer.mozilla.org/zh-TW/docs/Web/CSS/animation-fill-mode)

  定義元素在動畫播放外(動畫開始前及結束後)的狀態。

範例開始：

```html
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        p {
            font-size: large;
            animation-delay: 2s;
            animation-duration: 3s;
            animation-name: slidein;　
        }
        @keyframes slidein {
          from {
            margin-left: 100%;
            width: 300%;
          }
          to {
            margin-left: 0%;
            width: 100%;
          }
        }
      </style>
</head>
<body>
    <p>Hello World </p>
</body>
```

## 使用關鍵影格定義動畫流程

首先，我們使用 @keyframes 定義關鍵影格。基本上　from , to 二種 css 組態。

接著，animation-delay 延遲2秒，以 3秒 的時間完成從 from 到 to 的變化。

除了 from 及 to 之外，我們也可以增加關鍵影格，例如：

```css
75% {
  font-size: 300%;
  margin-left: 25%;
  width: 150%;
}
```

間隔中的 75% 時，組態改變到目前狀態。

定義動畫的其他性質：

```CSS
.slidein {
  animation-delay: 2s;
  animation-duration: 3s;
  animation-name: slidein;
  animation-iteration-count: 3;　　/*或是 infinite */
  animation-direction: alternate;  /* normal, reverse, alternate, alternate-reverse*/
}
```

