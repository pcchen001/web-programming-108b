---
title: HTML 表格與表單
date: 2020-03-18 20:44:16
tags:
- table
- form
categories:
- HTML
mathjax: false
---



# 表格

主要標籤： table, tr, td, th, caption, col, colgroup, tbody, thead, tfoot.

------

table 的屬性： border 格線粗細, rules"groups" 依照 thead, tbody, tfood 畫格線。

thead, tbody, tfoot : 表個標頭，內容，註。有時不分那麼細，這三個就省略了！

tr, td, th:  表格一行 及 表格一格。 td 與 th 是相同的，只是 th 出現在 thead, td 出現在 tbody。

td 或 th 的屬性： rowspan="2" 表占用二行的高度。colspan="3" 表占用三直列的寬度。

caption: 表格的標題。

```html
<table border="1" rules="groups">
    <caption>表一、 電子類股上市公司</caption>
      <thead>
        <tr>
          <th rowspan="2">&nbsp;</th>
          <th colspan="2">104年</th>
          <th colspan="2">105年</th>
        </tr> 
        <tr>        
          <th>營收（百萬）</th> 
          <th>純益（百萬）</th>
          <th>營收（百萬）</th> 
          <th>純益（百萬）</th>
        </tr>
      </thead>
      <tbody>   
        <tr> 
          <td>幸福</td>
          <td>3953</td>
          <td>1245</td>
          <td>3687</td>
          <td>763</td>
        </tr> 
        <tr> 
          <td>飛達</td>
          <td>5193</td>
          <td>1420</td>
          <td>4143</td>
          <td>932</td>
        </tr>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="5">註：年度營收實際數字依財務公告為準。</td>
        </tr>
      </tfoot>    
</table>
```

colgroup 及 col 用來對直列分群。

```html
<p>Table with colgroup and col</p>
<table>
  <colgroup>
    <col style="background-color: #0f0">
    <col span="2">
  </colgroup>
  <tr>
    <th>Lime</th>
    <th>Lemon</th>
    <th>Orange</th>
  </tr>
  <tr>
    <td>Green</td>
    <td>Yellow</td>
    <td>Orange</td>
  </tr>
</table>
```

# 表單

form, button, input, label, option, output, progress, select, textarea。

<form action="" method="get" class="form-example">
  <div class="form-example">
    <label for="name">Enter your name: </label>
    <input type="text" name="name" id="name" required>
  </div>
  <div class="form-example">
    <label for="email">Enter your email: </label>
    <input type="email" name="email" id="email" required>
  </div>
  <div class="form-example">
    <input type="submit" value="註冊">
  </div>
</form>

```html
<form action="" method="get" class="form-example">
  <div class="form-example">
    <label for="name">Enter your name: </label>
    <input type="text" name="name" id="name" required>
  </div>
  <div class="form-example">
    <label for="email">Enter your email: </label>
    <input type="email" name="email" id="email" required>
  </div>
  <div class="form-example">
    <input type="submit" value="註冊">
  </div>
</form>
```

## input

給一個範例如下：

```html
<form method="post" action="confirm.php">
	姓&nbsp;&nbsp;&nbsp;名：<input type="text" name="UserName" size="40"><br>
    E-Mail：<input type="email" name="UserMail" size="40" value="username@mailserver"><br>
    年&nbsp;&nbsp;齡：
      <input type="radio" name="UserAge" value="Age1">未滿20歲
      <input type="radio" name="UserAge" value="Age2" checked>20~29
      <input type="radio" name="UserAge" value="Age3">30~39
      <input type="radio" name="UserAge" value="Age4">40~49
      <input type="radio" name="UserAge" value="Age5">50歲以上<br>
    您使用過哪些廠牌的手機？
      <input type="checkbox" name="UserPhone[]" value="hTC" checked>hTC
      <input type="checkbox" name="UserPhone[]" value="Apple">Apple
      <input type="checkbox" name="UserPhone[]" value="ASUS">ASUS
      <input type="checkbox" name="UserPhone[]" value="acer">acer<br>
	您使用手機時最常碰到哪些問題？<br>
      <textarea name="UserTrouble" cols="45" rows="4">連線速度不夠快</textarea><br>
	您使用過哪家業者的門號？(可複選)
      <select name="UserNumber[]" size="4" multiple>
        <option value="中華電信">中華電信
        <option value="台灣大哥大" selected>台灣大哥大
        <option value="遠傳">遠傳
        <option value="威寶">威寶
      </select><br>
      <input type="submit" value="提交">
      <input type="reset" value="重新輸入">
</form>
```

介面如下：

<form style="background: lightyellow; padding: 1em;">
姓&nbsp;名：<input type="text" name="UserName" size="40"> <br>
E-mail: <input type="email" name="UserMail" size="40" value="sername@mailserver"> <br>
年齡：<input type="radio" name="UserAge" value="Age1"> 未滿20歲
    <input type="radio" name="UserAge" value="Age2"> 20-29
    <input type="radio" name="UserAge" value="Age3"> 30-39
    <input type="radio" name="UserAge" value="Age4"> 40-49
    <input type="radio" name="UserAge" value="Age5"> 50歲以上<br>
    您使用過哪些廠牌的手機？
   <input type="checkbox" name="UserPhone[]" value="hTC" checked>hTC
   <input type="checkbox" name="UserPhone[]" value="Apple">Apple
   <input type="checkbox" name="UserPhone[]" value="ASUS">ASUS
   <input type="checkbox" name="UserPhone[]" value="acer">acer<br>
  您使用手機時最常碰到哪些問題？<br>
   <textarea name="UserTrouble" cols="45" rows="4">連線速度不夠快</textarea><br>
  您使用過哪家業者的門號？(可複選)
   <select name="UserNumber[]" size="4" multiple>
    <option value="中華電信">中華電信
    <option value="台灣大哥大" selected>台灣大哥大
    <option value="遠傳">遠傳
    <option value="威寶">威寶
   </select><br>
   <input type="submit" value="提交">
   <input type="reset" value="重新輸入">
  </form>

除了上述常見 input type 之外，還有幾個也常用到：

```html
<input type="password" name="passwd" size="10">      密碼欄位
<input type="hidden" name="author" value="jkchen">   隱藏欄位，用來傳遞不給使用者看到的訊息
<input type="file" name"upfile" size="50">　上傳檔案，詳細會在 php 介紹。

<input type="email">
<input type="url">
<input type="search">
<input type="number" min="0" max="20" step="2">
<input type="range" min="0" max="24" step="3">
<input type="color">  跳出一個選色對話框
<input type="tel" pattern="[0-9]{4}(\-[0-9]{6})" placeholder="例如0926-388888" title="例如0926-388888">
<input type="date" name="birthday">
<input type="time" name="meeting">
<input type="datetime" name="activity">
<input type="month" name="duemonth">
<input type="week" name="workday">
<input type="datetime-local" name="activity">
```

非input 的還有

label 是用來協助標示輸入框的元素。

fieldset 及 legend 是用來將輸入欄位進行分群，並對每個分群給一個標註(legend)。

```html
<label for="username">姓名</label> <input type="text" id="username">

<fieldset><legend>個人資料</legend>
<input type="text" name="username">
<input type="number" name="age">
</fieldset>

<button>
    按下
</button>  一般按鈕
```

表單是用來提供我們處理資料時的資料輸入，輸入後的處理通常送到後端去處理，例如 php 程式碼。

基本的寫法是將要處理的程式碼檔案寫在 form 的 action 中：

```
<form method="post" action="confirm.php">
```

對應的 confirm.php 可以如下：

```php
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
	<title>大哥大使用意見調查表確認網頁</title>
  </head>
  <body background="free0.gif">
    <p><img src="free1.jpg"></p>
    <?php
      $Name = $_POST["UserName"];
      $Mail = $_POST["UserMail"];
      switch($_POST["UserAge"])
      {
        case "Age1":
          $Age = "未滿20歲";
          break;
        case "Age2":
          $Age = "20~29";
          break;
        case "Age3":
          $Age = "30~39";
          break;
        case "Age4":
          $Age = "40~49";
          break;
        case "Age5":
          $Age = "50歲以上";
      }
      $Phone = $_POST["UserPhone"];
      $Trouble = $_POST["UserTrouble"];
      $Number = $_POST["UserNumber"];
    ?>
    <p><i><?php echo $Name; ?></i>，您好！您輸入的資訊如下：</p>
      電子郵件地址：<?php echo $Mail; ?><br>
      年齡：<?php echo $Age; ?><br>
      曾經使用過的手機廠牌：<?php foreach($Phone as $Value) echo $Value.'&nbsp;'; ?><br>
      使用手機時最常碰到的問題：<?php echo $Trouble; ?><br>
      使用哪家業者的門號：<?php foreach($Number as $Value) echo $Value.'&nbsp;'; ?>
  </body>
</html>
```



### 其他

output，用來顯示計算結果或處理結果。例如：

```html
<form onsubmit="return false" oninput="sum.value=num1.valueAsNumber + num2.valueAsNumber">
  <input name="num1" type="number" step="any">
  <input name="num3" type="number" step="any">
</form>  
```

progress，用來顯示進度　（進度條）。例如：

```html
  <p>檔案下載進度：<progress id="pb" max="100"><span>0</span>%</progress></p>
    <script>
      var progressBar = document.getElementById('pb');
      var i = 10;
      function updateProgress(newValue) {
        progressBar.value = newValue;
        progressBar.getElementsByTagName('span')[0].innerText = newValue;	
      }
      function showProgress() {
        if (i <= 100) {
		  updateProgress(i);
	      i += 10; 
		}
		else clearInterval();
      }
      window.setInterval("showProgress();", 500);
    </script>
```

meter，用來顯示某個範圍內的比例或量標。例如：

```html
     <p>王大明得票率：<meter min="0" max="100" value="12">12%</meter></p>
     <p>孫小美得票率：<meter min="0" max="100" value="75">75%</meter></p>
```

optgroup, (option group) 下拉選單分群標籤。例如：

```html
      <form>
        <label>請選擇要觀賞的節目：</label>
        <select name="TVlist">
          <optgroup label="新聞頻道">
            <option value="news1">TVBS-N
            <option value="news2">三立新聞台
            <option value="news3">民視新聞台
			<option value="news4">年代新聞台
          <optgroup label="娛樂頻道">
            <option value="shows1">大學生了沒
            <option value="shows2">型男大主廚
            <option value="shows3">食尚玩家
        </select>
        <input type="submit">
      </form>
```



有些第三方廠商會提供免費的表單套件方便網頁設計者使用。

bootstrap 4 也有不少表單模板套件。