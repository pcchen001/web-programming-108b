---
title: Application GoogleMap
date: 2020-04-29 08:00:44
tags:
- google
- map
- api
categories:
- Application
---

範例 1 基礎 google map

```html
<!DOCTYPE html>
<html>
  <head>
    <style>
      /* Set the size of the div element that contains the map */
      #map {
        height: 400px;  /* The height is 400 pixels */
        width: 100%;  /* The width is the width of the web page */
       }
    </style>
  </head>
  <body>
    <h3>My Google Maps Demo</h3>
    <!--The div element for the map -->
    <div id="map"></div>
    <script>
        // Initialize and add the map
        function initMap() {
          // The location of Uluru
          var uluru = {lat: -25.344, lng: 131.036};
          // The map, centered at Uluru
          var map = new google.maps.Map(
              document.getElementById('map'), {zoom: 4, center: uluru});
          // The marker, positioned at Uluru
          var marker = new google.maps.Marker({position: uluru, map: map});
        }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap">
    </script>
  </body>
</html>
```

[google map 地圖應用 開發者說明](https://developers.google.com/maps/documentation/javascript/adding-a-google-map?hl=zh_TW)

google map api 需要使用者的API_KEY, google 接受免費申請成為開發者，但通常需要信用卡或是其他的條件。方便大家學習，大家可以暫時使用我的 api "AIzaSyDg1hWLwR2s0BYPJLV0NItq5HkRSQfGbf0"。

google map 使用 jsonp 的概念，再通過認證後，提供你所需要的程式庫支援。所以我們的程式要當作參數傳給google, 例如 initMap。

練習： 利用 navigator 取得目前所在的位置，標上在你的 google 地圖上。



範例 2 動態飛行範例 (googlemap_fly.html)

```html
<!DOCTYPE html>
<html>
  <head>
    <title>Simple Map</title>
    <meta name="viewport" content="initial-scale=1.0">
    <meta charset="utf-8">
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 100%;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <div id="map"></div>
    <script>
      var map;
      var lat, lng;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          center: {lat: 24.865, lng: 120.95},
          zoom: 16,
          mapTypeId: 'satellite',   // 
          heading: 90,
          tilt: 45
        });
        autoRotate();
        lat = 24.865; lng = 120.95;
      }
      /*
      roadmap: displays the default road map view. This is the default map type.
      satellite: displays Google Earth satellite images.
      hybrid: displays a mixture of normal and satellite views.
      terrain: displays a physical map based on terrain information.
      */
      function rotate90() {
        var heading = map.getHeading() || 0;
        map.setHeading(heading +90);
        console.log( map.getHeading() +"heading");
        var myLatlng = new google.maps.LatLng(lat+=0.0001, lng+=0.0001);
        map.setCenter(myLatlng);
      }

      function autoRotate() {
          setInterval(rotate90, 100);
      }
    </script>
    <script async defer 
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDg1hWLwR2s0BYPJLV0NItq5HkRSQfGbf0&callback=initMap" >
	</script>
  </body>
</html>
```

若網路速度夠快，0.1秒移動一點。