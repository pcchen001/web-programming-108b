---
title: JS 陣列
date: 2020-03-20 18:12:02
tags:
- Array
- JavaScript
categories:
- JavaScript
---

### 陣列

JavaScript 的陣列其實是一種類別為 Array 的特殊物件。它們的運作方式跟正常的物件很像（數字性的屬性只能透過 [] 語法進行存取），不過有個神奇的屬性，叫做「length」（長度）。這個屬性一定是陣列最高索引數加一。建立陣列的舊方法如下：

```js
> var a = new Array();  // 或是 var a = [];
> a[0] = "狗";
> a[1] = "貓";
> a[2] = "雞";
> a.length
3
```

比較方便的語法便是使用陣列實體語法：

```js
> var a = ["狗", "貓", "雞"];
> a.length
3
```

在陣列實體語法結尾留個空逗點在各瀏覽器間結果參差不齊，所以最好不要這樣：

```js
> var a = ["狗", "貓", "雞", ]; //最好不要這麼做
```

注意－－`array.length` 不一定是陣列的項目數。比方說：

```js
> var a = ["狗", "貓", "雞"];
> a[100] = "狐";
> a.length
101
```

別忘了－－陣列的 length 就是最高索引數加一。此時若我們檢視陣列 a 會發現 3~99 是沒有定義的。話雖如此，但是若我們 pop 掉最後一個，也就是 a[100]被刪掉，然後再 push() 一個項目，這個項目的索引會是 100，而不是 3。

如果你查詢一個不存在的陣列索引，得到的就是 `undefined`：

```js
> typeof(a[90])
undefined
```

假若一個陣列的索引沒有跳過，我們可以像下列一樣在陣列上做迴圈：

```js
for (var i = 0; i < a.length; i++) {
    //處理 a[i]
}
```

這樣不是很有效率，因為每迴圈一次就會查詢一次 length 屬性。比較好的做法是：

```js
for (var i = 0, len = a.length; i < len; i++) {
    //處理 a[i]
}
```

一個更棒的寫法是：

```js
for (var i = 0, item; item = a[i]; i++) {
    //處理 item
}
```

這裡設定了兩個變數。`for` 迴圈中間指定變數值的部分會被測試是否為「真的」(truthy)－－如果成功了，迴圈便會繼續。由於 `i` 每次都會加一，陣列內的每個項目會被照順序指定到變數 item。當偵測到「假的」(falsy) 項目時（如 `undefined`）迴圈便會停止。

注意－－這個小技巧只該用在你確定不會含有「假的」值的陣列（比如說一陣列的物件或 [DOM](https://developer.mozilla.org/zh_tw/DOM) 節點）。假如你在可能含有 0 的數字資料或可能含有空字串的字串資料上做迴圈，最好還是用 `i, j` 的方式。

另外一個做迴圈的方法是用 `for...in` 迴圈。不過，假如有人用 `Array.prototype` 新增新的屬性，那些屬性也會被這種迴圈讀到：

```js
for (var i in a) {
  //處理 a[i]
}
```

假如你要在陣列結尾加入項目，最安全的方法是這樣：

```js
a[a.length] = item;                 //同 a.push(item);
```

由於 `a.length` 一定是最高索引數加一，你可以很確定你指定到的是陣列結尾的空間。

Javascript 的陣列是一個類別Array 的 instance. 因此我們可以用 Array 類別的方式來說明：

Array 有 3 個靜態方法：

Array.from( arrayLike ), 將 arrayLike 轉成一個陣列。例如

```
console.log(Array.from('foo'));
// expected output: Array ["f", "o", "o"]

console.log(Array.from([1, 2, 3], x => x + x));
// expected output: Array [2, 4, 6]
```

Array.isArray( v ), 判斷 v 是否為一個 Array。

Array.of( e1, e2, ...), 將一組引數組成一個陣列。相當於 [e1, e2, e3, ...]

var arr = Array.of( 1, 3, 5); // var arr=[1, 3, 5];

Array 的 instance 方法(成員函式) 可以分為三大類： mutator methods(更動), accessor methods(存取), 以及 iteration methods(迭代)。

### Mutator Methods

 假設 a 是一個陣列， var a = [1, 3, 5];

a.copyWithin( target, start, end),  將 a 陣列的第 start 到 第 end 複製到 target。

a.[`fill(value[, start[, end\]])`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/fill), 填入 value 。

a.pop(), 刪除最後1 個

a.[`push(element1[, ...[, elementN\]])`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/push), 新增在後面

a.reverse(), 反排列

a.shilf(), 刪除第1個

a.[`unshift(element1[, ...[, elementN\]])`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/unshift), 新增在前面

a.sort() 排列

a.[`splice(start[, deleteCount[, item1[, item2[, ...\]]]])`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/splice) , 新增或取代部分元素。範例：

```
const months = ['Jan', 'March', 'April', 'June'];
months.splice(1, 0, 'Feb');
// inserts at index 1
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "June"]

months.splice(4, 1, 'May');
// replaces 1 element at index 4
console.log(months);
// expected output: Array ["Jan", "Feb", "March", "April", "May"]
```

 ### Accessor Methods

a.concat( array ), a.concat(e), a.concat(e1, e2, ...)  連接

a.filter( callbackFunction(e)), 根據 callbackFunction(e) 濾掉callbackFunction 返回 false 的。

a.include( valueToFind ), 是否包含

a.indexOf( searchElement ) 索引, -1 表示沒找到

a.join( ', ' ), 將陣列合成一個字串，元素以 ', ' 連接。

a.lastIndexOf( searchElement) 索引，從最後一個開始找

a.slice( begin[, end]), 切片。

### Iteration Methods

a.entries(), 取得陣列a 的迭代子。

```
const array1 = ['a', 'b', 'c'];

const iterator1 = array1.entries();

console.log(iterator1.next().value);
// expected output: Array [0, "a"]

console.log(iterator1.next().value);
// expected output: Array [1, "b"]
```

a.every( callbackFunction ) 是否每一個元素代入 callbackFunction 都是 true? 

​    例如： if ( a.every( e => e> 10) ) console.log(" every e is satisfied! ");

a.find( callbackFunction ), 找出代入 callbackFunction 返回 true 的元素。

​    例如： const found = array1.find(element => element > 10);

a.findINdex( callbackFunction), 返回索引， -1 表示沒找到。

a.forEach( callbackFunction ), 將每一個元素代入執行。

a.keys() 取得一個以 keys 組成的陣列的迭代子。例如：

```
    const array1 = ['a', 'b', 'c'];
    const iterator = array1.keys();

    for (const key of iterator) {
      console.log(key);
    }
```

a.map( callbackFunction )，每個元素經由 callbackFunction 計算，形成新的陣列。

a.reduce( callbackFunction( accumulator, currentValue)), 累積計算。currentValue若沒有給，預設為 0, 例如：

```javascript
const array1 = [1, 2, 3, 4];
const reducer = (accumulator, currentValue) => accumulator + currentValue;

// 1 + 2 + 3 + 4
console.log(array1.reduce(reducer));
// expected output: 10

// 5 + 1 + 2 + 3 + 4
console.log(array1.reduce(reducer, 5));
// expected output: 15
```

a.reduceRight( callbackFunction( accumutltor, currentValue)), 累積計算，陣列元素從右邊開始。

```
const array1 = [[0, 1], [2, 3], [4, 5]].reduceRight(
  (accumulator, currentValue) => accumulator.concat(currentValue)
);

console.log(array1);
// expected output: Array [4, 5, 2, 3, 0, 1]
```

a.some( callbackFunction ), 與 every 呼應，every 要每一個都符合， some 則是只要有一個符合就返回 true 。

### Iterator

迭代子型態只有一個函式next()。 it.next() 返回的物件是 {value, done}。value 是這個 it 所指向的物件， done 是這個物件是否是it 序列中的最後一個的下一個。當 it.next().done 是false, 表示 it.next().value 是沒有定義的！

大部分情況　for, map, filter 等陣列指令就可以對陣列的每一個元素工作，但若希望能洞察內部作業，可以使用 iterator ，就能明白這些指令是如何工作的！

取得 Iterator 的方式有

1. 從一個陣列取得 it = arr.values()

2. ```js
   const it = makeRangeIterator(1, 10, 2);
   ```

a.values() 取的陣列值的**迭代子,** 形態上是 Array Iterator。 

```javascript
    const array1 = ['a', 'b', 'c'];
    const iterator = array1.values();

    for (const value of iterator) {
      console.log(value);
    }

    while( !iterator.next().done){
        console.log(iterator.next().value);
    }
```

### Others

```js
const arr1=[1, 3, 5];
const narr=[...arr1, 7];　　// arr1 增加一個元素
```

narr = [1,3,5,7] 。三個小點表示將一個陣列張開成元素的序列，我們可以用這個運算將一個陣列加上一個元素，或是將二個陣列連結成一個陣列。

