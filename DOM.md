---
title: DOM HTML5
date: 2020-04-01 22:03:49
tags:
- dom
- html5
- web api
categories:
- DOM

---

# DOM Document Object Model

World Wide Web 瀏覽器各家不同，但都支援 W3C 制定的 Web API。這些 Web API 就是瀏覽器提供的 javascript 的函式庫，我們在撰寫前端程式時，這些函式庫是強大的軍火庫。

[Web API](https://developer.mozilla.org/en-US/docs/Web/API) 非常豐富，其基礎在於 DOM 。因此我們必須從 DOM 開始介紹。

DOM 簡單的說就是讓我們在寫 HTML 網頁時，網頁上的任何東西都有程式可以對應的變數物件，如此一來，我們就可以好好撰寫程式來進行我們的邏輯以及排版。

## window 物件

網頁開啟時，預設在一個 全域的 window 物件下因此 window.location 可以只寫 location。

window 物件常用的有四個： location, screen, navigator, history。

1. location 紀錄目前開啟的 URI, 我們可以更動 location 的內容來轉換瀏覽的頁面網址。
   - location 屬性：lhash(#), host, hostname, href, pathname, port, protocol, search
   - location 方法：
     - reload(), 重載原來的url
     - assign( url )　載入新網頁
     - replace( newurl ), 將目前的網址更改為新網址
2. screen 紀錄螢幕屬性，包含 height, width, availHeight, availWidth, colorDepth 等

3. navigator 包含瀏覽器的相關描述與系統資訊。

4. history 是瀏覽紀錄。


我們可以在瀏覽器的 console 直接體驗一下這些物件的屬性。

## document 物件

document 物件是window 物件的子物件，window 物件代表的是一個瀏覽器視窗、索引標籤或框架，而document 物件代表的是HTML 文件本身，我們可以透過它存取HTML 文件的元素，包括表單、圖片、表格、超連結等。

一個網頁可以對應一個 document tree, 例如：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Sample Document</title>
</head>
<body>
    <h1>An HTML Document</h1>
    <p>thie is a <i>sample</i> document</p>
</body>
</html>
```



![image-20200315222013986](image-20200315222013986.png)



這個 document tree 上的每一個節點，都屬於 Node 類別，在根據不同的性質屬於不同的子類別。我先放一張Node 型別的繼承圖做參考。

![image-20200315222547407](image-20200315222547407.png)



我們注意到大部分的節點都是屬於 Element, 特別是 HTMLElement 。字串通常屬於 CharacterData 下的 Text 節點。

一個網頁對應一個 window.document 物件。 因為 window 是預設，因此可以只寫 document. 

document 有幾個常用的屬性：

1. title : head 內的 title 元素
2. url : 這個網頁的 url
3. charset : 網頁的編碼
4. body : 對應唯一的 body 元素。
5. cookie : 瀏覽器對應的 cookie 資料
6. images : 網頁內所有 img 元素的陣列
7. links : 
8. scripts :  

document 的幾個常用的事件處理器設定屬性： (一般屬性名稱為 on 加上事件名稱，例如 onclick, onclose 等)

1. .onclick
2. .onblur
3. .onfocus
4. .onclose
5. .ondrop
6. .ondragstart,.ondragover, dragenter, dragexit, dragleave
7. .oninput
8. .onkeydown, onkeypress, onkeyup
9. .onload, 
10. .onmousemove, mouseup, mouseover, mousedown, …

在實務上， document 的事件與各個元素的事件幾乎都一樣。

document 的幾個常用方法：

1. createAttribute( attName ), 指定 attName 的元素，通常會再被　setAttributeNode 到元素上。

   ```javascript
   var node = document.getElementById("div1");
   var a = document.createAttribute("my_attrib");
   a.value = "newVal";
   node.setAttributeNode(a);
   console.log(node.getAttribute("my_attrib")); // "newVal"
   ```

2. createElement( tagName ), 指定 tagName 的元素 

3. createTextNode("textcontent"), 文字節點通常出現在元素的內部。

   ```html
     <script>
         var newtext = document.createTextNode("content added"),
         p1 = document.getElementById("p1");
   
         p1.appendChild(newtext);
         </script>1
   ...1
     <p id="p1"> some text</p>
   ```

   結果： some text content added

4. createComment(data)

5. write(data), writeln(data) : 直接將 data 寫道網頁上，一般比較少這樣寫，很容易蓋掉其他內容。

   - 在 write 之前及之後，會加上 document.open(); 及 document.close();

6. getElementsByTagName(), getElementsByClassName(), getElementById(), 

7. querySelectorAll(selector) 回傳陣列, querySelector( selector ) 回傳第一個符合的

   - selector 接近 CSS 的 selector, 例如 querySelector("#eid"), querySelector("body h1")
   - 對應 document, element 也有 querySelectorAll 及 querySelect 二個方法。

8. importNode(nodeToBeCloned, true) 複製一個節點。

   - 範例： var clone = document.importNode(nodetobecloned, true);  t.appendChild(clone);
   - 在使用 template 時，由於 template 元素並不屬於 document, 因此要將 template 裡面的元素複製出來，必須使用 importNode。 [importNode](https://developer.mozilla.org/en-US/docs/Web/API/Document/importNode)

9. ...

DOM 中大部分的元素都屬於 HTMLElement, 也屬於 Element 。因此這二個類別的所有屬性及方法是共用的。

大部分取得元素的方式是 document.getElementByID, document.getElementsByTagName。

# Elements

HTMLElement 的常有屬性有：

1. id

2. tagName

3. attributes 屬性陣列

4. classList 將class name 以陣列表示 className 將class name 以字串表示。

   ```
   <div id="d1" class="small blue top"> a div content </div>
   d1.className: "small,blue,top"
   d1.classList: ["small", "blue", "top"]
   ```

5. isContentEnable

6. innerHTML  夾在元素前標籤跟後標籤之間的部分。

7. outerHTML  一個元素包含標籤的所有部分

8. textContent

9. style 又有下列屬性
   1. color
   2. font-size
   3. ...

常用的方法：

1. setAttribute("attributeName", "attributeValue")
   1. 例如 e.setAttribute("class", "big"), setAttribute("style", "color: red; font: 16")
2. 

## Nodes

DOM 中所有的物件都是 Node, 因此都可以使用 Node 的屬性及方法。在實務上何時使用 Node 概念，何時使用 Element 概念是根據所需的屬性或所要進行的動作。例如要新增一個元素，設定內容會使用 element 概念，而要將這個元素掛在 document tree 的某個地方，就會使用 node 概念，如 appendChild, insertAfter, insertBefore 等。

Node 常用的屬性：

1. firstChild
2. nodeType
3. nodeValue
4. parentNode
5. parentElement

Node 常用的方法：

1. parentNode.appendChild( newNode )  新掛上一個已有的節點(子節點)
2. parentNode.insertAfter( newNode, currentNode )  新接上一個節點( 兄弟節點 )
3. parentNode.insertBefore( newNode, currentNode )
4. parentNode.removeChild( nodeToRemove )
5. parentNode.replaceChild( newNode, oldNode )

新增一個文字內容的節點，常用的方式是 document.createTextNode("text in the node") 以及 document.createTextContent("text in the Content")。

------

範例 1 開場範例 綜觀 js dom 如何運作

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>HTML 5 Introduction</title>
<script>
function myfun(){
   document.getElementById("demo").innerHTML = Date();
   }
</script>
<style>
  body { 
      width: 100%; 
      margin-left: auto; margin-right: auto; 
      background: gray;}
  header {
      width: 100%; height: 150px; 
      background: yellow; margins: 10px;}
  nav {
      width: 100%; height: 80px; color:blue;
      background-color: lightgrey;
    }
  nav li {
    float: left; color: black; display:green; width:100px;
    margin: 0px 10px ;
    border: 2px;
    border-color: green; border-width: medium; border-style: solid;
    background-color: yellow;}
  div.main{
    display: flex;
  }
  aside {width: 20%; background: pink; float: left;}
  article {width: 80%;  background: green; margins: 10px; float: left;}
  article p {color: white;}
  footer {width: 100%; height: 150px;
    /* background-size:1024px 150px; */
    background-repeat:no-repeat;
    margins: 10px;
  }
  footer div{
      margin-left:auto;    margin-right:auto;    width:70%;
      text-align: center;}
  li {
    list-style-type: none;
    text-align: center;
  }
</style>
   <script>
   function myfunc(){
     document.getElementById("demo").innerHTML=Date();
	   alert("對話框");
	   var r = confirm("確認更改背景色");
	   if( r ) 
           document.getElementsByTagName("article")[0].style.background="orange";
	   else document.getElementsByTagName("article")[0].style.background="white";
	   var fname = prompt("輸入字串", "預設字串");
	   document.getElementById("demo").innerHTML=Date() + fname;
  }
  function func2(x){
      x.style.color="#ff22ff";
  }
  function func3(x){
      x.style.color="black";
  }
  function changeColor(){
    document.getElementById("a1").style.backgroundColor="red";
  }
  </script>
</head>
<body>
    <header>
        <h1>HTML 5 </h1>
        <h2>An Introduction to HTML 5</h2>
    </header>
<nav>
  <ul>
  <li> 1. HTML</li><li> 2. CSS</li>
  <li> 3. JavaScript</li>
  </ul>
</nav>
<div class="main">
    <aside>
       <ul>
       <li>項目一</li>
       <li>項目二</li>
       <li ><input onclick="changeColor()" type="button" value="換顏色"  ></input></li>
       </ul>
    </aside>
    <article id = "a1">
    <p> 文章內容，幾個段落。</p>
        <form>
            <input type="date" name="d1"> </input>
            <input type="time" name="t1"> </input>
            <input type="submit" value="送出"> </input>
        </form>
        <input type="button" onclick="myfunc()" value="click here"/>
        <div id="demo" onmouseover="func2(this)" onmouseout="func3(this)">2013/1/1</div>
        <script>
          document.write("<p>Hello HTML5 JS程式輸出的標題</p>");
        </script>

        <h2>聯合大學109年數位學伴大學伴招募</h2>

        <p>歡迎所有聯大同學加入這個持續12年的公益服務活動</p>
        <p>不出校門也能做好事 每週只要2小時</p>
        <p>一起來 豐富生活 厚實生命</p>
        <p>守護偏鄉孩子 讓自己從被照護者成長為守護者</p>
        <p>全校20多個系所 近200為同學</p>
        <p>想更瞭解可洽各系駐系教授 或 曾參與同學</p>
	  </article>
</div>
<footer>
  <div> &copy; 2020 copyright </div>
</footer>
</body>
</html>
```

常用指令備忘：

1. let e = document.createElement("tagName");  // 建立元素

2. let e1 = document.importNode( e, true);  // 複製一個元素 e

3. e.setAttribute("attribute", "value");  // 給屬性值

4. v = e.getAttribute("attribute");  // 取得屬性值

5. let e = document.querySelector("selector");  // 取得元素節點

6. let es = document.querySelectorAll("selector"); // 取得元素陣列

7. 有 id 的元素，可以直接使用 id 當作元素參照：

   ```
   <form id="form1">
   <input type="text" value="">
   </form>
   
   <script>
     var fd = new FormData( form1 );  // from1 參照到網業中的 form 
     ...
   </script>
   ```

7. 掛上節點，
   1.  p.appendChild( node )
   2. p.insertBefore(node, child), p.insertAfter(node, child)
   3. e.insertAdjencentElement("beforeBegin", e2);   另外三種beforeEnd, afterBegin, afterEnd
8. e.id e.classList e.style 用來取得元素的 id, classList 以及 style 。可以讀寫
9.  input 的元素可以用  e.value 來讀寫內容。
10. 

