function outer(){
    var v1 = "叮噹";
    function inner(){
        alert(v1);
    }
    inner();
}

outer();

function makeFunc(){
    var name = "外部函式變數";
    function displayName(){
        alert(name);
    }
    return displayName;
}
var myFunc = makeFunc();
myFunc();

function makeAdder(x){
    return function(y){
        return x +y;
    }
}
var add5 = makeAdder(5);
var add7 = makeAdder(7);
console.log(add5(10));
console.log(add7(10));
// 在這個範例， x 是 makAdder 的區域變數，在 makeAdder(5) 的時候執行後
// x = 5了，此時雖然函式已經執行結束，但內部函式-匿名函式 function(y) 被 add5 reference 了，
// 因此 x 就會一保留住 5 給 add5() 使用。 這就是 lexical scoping 


function makeObj(y){
    var x = 0;
    return {
            increment: function(){ x += y;},
            decrement: function(){ x -= y;},
            value: function(){ return x}
        };
}

var ps5 = makeObj(5);
var ps7 = makeObj(7);

ps5.increment();
console.log(ps5.value());
ps5.decrement();
ps5.decrement();
console.log(ps5.value());

let today = new Date()
let birthday = new Date('December 17, 1995 03:24:00')
let birthday = new Date('1995-12-17T03:24:00')
let birthday = new Date(1995, 11, 17)
let birthday = new Date(1995, 11, 17, 3, 24, 0)

// Using Date objects
let start = Date.now()

// The event to time goes here:
doSomethingForALongTime()
let end = Date.now()
let elapsed = end - start // elapsed time in milliseconds
// Using built-in methods
let start = new Date()

// The event to time goes here:
doSomethingForALongTime()
let end = new Date()
let elapsed = end.getTime() - start.getTime() // elapsed time in milliseconds
// To test a function and get back its return
function printElapsedTime(fTest) {
  let nStartTime = Date.now(),
      vReturn = fTest(),
      nEndTime = Date.now()

  console.log(`Elapsed time: ${ String(nEndTime - nStartTime) } milliseconds`)
  return vReturn
}

let yourFunctionReturn = printElapsedTime(yourFunction)

var arx =  [];
