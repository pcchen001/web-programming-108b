function init(){
  // 如何使用 promise， 以 fetch 微粒
  $("#log").text("promise...");

fetch("./data.json")
.then( (resp) => {return resp.json()})
.then( (datajson) => {
  console.log("取得 data.json 資料");
  console.log( datajson )});

 

// 範例 1
var to = new Promise( function(rsolve, reject){
  if( Math.random() > 0.5){
    setTimeout(function () {
      
      rsolve("預設參數")

    }, 2000);
  }
  else{
    reject("隨機小於0.5")
  }

});

to
.then(function(res){sayHello(res);})
.catch((err)=>sayHello( err ));

function sayHello(res){
  console.log("範例1, 2秒之後 : hello "+ res)
}

// 範例 2
let myFirstPromise = new Promise((resolve, reject) => {
  // 當非同步作業成功時，呼叫 resolve(...),而失敗時則呼叫 reject(...)。
  // 在這個例子中，使用 setTimeout(...) 來模擬非同步程式碼。
  // 在實務中，您將可能使用像是 XHR 或者一個 HTML5 API.
  setTimeout(function(){
    resolve("Success!"); // Yay！非常順利！
  }, 250);
});

myFirstPromise.then((successMessage) => {
  // successMessage 是任何您由上方 resolve(...) 傳入的東西。
  // 在此僅作為成功訊息，但是它不一定是字串。
  console.log("範例2, 250ms 之後， Yay! " + successMessage);
});

let log = $("#log");
log.text("after Promise ...");

// 範例 3
// 'use strict';
var promiseCount = 0;

function testPromise() {
    let thisPromiseCount = ++promiseCount;

    // let log = document.getElementById('log');
    // log.innerText = "example 3 begins...";
    // log.insertAdjacentHTML('beforeend', "範例3<br/>" + thisPromiseCount +
    //     ') Started (<small>Sync code started</small>)<br/>');
    n = $("<p></p>").html("範例3<br/>" + thisPromiseCount +
         ') Started (<small>Sync code started</small>)<br/>');
    log.append(n);

    // 建立一個新的 promise：此 promise 承諾一個數值計數, 由 1 開始（等待約 2 秒）
    let p1 = new Promise( (resolve)=>{
        // 這個解決器函數（resolver function）呼叫實現或
        // 拒絕 promise。
            
            log.append($("<p>p1</p>"));
            // 在此例子單純用來產生非同步特性。
            window.setTimeout(
                function() {
                    // 實現這個 promise!
                    resolve(thisPromiseCount);
                }, Math.random() * 2000 + 1000);
        }
    );

    // 接著透過呼叫 then() 來決定 promise 進入 resolved 時，要透過 then() 做什麼，
    // 或是進入 rejected 時，要透過 catch() 方法要做什麼。
    p1.then(
        // 印出實現值（fulfillment value）
        function(val) {
            // log.insertAdjacentHTML('beforeend', val +
            //     ') Promise fulfilled (<small>Async code terminated</small>)<br/>');
        })
    .catch(
        // 印出失敗訊息（rejection reason）
        (reason) => {
            console.log('Handle rejected promise ('+reason+') here.');
        });

    // log.insertAdjacentHTML('beforeend', thisPromiseCount +
    //     ') Promise made (<small>Sync code terminated</small>)<br/>');
}

testPromise();
// ---------------------------------------------------------------
// async await
function resolveAfter2Seconds(x) {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(x);
    }, 2000);
  });
}


async function add1(x) {
  const a = await resolveAfter2Seconds(20);
  const b = await resolveAfter2Seconds(30);
  return x + a + b;
}

add1(10).then(v => {
  console.log(v);  // prints 60 after 4 seconds.
});


async function add2(x) {
  const p_a = resolveAfter2Seconds(20);
  const p_b = resolveAfter2Seconds(30);
  return x + await p_a + await p_b;
}

add2(10).then(v => {
  console.log(v);  // prints 60 after 2 seconds.
});

} // onload function end