class Particle{
  constructor(x, y){
    this.x = x;
    this.y = y;
  }
  change(){
    this.x += Math.random() * 10;
    this.y += Math.random() * 10;
  }
  display(){
    console.log(this.x + ", " + this.y);
  }
}

class SParticle extends Particle{
  constructor(x, y, s){
    super(x, y);
    this.size = s;
  }
  display(){
    console.log("SP: " + this.size + "(" + this.x + ", " + this.y + ")");
  }
}
var p1 = [];
p1.push( new Particle( 100, 100));
p1.push( new Particle( 100, 100));
p1.push( new SParticle( 10, 10, 5));
setInterval( ani, 1000 );
setTimeout( ()=>{
  for( p of p1){
    if( p.constructor.name == "SParticle")
        p.size += 100;
  }
}, 10000);

function ani(){
    for(let p of p1){
      p.change();
      p.display();
    }
}
