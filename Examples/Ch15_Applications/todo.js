// 使用 fetch 本機資料
function init(){
  fetch("todos.data")
  .then((resp)=>{return resp.json()})
  .then((resp)=>{resp.map(x=>{
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.name;
    })});
}
// 使用 fetch 遠端
function init2(){
  fetch("https://jsonplaceholder.typicode.com/todos")
  .then((resp)=>{return resp.json()})
  .then((resp)=>{resp.map(x=>{
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    if( x.completed === false)
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.title;
    else
    el.innerHTML="<input type='checkbox' onchange='toggle(event)' checked>"+x.title;
    
    })});
}
// 使用 axios
function init3(){
  axios.get("https://jsonplaceholder.typicode.com/todos")
  .then((resp)=>{resp.data.map(x=>{  
    console.log(x.name);
    var ul = document.getElementById("todos");
    var el = document.createElement("li");
    ul.appendChild(el);
    if( x.completed === false)
    el.innerHTML="<input type='checkbox' onchange='toggle(event)'>"+x.title;
    else
    el.innerHTML="<input type='checkbox' onchange='toggle(event)' checked>"+x.title;
    
    })});
}
function addtodo(){
  <!-- alert("hello"); -->
  var newitem = $("<li></li>")
      .html("<input type='checkbox' name='' value='' onchange='toggle(event)'>"
             +$("#newitem").val()
            );
  $("ul").append(newitem);
  $("#newitem").val("");
}
function toggle(event){
   if($(event.target).prop("checked") === true)
      $(event.target).parent().addClass("completed");
   else
      $(event.target).parent().removeClass("completed");
}
