export default {
    addpost: ({commit}, payload) => {
        commit("addnewpost", payload)
    },

    increment: ({commit})=> commit("inc"),
    decrement: ({commit})=> commit("dec")
}