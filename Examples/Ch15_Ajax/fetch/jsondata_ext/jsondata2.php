<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
if($_SERVER['REQUEST_METHOD'] == "POST"){
    // 換成php://input 
    $inputJson = file_get_contents('php://input');
    $p = json_decode($inputJson);
    $s = json_encode($p);
    // $s = '{"a":"post", "b":"banana", "c":123, "d":999}';
    echo $s;
}
else {
    $ans = $_GET['answer'];
    $s = '{"a":'.$ans.', "b":"banana", "c":123, "d":999}';
    echo $s;
}

?>