---
title: CSS Fonts & Icons
date: 2020-03-6 20:44:16
tags:
- Basic
- selector
categories:
- CSS
mathjax: false
---
# CSS 第三方支援

## Google Font API

使用 Google 提供的 font. 中文有 noto ，英文就多了。由於是免費有好看，許多人喜歡。

```html
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Noto+Sans+TC' rel='stylesheet' type='text/css'>

```

連結之後，我們可以使用 font-family 如 "Noto Sans TC" 等。

思源黑體繁體中文： Noto Sans TC

[參考說明](https://free.com.tw/google-fonts-noto-sans-tc/)

思源宋體繁體中文： Noto Serif CJK TC

[參考說明](https://free.com.tw/source-han-serif-noto-serif-cjk/)



## Font Awesome

線上免費圖示庫，你可以下載到本機端，也可以只使用線上的 CDN 支援。

[Font Awesome](https://fontawesome.com/icons?d=gallery&m=free) 檢視圖示

使用方式，點擊 Docs.

在 link font awesome 的連結之後，

```html
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css">
```

我們可以用下列範例來取得圖示：

```css
<i class="fas fa-camera"></i> <!-- this icon 1) style prefix == fas and 2) icon name == camera -->
<i class="fas fa-camera"></i> <!-- using an <i> element to reference the icon -->
<span class="fas fa-camera"></span> <!-- using a <span> element to reference the icon ->
```

下面用法，增加指定大小及顏色。我們注意到圖示是夾在  i 標籤內，而i標籤則夾在 span 標籤內。 Font Awesome 使用 i 的原因是斜體字大部分的人使用 em ，避免混淆。

```css
<span style="font-size: 3em; color: Tomato;">
  <i class="fas fa-camera"></i>
</span>

<span style="font-size: 48px; color: Dodgerblue;">
  <i class="fas fa-camera"></i>
</span>

<span style="font-size: 3rem;">
  <span style="color: Mediumslateblue;">
  <i class="fas fa-camera"></i>
  </span>
</span>
```

我們準備了一個範例：

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.12.1/css/all.css">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <div>

        hello world
        <div class="col-md-4">
            <span class="fa-stack fa-4x" style="color:tomato">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fas fa-paw fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">初學者輔導</h4>
            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
          </div>
          <div class="col-md-4">
            <span class="fa-stack fa-4x" style="color:tomato">
              <i class="fas fa-circle fa-stack-2x text-primary"></i>
              <i class="fab fa-apple fa-stack-1x fa-inverse"></i>
            </span>
            <h4 class="service-heading">初學者輔導</h4>
            <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Minima maxime quam architecto quo inventore harum ex magni, dicta impedit.</p>
          </div>
    </div>

</body>
</html>
```

品牌 icon 要使用 fab 取代 fas. 其餘相同。

著名的品牌： apple, google, facebook, twitter, ...