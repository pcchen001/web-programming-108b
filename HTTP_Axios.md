---
title: Ajax using Axios 
date: 2020-04-15 11:00:00
tags:
- JavaScript
- axios
- ajax
categories:
- HTTP
mathjax: false
---
# HTTP Ajax Axios

從網站擷取資料，可以是同步擷取或非同步方式。

非同步的方式唱用的有 XMLHTTPResquest/Response, jQuery, Fetch, Axios 等。

Fetch 是ES6 內建的函式。

Axios 是一個需要匯入的函式，受到很大的歡迎。

https://github.com/axios/axios 官方網站，常用的 CDN 有下列二個，擇一使用即可。

Using jsDelivr CDN:

```
<script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
```

Using unpkg CDN:

```
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
```



先來看看如何進行 GET request

```javaScript
// const axios = require('axios');  模組匯入 編譯時使用

// Make a request for a user with a given ID
axios.get('/user.php?ID=12345')
  .then(function (response) {
    // handle success
    console.log(response);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });

// Optionally the request above could also be done as
axios.get('/user.php', {
    params: {
      ID: 12345,
      passwd: 'mypassword'
    }
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  })
  .then(function () {
    // always executed
  });  

// Want to use async/await? Add the `async` keyword to your outer function/method.
//  若使用 async/await 指令時，加上 async 

async function getUser() {
  try {
    const response = await axios.get('/user.php?ID=12345');
    console.log(response);
  } catch (error) {
    console.error(error);
  }
}
```

> **NOTE:** `async/await` is part of ECMAScript 2017 and is not supported in Internet Explorer and older browsers, so use with caution. 微軟的　Edge 使用 Chrome 引擎，所以也支援了。

再來看如何進行一個 `POST` request

```javascript
axios.post('/user', {
    firstName: 'Fred',
    lastName: 'Flintstone'
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
```

同時進行多個  GET requests

```javascript
function getUserAccount() {
  return axios.get('/user/12345');
}

function getUserPermissions() {
  return axios.get('/user/12345/permissions');
}

axios.all([getUserAccount(), getUserPermissions()])
  .then(axios.spread(function (acct, perms) {
    // Both requests are now complete
    // acct 是 getUserAccount() 回來的，perms 是getUserPervissions()回來的
  }));
```

大部分的情形，上面的幾個範例都能夠解決，只有特殊的情況才需要更多的設定及細節，例如要 post 的包含檔案，以及要 post 的只有一個 blob 。

另外，有時候針對 CORS, request 端的 header 也需要設定，此時就需要使用 header 參數。



