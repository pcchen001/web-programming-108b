---
title: CSS Layout
date: 2020-03-6 20:44:16
tags:
- Basic
- selector
categories:
- CSS
mathjax: false
---
# CSS Layout 排版

CSS 的排版模式分為七種：

1. Normal Flow 正常流,  預設排版模式。包含block 或 inline。
2. Table 表格排版
3. Float 文繞圖排版
4. Positioned 定位式排版 靜態，相對，絕對，固定。
5. Multi-column layout 多欄排版
6. Flexible box 彈性盒排版
7. Grid 格狀排版

## Normal Flow

Inline 元素 一個接著一個，block 元素則上下一塊接一塊，稱之為 flow layout.
若有特別的設定時，就會跳出原來的 flow 排版，根據設定到指定的位置上。

## Table Layout

[Table Layout](http://www.mozila.org/tableLayout)

table-layout: fixed; // auto

## Positioning Layout

## Float Layout

## Multiple-Column Layout

多欄排版是指連續的文章(in-line)內容，依據設定做二欄以上的排版。最常用的是指令是以  column-count:  將版面分為二欄或三欄。

```
div {
  columns: 200px; // 一個欄寬設為 200px 若頁面 400 以上，分為2欄
}
columns: 3; // 分為3欄，欄寬平均分配
column-count: 4 // 分為 4 欄，欄寬平均分配
column-width: 300px // 1個欄寬設為 300px
column-gap: 10px; // 欄間空隙寬
column-fill: 
column-rule: silver solid 1px;
column-span: 2; // 跨2個欄位
```

[Mozilla-CSS-Multiple-Column](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Columns/Basic_Concepts_of_Multicol)

## Flex Box

這是 Bootstrap 等 Mobile 裝置上排版框架很常用的排版模式。

[Mozilla-Flex-Box](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container)

當一個 element 設定了 display:flex; 他所含的所有元素就具有 flex item 的性質。包含 flex items 的框框就是 flex box。

在一個 flex box 內，flex items 的排列方向有四種：row, row-reverse, column, column-reerse。預設 row (由左向右)。改變排列方向 flex-direction: row-reverse;  詳細參考： [flex-direction](https://developer.mozilla.org/en-US/docs/Web/CSS/flex-direction)

我們可以想像，flex box 的排版是將區域分為幾個直欄或或幾個橫列，每一個直欄或橫列再區分為幾個橫列或直欄，依此類推下去。也算是非常好用的排版方式。



flex 基於main axis 及 cross axis 架構進行對齊。

預設 row 是設定幾個直欄，main axis 是有左向右的水平方向。

排列方向決定了 main axis 及 cross axis 。main axis 是一條沿著 排列方性的主軸線。 cross axis 是與 main axis 垂直交叉方向的軸線。

flex box 排版模式的對齊概念是基於四個對齊概念：

- [align-items](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container)  所有項目在交叉軸上的對齊控制(與主軸垂直)
- [align-self](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container) 對單一項目在交叉軸上的對齊控制
- [align-content](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container) 在交叉軸上，flex lines 之間間距如何分配
- [justify-content](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Aligning_Items_in_a_Flex_Container) 所有項目在主軸上的對齊控制

我們從最簡單的範例看起。

```html
<body>
    <header>Hello Header</header>
    <section>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
        <article>First Article <p>the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs the paragraphs </p></article>
    </section>
</body>
```

```css
       section {
           display: flex;
           background-color: peru;
           flex-wrap: wrap;
       }
       article {
           width: 20%;
           margin: 10px;
       }
```

display: flex; 讓 section 內包含的 article 每一個都是一個 flex item。 flex-wrap: wrap; 表示當 flex box 的寬度不夠放所有的 flex item 時，放不下的折到下一列去。註：若 item 沒有設定寬度，item 的寬度將會由內容來決定。

上述的範例，請試試看挪動頁寬，看看效果。

Flex box, 以 flex-diretion: row 為例，可以用下圖來表示模型：

![flex_terms.png](https://developer.mozilla.org/files/3739/flex_terms.png)

在一個 flex box 安排 flex item 的位置，假設我們在主軸上安排3 個 item。若這三個 item 的高度不同，我們需要考慮如何對齊？沿著上面、沿著下面，對齊中間？等等。 [align-items](https://developer.mozilla.org/en-US/docs/Web/CSS/align-items) 就是設定如何對齊：

```css
/* Basic keywords */ 
align-items: normal; 
align-items: stretch; 

/* Positional alignment */ 
/* align-items does not take left and right values */
align-items: center; /* Pack items around the center */ 
align-items: start; /* Pack items from the start */ 
align-items: end; /* Pack items from the end */ 
align-items: flex-start; /* Pack flex items from the start */ 
align-items: flex-end; /* Pack flex items from the end */

/* Baseline alignment */
align-items: baseline; 
align-items: first baseline; 
align-items: last baseline; /* Overflow alignment (for positional alignment only) */ 
align-items: safe center; 
align-items: unsafe center; 

/* Global values */
align-items: inherit; 
align-items: initial; 
align-items: unset;
```

常用的有 stretch, center 。

如果只想針對個別的 flex item 作調整，我們使用 [align-self](https://developer.mozilla.org/en-US/docs/Web/CSS/align-self) 這個屬性。

```css
.box {
  display: flex;
  align-items: flex-start;  // 原則上 四個items 都是上沿對齊
  height: 200px;
}
.box>*:first-child {       // 但，One 的這個 div 拉滿
    align-self: stretch;  
}
.box .selected {           // 另外，Three 中央對齊
    align-self: center;
}
```

```html
<div class="box">
  <div>One</div>
  <div>Two</div>
  <div class="selected">Three</div>
  <div>Four</div>
</div>
```

以上面的片段為例，

align-self 只對所選的 item 進行對齊設定。可以設定的值有：

```css
/* Keyword values */ 
align-self: auto; 
align-self: normal; 

/* Positional alignment */ 
/* align-self does not take left and right values */
align-self: center; /* Put the item around the center */ 
align-self: start; /* Put the item at the start */ 
align-self: end; /* Put the item at the end */ 
align-self: self-start; /* Align the item flush at the start */ 
align-self: self-end; /* Align the item flush at the end */ 
align-self: flex-start; /* Put the flex item at the start */ 
align-self: flex-end; /* Put the flex item at the end */ 

/* Baseline alignment */ 
align-self: baseline; 
align-self: first baseline; 
align-self: last baseline; 
align-self: stretch; /* Stretch 'auto'-sized items to fit the container */ 

/* Overflow alignment */ 
align-self: safe center; 
align-self: unsafe center; 

/* Global values */ 
align-self: inherit; 
align-self: initial; 
align-self: unset;
```

### align-content

當我們的 flex-item 在一個 flex-box 因為wrap 而不只一排時， [align-content](https://developer.mozilla.org/en-US/docs/Web/CSS/align-content) 用來設定橫排之間的間隙。

以下圖為例，8個方塊被排成了三排，三排之間的上下的間隙就由 align-content 來設定。

![image-20200308162517663](image-20200308162517663.png)

```css
/* Basic positional alignment */
/* align-content does not take left and right values */
align-content: center;     /* 三排集中在中間 */
align-content: start;      /* 三排往上面擠 */
align-content: end;        /* 三排往下面擠 */
align-content: flex-start; /* flex items 往上面擠 */
align-content: flex-end;   /* flex items 往下面擠 */

/* Normal alignment */
align-content: normal;

/* Baseline alignment */ 
align-content: baseline;
align-content: first baseline;
align-content: last baseline;

/* Distributed alignment */
align-content: space-between; /* 上下頂住，空隙平均分配 */
align-content: space-around;  /* 每一塊都有相同的外圍空隙 */
align-content: space-evenly;  /* 類似上一個 */
align-content: stretch;       /* 彈性高度，填滿box高度 */

/* Overflow alignment */
align-content: safe center;
align-content: unsafe center;

/* Global values */
align-content: inherit;
align-content: initial;
align-content: unset;
```

提醒：如果 flex-direction 改為 column，那麼上下就變成了左右囉！

### justify-content

 [justify-content](https://developer.mozilla.org/en-US/docs/Web/CSS/justify-content) 設定主軸上 item 的排版˙，預設是接著放如下。

假設一個 flexbox 寬 500px, 三個 item 分別都是 100px. 不同的 justify-content 設定，a, b, c 放置的方式不同。

![image-20200308164124439](image-20200308164124439.png)

```css
/* Positional alignment */
justify-content: center;     /* 三個靠中間，左右留空 */
justify-content: start;      /* 三個靠左 */
justify-content: end;        /* 三個靠右 */
justify-content: flex-start; /* 三個靠左 */
justify-content: flex-end;   /* 三個靠右 */
justify-content: left;       /* Pack items from the left */
justify-content: right;      /* Pack items from the right */

/* Baseline alignment */
/* justify-content does not take baseline values */

/* Normal alignment */
justify-content: normal;

/* Distributed alignment */
justify-content: space-between; /* 平均分配之間空隙 */
justify-content: space-around;  /* 平均分配周圍空隙 */
justify-content: space-evenly;  /* 平均分配周圍空隙，.. */
justify-content: stretch;       /* 拉滿 */

/* Overflow alignment */
justify-content: safe center;
justify-content: unsafe center;

/* Global values */
justify-content: inherit;
justify-content: initial;
justify-content: unset;
```

### flex

[flex](https://developer.mozilla.org/en-US/docs/Web/CSS/flex) flex 設定膨脹縮小的情形。

flex 設定每一個 item 是否膨脹或縮小來滿足外框 flex box 的空間。當 item 較小，box 較大，依照 膨脹設定拉大( flex-grow ) 當 item 較大，box 較小，依照縮小設定變小( flex-shink )。flex 可以設 1 個，2個或 3 個。

```css
/* Keyword values */
flex: auto;
flex: initial;
flex: none;

/* One value, unitless number: flex-grow */
flex: 2;

/* One value, width/height: flex-basis */
flex: 10em;
flex: 30%;
flex: min-content;

/* Two values: flex-grow | flex-basis */
flex: 1 30px;

/* Two values: flex-grow | flex-shrink */
flex: 2 2;

/* Three values: flex-grow | flex-shrink | flex-basis */
flex: 2 2 10%;

/* Global values */
flex: inherit;
flex: initial;
flex: unset;
```

我自己覺得 flex 就很好用了！

## Grid

grid layout 有點像 Table, 但支援更多一些。

[CSS layout - Grid](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Grids) [Overview]

[Basic Concepts of grid layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout) [Basic Concept, 建議]

[box-alignment in grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Alignment/Box_Alignment_In_Grid_Layout) [Alignment]

[CSS Grid Layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout) [Reference]

參考投影片說明。

筆記：

1. Grid 的排版方式可以先定義一個 container 的 display: grid。使用 grid-template-columns: 及 grid-template-rows: 二個性質設定幾個直欄，幾個橫列。指定的時候可以使用 px 也可以使用 fr 作單位。接著使用 grid-gap: 設定格子之間的間隙

   ```
   .container {
       display: grid;
       grid-template-columns: 200px 200px 200px;
   }
   .container {
       display: grid;
       grid-template-columns: 1fr 1fr 1fr;  // 分為三的單位，各分配一個單位
   }
   .container {
     display: grid;
     grid-template-columns: repeat(auto-fill, minmax(200px, 1fr));
     grid-auto-rows: minmax(100px, auto);
     grid-gap: 20px;
   }
   // auto-fill 是能擠幾個就擠幾個 (as many columns as will fit)
   // grid-auto-rows 或是 grid-auto-columns 是指定一格的高或是寬。
   // minmax()來設定最小及最大。 auto 表示預設
   ```

2. 格子打好之後，.container 所包含的元素，依序上到下，從左到右，一個一個擺放。擺放之後的內容要對齊，使用跟 flexbox layout 相似的 align 設定。

3. 另一種方格指定的方式是以分界線為基礎的配置。假如我們使用 grid-template-columns 加grid-template-rows 設定了有 n x m 個。我們可以想像格狀配置的分界線，從上到下編號 1 ~ n+1；從左到右編號 1 ~ m+1 。這個以分界線為基礎的配置就是指定一個元素的座標

   ```
   .box1 {
       grid-column-star: 1;
       grid-column-end: 3;  // 從線1到線3 佔二個格寬
       grid-row-start: 2;
       grid-row-end: 3;     // 從線2到線3 占一個格高
   }
   // 另一種寫法
   .box1 {
       grid-column: 1/3;    // 使用斜線標示從1線1到線3
       grid-row: 2;
   }
   ```

   ```html
   <div class="container">
   <div class="box1"> ... </div>
   ...
   </div>
   ```

   除了使用 grid-tempalte-columns 及 grid-template-rows 設定之外，也可以使用 grid-template-area 來設定。我們可以為每一個方格標一個名字，然後用此名字來指定元素的位置。

   ```css
   .container {
     display: grid;
     grid-template-areas: 
         "header header"
         "sidebar content"
         "footer footer";
     grid-template-columns: 1fr 3fr;
     grid-gap: 20px;
   }
   
   header {
     grid-area: header;
   }
   
   article {
     grid-area: content;
   }
   
   aside {
     grid-area: sidebar;
   }
   
   footer {
     grid-area: footer;
   }
   ```

   

## Contents

display 可以設定 none, 這個元素將會隱藏。

display 也可以設定 contents, 這會讓這個元素不產生 box, 只有純粹內容而已。因為不產生 box, 也就沒有元素內的元素的概念囉！

```css
display: none;
display: contents;
```

