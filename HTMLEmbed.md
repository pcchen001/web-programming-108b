---
title: HTML 影像及影音嵌入
date: 2020-01-27 19:25:52
tags:
- HTML
- basics
- embedding
categories:
- HTML
mathjax: false
---

豐富一個網頁需要一些文字以外的元素，HTML 提供可以插入圖像、影音、表格以及表單等非文字內容，我們要熟悉這些語法。

# 多媒體嵌入

## Images

嵌入圖像，在概念上分為二種，一種是這個圖像是內容的一部分，另一種就只是用來裝飾畫面的，若是內容的一部分最好在標籤的屬性上加上 alt=" "，對這個圖像給一些說明。若是只用來裝飾的，建議使用 css 的背景圖像設定。嵌入圖像的語法：

```html
<img src="dinosaur.jpg">
```

或是

```html
<img src="images/dinosaur.jpg">
```

或是

```html
<img src="https://www.example.com/images/dinosaur.jpg">
```

影像若有問題時，我們可以先準備好替代文字：

```html
<img src="images/dinosaur.jpg"
     alt="恐龍圖片替代文字">
```

另外常用的屬性有 width, height 以及 title 。title 用來標示當滑鼠移到圖像上時，要顯示的文字內容：

```html
<img src="images/dinosaur.jpg"
     alt="The head and torso of a dinosaur skeleton;
          it has a large head with long sharp teeth"
     width="400"
     height="341"
     title="A T-Rex on display in the Manchester University Museum">
```

對圖像的註腳說明，可參考下列標籤：

```html
<figure>
  <img src="images/dinosaur.jpg"
       alt="The head and torso of a dinosaur skeleton;
            it has a large head with long sharp teeth"
       width="400"
       height="341">

  <figcaption>A T-Rex on display in the Manchester University Museum.</figcaption>
</figure>
```

## Video & audio

[內容參考Mozilla](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Video_and_audio_content)

```html
<video src="rabbit320.webm" controls>
  <p>Your browser doesn't support HTML5 video. Here is a <a href="rabbit320.webm">link to the video</a> instead.</p> 
</video>
```

加上 controls 會讓影音框具有控制按鈕。標籤內的內容只有再 video src 的內容無法正確呈現時，才會出現。

```html
<video controls width="400" height="400"
       autoplay loop muted
       poster="poster.png">
  <source src="rabbit320.mp4" type="video/mp4">
  <source src="rabbit320.webm" type="video/webm">
  <p>Your browser doesn't support HTML video. Here is a <a href="rabbit320.mp4">link to the video</a> instead.</p>
</video>
```

這個範例比較完整， autoplay 表示會自動播放， loop 循環播放，muted 消音，poster 放置靜態圖像。source 可以有二個以上，看瀏覽器支援的解碼器依序擇播。

```html
<audio controls>
  <source src="viper.mp3" type="audio/mp3">
  <source src="viper.ogg" type="audio/ogg">
  <p>Your browser doesn't support HTML5 audio. Here is a <a href="viper.mp3">link to the audio</a> instead.</p>
</audio>
```

聲音檔的嵌入與影音檔類似， controls 也是控制按鈕。autoplay 自動播放；loop 循環播放。沒有 width, height 以及 poster 這三個屬性。

影音檔嵌入也可以加入字幕，需要時再查相關用法。

[多媒體嵌入-Mozilla練習](https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Mozilla_splash_page)