---
title: JS Promise, Async & Await
date: 2020-01-25 11:13:04
tags:
- Promise
- Async
- Await
categories:
- Advanced
- Javascript
---

## Promise, async & await

JS 的 promise 提供方便的非同步指令。

傳統， callback function 用來處理某個函式完成後，接著才進行 callback function 的動作。ES6 增加使用 Promise 來處理。目前 fetch 就是一個 Promise 的實作，此外 axios 也是使用 Promise 完成的 ajax 函式。

下面是一個簡潔的使用範例，r, j 代表 resolve 及 reject 的函式名稱。r("Good Return") 會正常返回字串內容。j("Error Message") 表示程式執行至此的狀況不OK, 返回一個錯誤訊息。

執行時，.then( func(v) ) 處理正常返回 .catch( func(e)) 處理錯誤返回。

```js
        tmp = new Promise((r, j)=>{
            v = Math.random();
            if( v < 0.4 ) j("Error Message");
            else r("Good Return");
        });
// r, j 分別對應 resolve 及 reject 
tmp
.then((v)=>{console.log(v)})
.catch((e)=>{console.log(e)});
// 執行時，若 v >= 0.4 我們設定為正常結束， v 返回 Good Return
//        若 v < 0.4 我們設定為不正常結束， e 返回 Error Message
```

我們再舉一個範例，設計一個延遲 t ms 的函式：

我們先使用傳統的寫法，看看有何問題：( ref. promise01.html)

```js
       function p1(t){
           setTimeout(()=>{
               console.log("P: " + t)
           }, t);
       }
       noAwait.onclick = ( e )=>{
           const a = p1(2000);
           const b = p1(1000);
           const c = p1(500);
       }
```

你會注意到，先顯示 P: 500, 然後是 P:1000 最後才是 P:2000。

若希望我們的程式能夠依照 a, b, c 順序執行，那就需要一個同步的處理。我們看看 Promise 如何處理：

```js
        function p(t){
            return new Promise((resolve,reject)=>{
                setTimeout(()=>{
                    console.log("P: " + t);
                    resolve(t+99);
                }, t);
            })
        }
        p(1000).then((v)=>{console.log("..."+v)});
```

假設我們要執行三個程式，要求第一個完成後才能進行第二個，完成之後才能進行第三個。

```js
       function p(t){
           return new Promise(
               (r, j) =>{
                    setTimeout(()=>{
                        console.log("P: "+t);
                        r( t +99 )
                    }, t);
               }
           );
       }
       delayt.onclick = async ( e )=>{
           const a = await p(2000);
           const b = await p(1000);
           const c = await p(500);
           console.log(a+b+c)
       }
```

我們可以將要進行的程式以 await 執行。 await 會強制這個 Promise 執行之後再繼續。

await 只能放在 async 的函式內。 await 後面接的通常是一個 Promise 或是會返回一個 Promise 的函式。

async 的函式，預設返回一個 Promise 而 return 的值會作為 resolve 的返回值。在上面的範例，ps().then()表示ps()返回一個 Promise, resolve 返回的值出現再 .then() 中的 v。

當我們需要執行一些用 Promise 實作的函式時，很方便的一種做法是使用 await 呼叫。

```js
async handle(){
   try{
    let r = await fetch("url");
    r = await r.json();
    result.innerHTML = r.content;
   }
   catch( error ){
     console.log( error );
   }

}
```





------

下面是另一個稍微複雜一些，我讓每一個要 return Promise 的函式都有 resolve 及 reject 二,種返回。這些 function 在執行 await 時，需要使用 try{ }catch(e){}來處理。

```js
        let Y = 0;
        function one_(y){ return new Promise((r, j)=>{
            setTimeout(()=>{
                y += 10;
                let v = Math.random();
                Y += 10;
                if( v < 0.4 ) 
                    j("Error Message");
            	else r(y);
            } 
                ,1000
            );
        });}

        function zero_(y){ return new Promise((r, j)=>{
            console.log("zero:-- "+y);
            console.log("Y: -----" + Y)
            y+=15
            let v = Math.random();
            if( v < 0.4 ) j("Error---");
            else r(y);
        });}

        // tmp 會比 tmp1 更早結束，因為 tmp 不等待 -------------
        
        // 若要 tmp1 執行完再接著 tmp 則需要下列 async設計
        async function ttmp(){
            let a="", b="";
            try{const v = await one_(3); a = v; console.log("await one second");}
            catch(e){ console.log(e)}
            try{ const v = await zero_(a); b = v; console.log("await none");}
            catch(e){ console.log(e)}           
            return a+"--"+b;
        }
        one_(3)
        .then((v)=>{console.log(v); })
        .catch((e)=>{console.log(e)});

        zero_(103)
        .then((v)=>{console.log(v); })
        .catch((e)=>{console.log(e)});

        ttmp()
        .then((v)=>{console.log(v)});
```

我們執行了三個指令： 

one---  延遲1秒 返回 3+10

zero--- 不延遲 返回 103+15

ttmp--- 先 one 然後 zero 最後印出 one 返回+zero 返回。

我們發現： zero 最先做完，接著是 one ，ttmp 中， one 先做完後才輪到 zero 。

```
zero:-- 103
promise1.html:30 Y: -----0
promise1.html:40 Error---
promise1.html:26 Error Message
promise1.html:46 await one second
promise1.html:29 zero:-- 13
promise1.html:30 Y: -----20
promise1.html:48 await none
promise1.html:53 13--28
```

我們測試了 Y 全域變數，希望確認全域變數的更動按照執行的先後更動！ 結果是正確的！

我們設計了 y 作為引數帶入我們的函式，希望學習如何傳遞參數。

