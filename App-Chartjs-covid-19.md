---
title: App-Chartjs-covid-19
date: 2020-05-14 21:58:14
tags:
- chart
- chart.js
categories:
- Application
---

# 統計圖的繪製 chart.js 應用

以 約翰霍普斯金醫學中心 新冠確診/死亡資料 .csv 為例

抓資料：

```
let url = "https://raw.githubusercontent.com/CSSEGISandData/COVID-19/master/csse_covid_19_data/csse_covid_19_time_series/"
let urlgc = url + "time_series_covid19_confirmed_global.csv"
let urlgd = url + "time_series_covid19_deaths_global.csv"
let urlusc = url + "time_series_covid19_confirmed_US.csv"
fetch(furgc)
.then((f)=>f.text())
.then((csv)=>{
  .... csv 資料的處理
})
```

我們將文字資料取出之後，施行 .csv 的格式讀取：

```
                let lines = csv.split("\n")
                let headers = lines[0].split(",")  // 表頭資料 (日期)
                result=[]
                for(let i = 1; i <lines.length; i++){
                    let cline = lines[i].split(",")
                    if( parseInt(cline[headers.length-2]) > 100000)  //最近一日累積超過十萬
                        result.push(cline)
                }
```

接著取出符合的國家區域的每日資料：

```
                let data = []
                for(let i = 0; i < result.length-1; i++){
                    tmpdata = result[i].slice(4)  // 前四項 名稱座標 暫時不取
                    for(let j = 0; j < tmpdata.length; j++){
                        tmpdata[j] = parseInt(tmpdata[j])  // 文字轉數字
                    }
                    data.push(tmpdata) // 將每個國家的資料記錄下來！
                }
```

這些資料我們要拿來繪圖，因此我們要看看 chart.js 的繪圖組態資料：

```
            let config = {
                    type: 'line',    // 這是型態
                    data: {
                        labels: [],              // 這是 X 軸的標籤
                        datasets: [{
                        			label: 'US',                    // 第一組資料
                        			data: [....]   //該國資料,
                        			borerColor: 'rgba(100, 200, 10, 1)',   // 框色
                        			backgroundColor: 'rgba(0,0,0,0)'       // 被景色
                                    },{
																    // 第二組
                                    },{
																    // 第三組
                                    }]
                    },
                    options: {}
                }
```

其中 config.data.labels 是一組資料有幾個資料，在我們的範例，這是日期，要從第一行資料中去取。

 config.data.datasets 則一組一組國家資料，包含國家區域名稱、資料組(陣列)、外框線條色彩、背景塗色等。

config 是 javascript 物件格式，裡面scale型態直接給值， array 型態則用 push 加資料上去：

```js
            let config = {
                    type: 'line',
                    data: {
                        labels: [],
                        datasets: []
                    },
                    options: {}
                }
            var data = []
            var result;
            ctx = 'myChart'              // 取的 ctx 的幾種方法之一
            fetch(urlgc)
            .then((f)=>f.text())
            .then((csv)=>{
                let lines = csv.split("\n")
                let headers = lines[0].split(",")
                result=[]
                for(let i = 1; i <lines.length; i++){
                    let cline = lines[i].split(",")
                    if( parseInt(cline[headers.length-2]) > 100000)
                        result.push(cline)
                }
                let data = []
                for(let i = 0; i < result.length; i++){
                    tmpdata = result[i].slice(4)
                    for(let j = 0; j < tmpdata.length; j++){
                        tmpdata[j] = parseInt(tmpdata[j])
                    }

                    adata={}
                    adata.label = result[i][1]+"/"+result[i][0]
                    // 色彩依據 css 定義，可以有三種表示 rgb/rgba, hsl/hsla, #0387aa
                    // rgba: r,g,b 255   a, 0~1
                    // hsl: h 0 ~ 360  s,l 百分比符號：0% ~ 100%
                    // hue 色相  saturation 飽和度  lightness 明度
                    h = Math.floor( i * 360 / result.length)     // 間隔取色相
                    s = "90%"
                    l = "50%"
                    adata.borderColor = 'hsl('+h+', '+s+', '+l+')'
                    adata.backgroundColor = 'rgba(0,0,0,0)'     // 背景色設為透明
                    adata.data = tmpdata
                    config.data.datasets.push(adata)                    
                }
                for(let i = 4; i < headers.length; i++){  // x軸標示， 資料前四個是名稱及座標
                    config.data.labels.push(headers[i]);
                }
                new Chart(ctx, config)
            })
```

new Chart( ctx, config ) 是執行繪圖動作的指令， ctx  = 'myChart' 是取得變數的捷徑語法。 

現在，只要再網頁上加上如下內容 即可

```
<canvas id="myChart"></canvas>
```

為了好玩一點，請大家練習將圖片改成每日確診人數( 每一天減去前一天 )。

```js
            byday.onclick = (e) =>{
                let data=[]
                config.data.datasets=[]
                for(let i = 0; i < result.length; i++){
                    let tmpdatax = result[i].slice(4)
                    console.log(tmpdatax.length)
                    let tmpdata = []
                    tmpdata.push(0)    // 補第一天沒得減前一天！
                    for(let j = 0; j < tmpdatax.length; j++){
                        tmpdata.push(parseInt(tmpdatax[j+1])-parseInt(tmpdatax[j]))
                    }
                    data.push ( tmpdata )
                    // console.log(tmpdata)
                    adata={}
                    adata.label = result[i][1]
                    h = Math.floor( i * 360 / result.length)
                    s = "90%"
                    l = "50%"
                    adata.borderColor = 'hsl('+h+', '+s+', '+l+')'
                    adata.backgroundColor='hsla(30, 90%, 50%, 0.3'
                    adata.data = tmpdata
                    config.data.datasets.push(adata) 
                                     
                }
                new Chart(ctx, config)  
            }
```

請各位留意更改 config 的做法。 new Chart(ctx, config) 就可以重繪內容。若要進行切換，則應該再加一個 button 進行驅動。

```js
            accum.onclick = (e) =>{
                let data=[]
                config.data.datasets=[]
                for(let i = 0; i < result.length-1; i++){
                    let tmpdata = result[i].slice(4)
                    for(let j = 0; j < tmpdata.length; j++){
                        tmpdata[j] = parseInt(tmpdata[j])
                    }
                    data.push ( tmpdata )
                    // console.log(tmpdata)
                    adata={}
                    adata.label = result[i][1]
                    h = Math.floor( i * 360 / result.length)
                    s = "90%"
                    l = "50%"
                    adata.borderColor = 'hsl('+h+', '+s+', '+l+')'
                    adata.backgroundColor = 'rgba(100, 200, 30, 0.1)'
                    adata.data = tmpdata
                    config.data.datasets.push(adata)                    
                }
                new Chart(ctx, config)  
            }
```

 chart.js 同時也支援許多不同的圖表，資料結構類似。

http://chartjs.org 

```html
<body onload="init()">
    <div id="headerdiv">headers</div>
    <div id="cdata"></div>
    <button id="byday">每日確診</button>
    <button id="accum">每日累積</button>
    <canvas id="myChart"></canvas>
</body>
```

參考的 html body 。